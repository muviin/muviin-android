package com.muviin.muviin_app;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.android.FlutterFragmentActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.shim.ShimPluginRegistry;

import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import java.util.Map;


public class MainActivity extends FlutterActivity {

  private static final String CHANNEL = "com.muviin.muviin_app/street";


  private MethodChannel.Result result = null;
  private MethodChannel.Result channelResult;


//  @Override
//  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//    super.onActivityResult(requestCode, resultCode, data);
//    if(requestCode == 300){
//      result.success("yo very successfull");
//    }
//
//  }

//  @Override
//  protected void onCreate(Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//    GeneratedPluginRegistrant.registerWith(this);
//
//
//  }

  @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {

    // Use the GeneratedPluginRegistrant to add every plugin that's in the pubspec.
    super.configureFlutterEngine(flutterEngine);


    ShimPluginRegistry shimPluginRegistry = new ShimPluginRegistry(flutterEngine);
    flutterEngine.getPlugins().add(new FlutterPlugin() {
      @Override
      public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        // this commented line of codes is used for displaying view inside flutter
//        RelativeLayout lay = findViewById(R.id.googlemapstreetview);
//        binding.getPlatformViewRegistry().registerViewFactory("flutterpiechart", new StreetViewFactory(shimPluginRegistry.registrarFor("com.muviin.muviin_app"), flutterEngine.getDartExecutor(), lay));


        new MethodChannel(flutterEngine.getDartExecutor(), CHANNEL).setMethodCallHandler(
              (call, result) -> {
                channelResult = result;

                if (call.method.equals("streetview")) {
                  Object argumentsObj = call.arguments;
                  if (argumentsObj instanceof Map) {
                    Map<String, Double> arguments = (Map<String, Double>) argumentsObj;
                    // start intent
                    moveToStreetView(arguments);
                  }
                }
              });
      }

      @Override
      public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {

      }
    });

  }

  void moveToStreetView(Map<String, Double> arguments){
    System.out.println("moving into streetview class");
    System.out.println(arguments.get("latitude"));
    System.out.println(arguments.get("longitude"));

    Intent intent = new Intent(this, StreetC.class);
    intent.putExtra("latitude", arguments.get("latitude").toString());
    intent.putExtra("longitude", arguments.get("longitude").toString());
    startActivity(intent);
  }


  void test(MethodChannel.Result result){

    this.result = result;
    Intent move = new Intent(this, Click.class);
    startActivityForResult(move, 300);
//    startActivity(move);
  }





}
