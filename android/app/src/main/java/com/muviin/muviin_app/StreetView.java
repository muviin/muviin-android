package com.muviin.muviin_app;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
//import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewPanoramaOrientation;
import com.google.android.gms.maps.model.StreetViewSource;

import java.util.HashMap;
import java.util.Map;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.android.FlutterFragmentActivity;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.platform.PlatformView;

public class StreetView extends Activity implements PlatformView, MethodChannel.MethodCallHandler, OnStreetViewPanoramaReadyCallback {
    Context context;
    PluginRegistry.Registrar registrar;
    MethodChannel channel;
    DartExecutor executor;
    View  finalView;
    RelativeLayout llll;

    private StreetViewPanorama streetViewPanorama;
    private boolean secondLocation = false;


    StreetView(Context context, PluginRegistry.Registrar registrar, int id, Object args, DartExecutor executor, RelativeLayout layout) {
        this.context = context;
        this.registrar = registrar;
        this.executor = executor;
        this.llll = layout;
        Map<String, Double> arguments = new HashMap<>();

        if (args instanceof HashMap) {
            arguments = (Map<String, Double>) args;
        } else {
        }

        this.finalView = setupStreetView(registrar, arguments);

        this.channel = new MethodChannel(executor, "flutterpiechart_" + id);
        this.channel.setMethodCallHandler(this);
    }


    private View setupStreetView(PluginRegistry.Registrar registrar, Map<String, Double> dataMap) {


        RelativeLayout layout = new RelativeLayout(context);
        layout.setBackgroundColor(Color.BLUE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(params);
        StreetC ss = new StreetC();


        return layout;
    }


    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        this.streetViewPanorama = streetViewPanorama;
        if(secondLocation){
            streetViewPanorama.setPosition(new LatLng(51.52887, -0.1726), StreetViewSource.OUTDOOR);
        }
        else{
            streetViewPanorama.setPosition(new LatLng(51.52887, -0.1726));
        }
        streetViewPanorama.setStreetNamesEnabled(true);
        streetViewPanorama.setPanningGesturesEnabled(true);
        streetViewPanorama.setZoomGesturesEnabled(true);
        streetViewPanorama.animateTo(
                new StreetViewPanoramaCamera.Builder().orientation(new StreetViewPanoramaOrientation(20,20))
                        .zoom(streetViewPanorama.getPanoramaCamera().zoom)
                        .build(), 2000
        );
        streetViewPanorama.setOnStreetViewPanoramaCameraChangeListener(paranomaChangelistener);
    }

    private StreetViewPanorama.OnStreetViewPanoramaCameraChangeListener paranomaChangelistener = new StreetViewPanorama.OnStreetViewPanoramaCameraChangeListener() {
        @Override
        public void onStreetViewPanoramaCameraChange(StreetViewPanoramaCamera streetViewPanoramaCamera) {
//            Toast.makeText(this, "Location Updated", Toast.LENGTH_SHORT).show();
        }
    };


    @Override
    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
        // Log.e("onMethodCall", result.toString());
        switch (call.method) {
//            case "loadUrl":
//                String url = call.arguments.toString();
//                webView.loadUrl(url);
//                break;
            default:
                result.notImplemented();
        }

    }

    public View getView() {
        return finalView;
    }

    @Override
    public void dispose() {

    }



}


