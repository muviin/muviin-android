package com.muviin.muviin_app;

import android.content.Context;
import android.widget.RelativeLayout;

import io.flutter.embedding.android.FlutterFragmentActivity;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

public class StreetViewFactory extends PlatformViewFactory {

    private final PluginRegistry.Registrar mPluginRegistrar;
    private  final DartExecutor executor;
    private final RelativeLayout layout;

    public StreetViewFactory(PluginRegistry.Registrar registrar, DartExecutor exec, RelativeLayout lay) {
        super(StandardMessageCodec.INSTANCE);
        mPluginRegistrar = registrar;
        executor = exec;
        layout = lay;
    }

    @Override
    public PlatformView create(Context context, int i, Object args) {
        return new StreetView(context, mPluginRegistrar, i, args, executor, layout);
    }
}
