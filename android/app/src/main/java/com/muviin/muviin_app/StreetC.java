package com.muviin.muviin_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewPanoramaOrientation;
import com.google.android.gms.maps.model.StreetViewSource;


public class StreetC extends Activity implements OnStreetViewPanoramaReadyCallback {

    private StreetViewPanorama streetViewPanorama;
    private boolean secondLocation = false;

    String latitude;
    String longitude;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);

        // get cordinates and assign
        Intent intent = getIntent();
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        System.out.println("SYSTEM CORDINATE LATITUDE");
        System.out.println(Double.parseDouble(latitude));


        StreetViewPanoramaView vv = new StreetViewPanoramaView(this);
        vv.onCreate(savedInstanceState);
        vv.getStreetViewPanoramaAsync(this);
        RelativeLayout dd = findViewById(R.id.googlemapstreetview);
        dd.addView(vv);




    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        this.streetViewPanorama = streetViewPanorama;
        if(secondLocation){
            streetViewPanorama.setPosition(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)), StreetViewSource.OUTDOOR);
//            streetViewPanorama.setPosition(new LatLng(5.52887, -0.1726), StreetViewSource.OUTDOOR);
        }
        else{
            streetViewPanorama.setPosition(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)),  StreetViewSource.OUTDOOR);
//            streetViewPanorama.setPosition(new LatLng(5.52887, -0.1726), StreetViewSource.OUTDOOR);
        }
        streetViewPanorama.setStreetNamesEnabled(true);
        streetViewPanorama.setPanningGesturesEnabled(true);
        streetViewPanorama.setZoomGesturesEnabled(true);
        streetViewPanorama.animateTo(
                new StreetViewPanoramaCamera.Builder().orientation(new StreetViewPanoramaOrientation(20,20))
                        .zoom(streetViewPanorama.getPanoramaCamera().zoom)
                        .build(), 2000
        );
        streetViewPanorama.setOnStreetViewPanoramaCameraChangeListener(paranomaChangelistener);
    }

    private StreetViewPanorama.OnStreetViewPanoramaCameraChangeListener paranomaChangelistener = new StreetViewPanorama.OnStreetViewPanoramaCameraChangeListener() {
        @Override
        public void onStreetViewPanoramaCameraChange(StreetViewPanoramaCamera streetViewPanoramaCamera) {
//            Toast.makeText(this, "Location Updated", Toast.LENGTH_SHORT).show();
        }
    };

}
