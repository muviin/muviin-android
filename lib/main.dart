import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/notification_bloc.dart';
import 'package:rentme_ghana/src/blocs/verification_block.dart';
import 'package:rentme_ghana/src/model/sockets.dart';
import 'package:rentme_ghana/src/persistence/base_persistence.dart';
import 'package:rentme_ghana/src/settings/notificztion_manager.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/get_started_page.dart';
import 'package:rentme_ghana/src/ui/setup_page.dart';
import 'package:rentme_ghana/src/ui/street_view_page.dart';
import 'package:rentme_ghana/src/ui/test_ui.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

// void main() => runApp(MyApp());
var messageShow = 0;
var messageCount = 0;
SocketConnection connection = SocketConnection();
Future<void> main() async {


  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());

}

class MyApp extends StatelessWidget {


  UserSettings _userSettings;
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey(debugLabel: "Main Navigator");

  Future<bool> _runBeforeBuild() async {
    // get shared preferences for UserSettings
    final prefs = await SharedPreferences.getInstance();

    // init UserSettings
    _userSettings = UserSettings(prefs);

      try {
        // connect to sockets
        // connection.connect();
        if(!database.isReady) {
          await DataPersistence.initDatabase(userSettings: _userSettings);
        }
      } catch(ex) {
        // Utils.log(ex);
        // show some error message
        Utils.log("err");

      }

    return true;
  }
    @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    
    return FutureBuilder(
      future: _runBeforeBuild(), //SharedPreferences.getInstance(),
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasData && snapshot.data != null) {
          // final UserSettings _userSettings = UserSettings(snapshot.data);
          final NotificationsBloc _notificationsBloc = NotificationsBloc();
          final NotificationManager _notificationManager = NotificationManager();
           _notificationManager.initialize(context, _notificationsBloc, _userSettings,navigatorKey);

          return MultiProvider(
            providers: [
              ChangeNotifierProvider(builder: (_) => _userSettings),

            ],
            child:     MaterialApp(
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                fontFamily: 'Roboto',
              ),
              home: Scaffold(
                body: Container(
                  child: Container(
                    width: double.infinity,
                    child: _determineHomeView(_userSettings),
                  ),
                ),
              ),
              builder: EasyLoading.init(),
            ),
          );


        }
        return Center();
      }
    );
  }

      Widget _determineHomeView(UserSettings userSettings) {
      return SetupPage();
      

    }
}

