import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/main.dart';
import 'package:rentme_ghana/src/blocs/notification_bloc.dart';
import 'package:rentme_ghana/src/model/fcm_payload.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class NotificationManager {

  static const String PUSH_TYPE_NOTIFY = "notify";
  static const String PUSH_TYPE_ANNOUNCE = "announce";

  static final int _notificationIdPushTypeActivity = 58;
  static final int _notificationIdPushTypeAnnouncement = 27;

  int activitiesCount = 0;

  var activitiesInboxLines = List<String>();
  BuildContext context;
  GlobalKey<NavigatorState> navigatorKey;
  var activity;
  Map<String, dynamic> actualAct;

  NotificationsBloc _notificationsBloc;
  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

  NotificationManager();

  UserSettings _userSettings;

  initialize(BuildContext context, NotificationsBloc notificationsBloc, UserSettings userSettings,GlobalKey<NavigatorState> navigatorKey) {
    this.context = context;
    this.navigatorKey = navigatorKey;
    this._notificationsBloc = notificationsBloc;
    this._userSettings = userSettings;
    _setupFcmMessaging();
    _setupLocalNotifications(context);
    _configureOnMessageReceivedFcm();
  }

  _setupFcmMessaging() {
    //ios
    _firebaseMessaging.requestNotificationPermissions(
      // const IosNotificationSettings(sound: false, alert: true, badge: true)
      IosNotificationSettings(sound: true, alert: true, badge: true)
    );
    _firebaseMessaging.onIosSettingsRegistered.listen((settings){
        // Utils.log("settings registered: $settings");
    });
  }

  _configureOnMessageReceivedFcm() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> payload){
        Utils.log(">>> FCM Message Received in onMessage: $payload");
        // activateRestart(Map<String, dynamic>.from(payload['data']));

        if (Platform.isAndroid) {
          _showAndroidNotification(payload);
        } else if (Platform.isIOS) {
          _showIOSNotification(payload);
        }
      },
      onResume: (Map<String, dynamic> payload){
        Utils.log(">>> FCM Message Received in onResume: $payload");
        
        if (Platform.isAndroid) {
          _showAndroidNotification(payload);
        } else if (Platform.isIOS) {
          _showIOSNotification(payload);
        }
      },
      onLaunch: (Map<String, dynamic> payload){
        Utils.log(">>> FCM Message Received in onLaunch: $payload ");
        
        if (Platform.isAndroid) {
          _showAndroidNotification(payload);
        } else if (Platform.isIOS) {
          _showIOSNotification(payload);
        }
      },
    );
  }

  _setupLocalNotifications(BuildContext context) {
    var initializationSettingsAndroid = new AndroidInitializationSettings('@mipmap/launcher_icon');
    var initializationSettingsIOS = new IOSInitializationSettings(defaultPresentSound: true,onDidReceiveLocalNotification: _onDidReceiveLocalNotification);
    var initializationSettings = new InitializationSettings(android: initializationSettingsAndroid,iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: _onSelectNotification);
  }

  void _showAndroidNotification(Map<String, dynamic> payload) async {
    Utils.log(">>> Show Notificatioin, Payload Data: ${payload['data']}");

    activity = payload['data']; // IF WE ARE SENDING FROM BACKEND, WE MODIFY IT
    actualAct = Map<String, dynamic>.from(payload['data']);
    //incase method doesnt get called in the onMessage
    // activateRestart(actualAct);

    if (payload['data'] != null) {

      var notification = payload['notification'].cast<String, dynamic>();
      var data = payload['data'].cast<String, dynamic>();
      var type = data["type"].toString();
      var fcmPayload = FcmPayload()
        ..title =  notification["title"].toString()
        ..description = notification["body"].toString();

      if(messageShow == 0){
        /// [messageShow] is 0, meaning user hasnt opened messages screen
        /// so we sent fcm
        /// if user has opened that, we dont receive that
          switch (type) {
            case PUSH_TYPE_ANNOUNCE:
              announce(fcmPayload);
              break;
            case PUSH_TYPE_NOTIFY:
              _notify(fcmPayload);
              break;
            default:
          }
        
      }
      // else{
      //    _notify(fcmPayload);
      // }
    }
  }

  void _showIOSNotification(Map<String, dynamic> payload) async {
    Utils.log(">>> Show Notificatioin, Payload Data: $payload");

    activity = payload['data'];
    actualAct = Map<String, dynamic>.from(payload['data']);

    if (FcmPayload != null) {

      var notification = payload['notification'].cast<String, dynamic>();
      var data = payload['data'].cast<String, dynamic>();
      var type = data["type"].toString();
      var fcmPayload = FcmPayload()
        ..title =  notification["title"].toString()
        ..description = notification["body"].toString();
      
      switch (type) {
        case PUSH_TYPE_ANNOUNCE:
          announce(fcmPayload);
          break;
        case PUSH_TYPE_NOTIFY:
          _notify(fcmPayload);
          break;
        default:
      }
    }
  }

  Future _notify(FcmPayload fcmPayload) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'com.rentme.rentme_ghana.GENERAL', 'General', 'For general notifications',
        importance: Importance.high, priority: Priority.high, ticker: fcmPayload.title,
        playSound: true,
        // sound: "Default_Sound"
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails(
      presentSound: true,
      // sound: "Default_Sound"
    );
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics, 
        iOS:iOSPlatformChannelSpecifics
      );

    await flutterLocalNotificationsPlugin.show(
      _notificationIdPushTypeAnnouncement,
      fcmPayload.title,
      fcmPayload.description,
      platformChannelSpecifics,
      payload: "fcmPayload",
    );

    // _notificationsBloc.increment(); //TODO:

  }

  void _subscribeToTopic(FcmPayload fcmPayload) async {
    if (Utils.isEmptyOrNull(fcmPayload.topic)) {
      _firebaseMessaging.subscribeToTopic(fcmPayload.topic);
    }
  }

  Future<void> announce(FcmPayload fcmPayload) async {
    var count = 0;
    var lines;
    String contentTitle = "";

    activitiesCount++;
    count = activitiesCount;
    activitiesInboxLines.add(fcmPayload.description);
    lines = activitiesInboxLines;
    contentTitle = "New activity notifications";

    var inboxStyleInformation = InboxStyleInformation(
      lines,
      htmlFormatLines: true,
      contentTitle: contentTitle,
      htmlFormatContentTitle: true,
      summaryText: "RentMe Ghana",
      htmlFormatSummaryText: true
    );

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'com.rentme.rentme_ghana.ACTIVITIES',
      'App Activities', 'For specific activity notifications',
      
      // style: AndroidNotificationStyle.Inbox,
      styleInformation: inboxStyleInformation
    );

    var platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics);

    int notificationId = 100;
    String inboxBody = count.toString() + " new activity " + (count == 1 ? "notification" : "notifications");
    
    notificationId = _notificationIdPushTypeActivity;
  
    await flutterLocalNotificationsPlugin.show(
      notificationId,
      fcmPayload.title,
      inboxBody,
      platformChannelSpecifics
    );

    // _notificationsBloc.increment(); //TODO:
  }

  Future _onSelectNotification(String payload,) async {
    if (payload != null) {
      Utils.log(">>> Notificatioin Selected, Payload: $payload");
      Utils.log(">>>> YO : $activity");
    }

    Utils.log(">>>Opening");
    // if(_userSettings.isLoggedIn()){
    //   this.navigatorKey.currentState.push(
    //     MaterialPageRoute(builder: (BuildContext context){
    //       return ActivitiesPage(openDetail: true,activity: actualAct,); //ActivitiesPage(openDetail: true,activity: _activity,);
    //     })
    //   );
    // }

  }

  Future<void> _onDidReceiveLocalNotification(int id, String title, String body, String payload) async {
    Utils.log(">>> Local Notifications Received on iOS, Payload: $payload ");
  }



  // this method gets called if app detects an fcm 
  // and its about kyc update
  // activateRestart(Map<String, dynamic> payload){
  //   // the payload is the data field from the fcm
  //   if(payload.isNotEmpty){ 

  //     String message = payload["title"].toString();
  //     if(message == "KYC Status" || message.contains("KYC Status") || message.toUpperCase() == "KYC STATUS" || message.contains("Your kyc Your kyc verification failed")){
  //       if(_userSettings.isLoggedIn()){
  //         // set all cached values relating to ID upload to false
  //         _userSettings.setIdUploaded(false);
  //         _userSettings.setIdScanResult(Map());
  //         this.navigatorKey.currentState.pushAndRemoveUntil(
  //           PageTransition(
  //             type: PageTransitionType.bottomToTop, 
  //             alignment: Alignment.bottomCenter,
  //             child: Provider<SetupBloc>(
  //               builder: (_) => SetupBloc(),
  //               dispose: (_, value) => value.dispose(),
  //               child: SetupPage(),
  //             ),
  //           ),
  //           (Route<dynamic> route) => false
  //         );
  //       }
  //     }
  //   }

  // }





}




