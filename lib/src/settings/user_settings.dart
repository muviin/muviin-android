
import 'package:flutter/material.dart';
import 'package:rentme_ghana/main.dart';
import 'package:rentme_ghana/src/model/country.dart';
import 'package:rentme_ghana/src/model/device.dart';
import 'package:rentme_ghana/src/model/message.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/settings/shared_settings.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserSettings extends SharedSettings with ChangeNotifier {

  static final String USER_SETTINGS = "user_settings";
  static final String IS_DEVICE_REGISTERED = "is_device_registered";
  static final String IS_ALL_INFO_FETCHED = "is_all_info_fetched";
  static final String IS_PIN_ENTERED = "is_pin_entered";
  static final String LANGUAGE = "language";
  static final String COUNTRY = "country";
  static const String COUNTRY_LIST = "country_list";
  static final String USER = "user";
  static final String TOKEN = "token";
  static final String CLIENT_TOKEN = "client_token";
  static final String DEVICE = "device";
  static final String ACTIVATE_PIN = "push";
  static final String PROFILE_PIC= "profile_pic";
  static final String TOGGLE_MAP= "toggle_map";
  static final String CART= "cart";
  static final String LOCATION = "location";
  static final String RENTS = "rents";
  static final String USER_POST = "user_post";
  static const String COUNTRIES = "countries";
  static const String MESSAGES = "messages";
  static const String SIMILAR_RENTS = "similar_rents";



  UserSettings(SharedPreferences prefs) : super(prefs);


  void setIsDeviceRegistered(bool isDeviceRegistered) {
    setAndSaveBool(IS_DEVICE_REGISTERED, isDeviceRegistered);
    notifyListeners();
  }

  bool isDeviceRegistered() {
    return getBoolFromPrefsDefaultFalse(IS_DEVICE_REGISTERED);
  }

  void setIsAllInfoFetched(bool isAllInfoFetched) {
    setAndSaveBool(IS_ALL_INFO_FETCHED, isAllInfoFetched);
    notifyListeners();
  }

  bool isAllInfoFetched() {
    return getBoolFromPrefsDefaultFalse(IS_ALL_INFO_FETCHED);
  }

  void setIsPinEntered(bool isPinEntered) {
    setAndSaveBool(IS_PIN_ENTERED, isPinEntered);
  }

  bool isPinEntered() {
    return getBoolFromPrefsDefaultFalse(IS_PIN_ENTERED);
  }

  // void setCountry(Country country) {
  //   setAndSaveObject(COUNTRY, country.toJson());
  // }

  // Country getCountry() {
  //   Map<String, dynamic> countryMap = getObjectFromPrefs(COUNTRY);
  //   if (countryMap != null) {
  //     return Country.fromJson(countryMap);
  //   } else {
  //     return Country();
  //   }
  // }

  void setLanguage(String language) {
    setAndSaveString(LANGUAGE, language);
  }

  String getLanguage() {
    String preferedLanguage = getStringFromPrefs(LANGUAGE);
    return !Utils.isEmptyOrNull(preferedLanguage) ? preferedLanguage : "English";
  }

  void setUser(User user) {
    setAndSaveObject(USER, user.toJson());
  }

  User getUser() {
    Map<String, dynamic> userMap = getObjectFromPrefs(USER);
    if (userMap != null) {
      return User.fromJson(userMap);
    } else {
      return User();
    }
  }

  // void setDevice(Device device) {
  //   setAndSaveObject(DEVICE, device.toJson());
  // }

  // Device getDevice() {
  //   Map<String, dynamic> deviceMap = getObjectFromPrefs(DEVICE);
  //   if (deviceMap != null) {
  //     return Device.fromJson(deviceMap);
  //   } else {
  //     return Device();
  //   }
  // }

  setCountries(List<Country> countries){
    setAndSaveList(COUNTRIES, countries);
  }
  List<dynamic> getCountriess(){
    var countries = getListFromPrefs(COUNTRIES);
    return countries;
  }


  void setPin(bool val){
    setAndSaveBool(ACTIVATE_PIN, val);
  }
  void toggleMap(bool val){
    setAndSaveBool(TOGGLE_MAP, val);
  }
  bool isPinSet() {
    return getBoolFromPrefsDefaultFalse(ACTIVATE_PIN);
  }

  bool isMapToggled() {
    return getBoolFromPrefsDefaultFalse(TOGGLE_MAP);
  }

  setRents(List<Rents> rents){
    setAndSaveList(RENTS, rents);
  }
  List<dynamic> getRents(){
    var rents = getListFromPrefs(RENTS);
    return rents;
  }

  setSimilarRents(List<Rents> rents){
    setAndSaveList(SIMILAR_RENTS, rents);
  }
  List<dynamic> getSimilarRents(){
    var rents = getListFromPrefs(SIMILAR_RENTS);
    return rents;
  }



  setMessages(List<Message> rents){
    setAndSaveList(MESSAGES, rents);
  }
  List<dynamic> getMessages(){
    var rents = getListFromPrefs(MESSAGES);
    return rents;
  }

  setUserPosts(List<Rents> rents){
    setAndSaveList(USER_POST, rents);
  }
  List<dynamic> getUserPosts(){
    var rents = getListFromPrefs(USER_POST);
    return rents;
  }

  void setLocation(String location){
    setAndSaveString(LOCATION, location);
  }

  String getLocation(){
    return getStringFromPrefs(LOCATION);
  }

    void setCountry(Country country) {
      setAndSaveObject(COUNTRY, country.toJson());
  }

    Country getCountry() {
    Map<String, dynamic> countryMap = getObjectFromPrefs(COUNTRY);
    if (countryMap != null) {
      return Country.fromJson(countryMap);
    } else {
      return Country();
    }
  }

  Map<String, Country> getCountries() {
    Map<String, dynamic> map = getObjectFromPrefs(COUNTRY_LIST);
    if(map != null) {
      return map.map((key, value) => MapEntry(key, Country.fromJson(value)));
    } else {
      return {};
    }
  }

  void setDevice(Device device) {
    setAndSaveObject(DEVICE, device.toJson());
  }
  Device getDevice() {
    Map<String, dynamic> deviceMap = getObjectFromPrefs(DEVICE);
    if (deviceMap != null) {
      return Device.fromJson(deviceMap);
    } else {
      return Device();
    }
  }
  






  logout(BuildContext context) {
    setIsDeviceRegistered(false);
    setIsAllInfoFetched(false);
    setIsPinEntered(false);
    setLanguage("");
    setUser(User());
    setLocation("");
    setRents([Rents()]);
    setMessages([Message()]);
    setUserPosts([Rents()]);
    setCountries([Country()]);
    setDevice(Device());
    setIsDeviceRegistered(false);
    setSimilarRents([Rents()]);
                                
    Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(
        builder: (context) {
          return MyApp();
        },
      ),
      (Route<dynamic> route) => false
    );
  }
}