
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:phone_number/phone_number.dart';
import 'package:rentme_ghana/src/model/country.dart';
import 'package:rentme_ghana/src/model/phone.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VerificationUtils {

  static Future<String> formatPhoneNumber(String phone, String countryCode, {PhoneFormat format = PhoneFormat.E164}) async {
    if (Utils.isEmptyOrNull(phone)) {
      return '';
    }

    if(kIsWeb) {
      // TODO WEB PHONE VALIDATION
      return phone;
    }
      
    try {
      final value = await PhoneNumber().parse(phone, region: countryCode);
      if (PhoneFormat.INTERNATIONAL == format) {
        return value['international'];
      } else if (PhoneFormat.NATIONAL == format) {
        return value['national'];
      } else if (PhoneFormat.E164 == format) {
        return value['e164'];
      }
    } catch (e) {
    }
    
    return '';
  }

  static Future<Country> getCountryFromPhone(String phone, String countryCode) async {
    if (Utils.isEmptyOrNull(phone)) {
      return null;
    }
      
    try {
      final parsedPhone = await parsePhoneNumber(phone, countryCode);
      final prefs = await SharedPreferences.getInstance();
      final userSettings = UserSettings(prefs);
      // find particular country
      return userSettings.getCountries()[parsedPhone.type];
    } catch (e) {
    }
    
    return null;
  }

  static Future<ParsedPhone> parsePhoneNumber(String phone, String countryCode) async {
    if (Utils.isEmptyOrNull(phone)) {
      return null;
    }

    if(kIsWeb) {
      // TODO WEB PHONE VALIDATION
      return ParsedPhone.fromJson({
        'country_code': '233',
        'e164': '233$phone',
        'national': phone,
        'type': '',
        'international': '+233$phone',
        'national_number': '024'
      });
    }
      
    try {
      final value = await PhoneNumber().parse(phone, region: countryCode);
      return ParsedPhone.fromJson(value);
    } catch (e) {
    }
    
    return null;
  }

    static formatOtp(BuildContext context, {String prefix, String suffix, onSuccess(value), onFailure(reason)}) async {

    if (Utils.isEmptyOrNull(prefix)) {
      onFailure("Empty otp prefix");
      return;
    }

    if (Utils.isEmptyOrNull(suffix)) {
      onFailure("empty otp suffix");
      // onFailure('OTP suffix is empty.');
      return;
    }

    try {
      String otpValue = "$prefix-$suffix";
      onSuccess(otpValue);
    } catch (e) {
      onFailure("otp cant be formatted");
      // onFailure('The OTP could not be formatted.');
    }
  }

  // static validateEmail(BuildContext context, {String email, onSuccess(value), onFailure(reason)}) async {

  //   if (Utils.isEmptyOrNull(email)) {
  //     onFailure(AppLocalizations.of(context).emptyEmail);
  //     // onFailure('Email is empty.');
  //     return;
  //   }
    
  //   try {
  //     bool valid = RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$').hasMatch(email.toLowerCase());
  //     if (valid)
  //       onSuccess(email.trim());
  //     else
  //       onFailure(AppLocalizations.of(context).invalidMail);
  //       // onFailure('Invalid email');
  //   } catch (e) {
  //     onFailure(AppLocalizations.of(context).invalidMail);
  //     // onFailure('Invalid email');
  //   }
  // }

  // static pushPermissionCheck({onSuccess(value), onFailure(reason)}) async{
  //   var status = await Permission.location.serviceStatus.isEnabled;
  //   var request = await Permission.location.request();
  //   if (!status && request.isGranted) {
  //     // location is turned off, bh apps permission to use location is granted
  //     // prompt user to turn on location
  //     onFailure(PermissionState.ENABLE);
  //   }
  //   else if (status && request.isDenied){
  //     // request for permission
  //     onFailure(PermissionState.REQUEST);
  //   }
  //   else{
  //     onSuccess(true);
  //   }
  // }

  // static Future<Device> buildDevice() async {
  //   UserSettings userSettings = UserSettings(await SharedPreferences.getInstance());
    
  //   String fcmToken = await FirebaseMessaging().getToken();
  //   Utils.log(">>> FCM Token: $fcmToken");

  //   DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  //   Device device = userSettings.getDevice();
  //   device.userPhone = userSettings.getUser().phone;

  //   if (Platform.isAndroid) {
  //     AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      
  //     device.fcmToken = fcmToken;
  //     device.model = androidInfo.model;
  //     device.brand = androidInfo.brand;
  //     device.manufacturer = androidInfo.manufacturer;
  //     device.osInfo = "Android SDK INT ${androidInfo.version.sdkInt}";

  //   } else if (Platform.isIOS) {
  //     IosDeviceInfo iosInfo = await deviceInfo.iosInfo;

  //     device.fcmToken = fcmToken;
  //     device.model = iosInfo.model;
  //     device.brand = iosInfo.name;
  //     device.manufacturer = "Apple";
  //     device.osInfo = "${iosInfo.systemName} ${iosInfo.systemVersion}";
  //   }

  //   userSettings.setDevice(device);
  //   return device;
  // }

  

}

