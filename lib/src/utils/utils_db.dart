
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:sqlcool/sqlcool.dart';


/// SQL COOL IMPLEMENTATION OF TABLE SCHEMA, REWRITTEN
class DbMigration {

  /// Name of the table: no spaces
  final String tableName;

  /// Describe (in snake case) what the migration is about
  final String description;

  /// Default constructor
  DbMigration(this.tableName, this.description) : assert(tableName != null);

  final List<String> _columns = <String>[];
  final List<String> _columnsNew = <String>[];
  final List<String> _columnsChange = <String>[];
  final List<String> _queries = <String>[];
  final List<DbColumn> _columnsData = <DbColumn>[];
  final List<String> _fkConstraints = <String>[];

  /// The columns info
  List<DbColumn> get columns => _columnsData;

  /// The foreign key columns
  List<DbColumn> get foreignKeys => _foreignKeys();

  /// Get the list of queries to perform for database initialization
  List<String> get queries => _getQueries();

  /// Get the table constraints
  List<String> get constraints => _fkConstraints;

  /// Check if a column exists
  bool hasColumn(String name) => _hasColumn(name);

  /// Get a column by name
  DbColumn column(String name) {
    DbColumn col;
    for (final c in _columnsData) {
      if (c.name == name) {
        col = c;
        break;
      }
    }
    return col;
  }

  /// Add an index to a column
  ///
  /// If a [tableName] is given the index name will
  /// be set to it, otherwise it is infered from
  /// the column name
  void index(String column, {String indexName}) {
    var idxName = column;
    switch (indexName != null) {
      case true:
        idxName = indexName;
        break;
      default:
        idxName = "idx_$column";
    }
    final q = "CREATE UNIQUE INDEX IF NOT EXISTS $idxName ON $tableName($column)";
    _queries.add(q);
  }

  /// Add a unique constraint for combined values from two columns
  // void uniqueTogether(String column1, String column2) {
  //   final q = 'UNIQUE("$column1", "$column2")';
  // if(alter) {
  //     _columnsChange.add(q);
  //   } else {
  //     _columnsNew.add(q);
  //   }
  // }

  /// Add a foreign key to a column
  // void foreignKey(String name,
  //     {String reference,
  //     bool nullable = true,
  //     bool unique = false,
  //     bool alter = false,
  //     String defaultValue,
  //     OnDelete onDelete = OnDelete.restrict}) {
    
  //   // if nullable, defaultValue must be provided
  //   if(!nullable && defaultValue != null) {
  //     throw(Exception('Non-nulls should have a default value'));
  //   }

  //   var q = "$name INTEGER";
  //   if (unique) {
  //     q += " UNIQUE";
  //   }
  //   if (!nullable) {
  //     q += " NOT NULL";
  //   }
  //   if (defaultValue != null) {
  //     q += " DEFAULT $defaultValue";
  //   }
  //   String fk;
  //   fk = "  FOREIGN KEY ($name)\n";
  //   reference ??= name;
  //   fk += "  REFERENCES $reference(id)\n";
  //   fk += "  ON DELETE ";
  //   switch (onDelete) {
  //     case OnDelete.cascade:
  //       fk += "CASCADE";
  //       break;
  //     case OnDelete.setNull:
  //       fk += "SET NULL";
  //       break;
  //     case OnDelete.setDefault:
  //       fk += "SET DEFAULT";
  //       break;
  //     default:
  //       fk += "RESTRICT";
  //   }
  // if(alter) {
  //     _columnsChange.add(q);
  //   } else {
  //     _columnsNew.add(q);
  //   }
  //   _fkConstraints.add(fk);
  //   _columnsData.add(DbColumn(
  //       name: name,
  //       unique: unique,
  //       nullable: nullable,
  //       defaultValue: defaultValue,
  //       type: DbColumnType.integer,
  //       isForeignKey: true,
  //       reference: reference,
  //       onDelete: onDelete));
  // }

  /// Add a varchar column
  void varchar(String name,
      {int maxLength,
      bool nullable = true,
      bool unique = false,
      bool alter = false,
      String defaultValue,
      String check}) {
        
    var q = "$name VARCHAR";
    
    // if nullable, defaultValue must be provided
    if(!nullable && defaultValue != null) {
      throw(Exception('Non-nulls should have a default value'));
    }

    if (maxLength != null) {
      q += "($maxLength)";
    }
    if (unique) {
      q += " UNIQUE";
    }
    if (!nullable) {
      q += " NOT NULL";
    }
    if (defaultValue != null) {
      q += " DEFAULT $defaultValue";
    }
    if (check != null) {
      q += " CHECK($check)";
    }

    if(alter) {
      _columnsChange.add(q);
    } else {
      _columnsNew.add(q);
    }

    _columnsData.add(DbColumn(
        name: name,
        unique: unique,
        nullable: nullable,
        defaultValue: defaultValue,
        check: check,
        type: DbColumnType.varchar));
  }

  /// Add a text column
  void text(String name,
      {bool nullable = true,
      bool unique = false,
      bool alter = false,
      String defaultValue,
      String check}) {
    
    // if nullable, defaultValue must be provided
    if(!nullable && defaultValue != null) {
      throw(Exception('Non-nulls should have a default value'));
    }

    var q = "$name TEXT";
    if (unique) {
      q += " UNIQUE";
    }
    if (!nullable) {
      q += " NOT NULL";
    }
    if (defaultValue != null) {
      q += " DEFAULT $defaultValue";
    }
    if (check != null) {
      q += " CHECK($check)";
    }

    if(alter) {
      _columnsChange.add(q);
    } else {
      _columnsNew.add(q);
    }
    _columnsData.add(DbColumn(
        name: name,
        unique: unique,
        nullable: nullable,
        defaultValue: defaultValue,
        check: check,
        type: DbColumnType.text));
  }

  /// Add a float column
  void real(String name,
      {bool nullable = true,
      bool unique = false,
      bool alter = false,
      double defaultValue,
      String check}) {
    
    // if nullable, defaultValue must be provided
    if(!nullable && defaultValue != null) {
      throw(Exception('Non-nulls should have a default value'));
    }

    var q = "$name REAL";
    if (unique) {
      q += " UNIQUE";
    }
    if (!nullable) {
      q += " NOT NULL";
    }
    if (defaultValue != null) {
      q += " DEFAULT $defaultValue";
    }
    if (check != null) {
      q += " CHECK($check)";
    }

    if(alter) {
      _columnsChange.add(q);
    } else {
      _columnsNew.add(q);
    }
    _columnsData.add(DbColumn(
        name: name,
        unique: unique,
        nullable: nullable,
        defaultValue: "$defaultValue",
        check: check,
        type: DbColumnType.real));
  }

  /// Add an integer column
  void integer(
    String name, {
    bool nullable = true,
    bool unique = false,
    bool alter = false,
    int defaultValue,
    String check,
  }) {
    
    // if nullable, defaultValue must be provided
    if(!nullable && defaultValue != null) {
      throw(Exception('Non-nulls should have a default value'));
    }

    var q = "$name INTEGER";
    if (unique) {
      q += " UNIQUE";
    }
    if (!nullable) {
      q += " NOT NULL";
    }
    if (defaultValue != null) {
      q += " DEFAULT $defaultValue";
    }
    if (check != null) {
      q += " CHECK($check)";
    }

    if(alter) {
      _columnsChange.add(q);
    } else {
      _columnsNew.add(q);
    }
    _columnsData.add(DbColumn(
        name: name,
        unique: unique,
        nullable: nullable,
        defaultValue: "$defaultValue",
        check: check,
        type: DbColumnType.integer));
  }

  /// Add a float column
  void boolean(String name, {@required bool defaultValue, bool alter = false}) {
    var q = "$name BOOLEAN";
    q += " DEFAULT $defaultValue";

    if(alter) {
      _columnsChange.add(q);
    } else {
      _columnsNew.add(q);
    }
    _columnsData.add(DbColumn(
        name: name, defaultValue: "$defaultValue", type: DbColumnType.boolean));
  }

  /// Add a blob column
  void blob(
    String name, {
    bool nullable = true,
    bool unique = false,
    bool alter = false,
    Uint8List defaultValue,
    String check,
  }) {
    
    // if nullable, defaultValue must be provided
    assert(nullable && defaultValue != null);

    var q = "$name BLOB";
    if (unique) {
      q += " UNIQUE";
    }
    if (!nullable) {
      q += " NOT NULL";
    }
    if (defaultValue != null) {
      q += " DEFAULT $defaultValue";
    }
    if (check != null) {
      q += " CHECK($check)";
    }

    if(alter) {
      _columnsChange.add(q);
    } else {
      _columnsNew.add(q);
    }
    _columnsData.add(DbColumn(
        name: name,
        unique: unique,
        nullable: nullable,
        defaultValue: "$defaultValue",
        check: check,
        type: DbColumnType.blob));
  }

  /// drop a column
  void delete(String name) {
    var q = "ALTER TABLE $tableName DROP COLUMN $name;";

    _columns.add(q);
    // _columnsData.remove(column(name));
  }

  /// Add an automatic timestamp
  void timestamp(String column, {bool alter = false}) {
    final q = "$column TIMESTAMP";

    if(alter) {
      _columnsChange.add(q);
    } else {
      _columnsNew.add(q);
    }
    _columnsData.add(DbColumn(name: tableName, type: DbColumnType.timestamp));
  }

  /// Print the queries to perform for database initialization
  void printQueries() {
    queries.forEach((q) {
      print("----------");
      print(q);
    });
  }

  /// print a description of the schema
  void describe({String spacer = ""}) {
    print("${spacer}Table $tableName:");
    for (final column in columns) {
      column.describe(spacer: "  ");
    }
  }

  @override
  String toString() => tableName;

  /// The string for the table create query
  String queryString() {
    if(_columnsNew.isNotEmpty || _columnsChange.isNotEmpty) {
      var queryNew = 'ALTER TABLE $tableName';

      if(_columnsNew.isNotEmpty) {
        queryNew += ' ADD ' +
          _columnsNew.join(' ');

        _columns.add('$queryNew;');
      }

      var queryChange = 'ALTER TABLE $tableName';

      if(_columnsChange.isNotEmpty) {
        queryChange += ' ALTER COLUMN ' +
          _columnsNew.join(' ');

        _columns.add('$queryChange;');
      }


    }

    if (_columns.isNotEmpty) {
      final String query = _columns.join("\n");
    // if (_fkConstraints.isNotEmpty) {
    //   q += ";\n";
    //   q += _fkConstraints.join(";\n");
    // }
      return query;
    }
    
    return '';
  }

  List<String> _getQueries() {
    final qs = <String>[this.queryString(), ..._queries];
    Utils.log('QUERY' + qs.toString());
    return qs;
  }

  List<DbColumn> _foreignKeys() {
    final fks = <DbColumn>[];
    _columnsData.forEach((col) {
      if (col.isForeignKey) {
        fks.add(col);
      }
    });
    return fks;
  }

  bool _hasColumn(String name) {
    final hasCol = column(name);
    if (hasCol == null) {
      return false;
    }
    return true;
  }
}


class DatabaseQuery {
  List<String> _queries = [];

  DatabaseQuery();

  String get query => _queries.join(' ');
  
  bool get isEmpty => _queries.isEmpty;
  bool get isNotEmpty => _queries.isNotEmpty;

  void where(String column, dynamic value, [QueryGate linker = QueryGate.AND]) {
    _checks(value);
      
    String subquery = '';
    if(_queries.isNotEmpty) {
      subquery += ' ${Utils.enumToString(linker)} ';
    }

    subquery += '$column = "$value"';
    _queries.add(subquery);
  }

  void group(DatabaseQuery groupQuery, [QueryGate linker = QueryGate.AND]) {
    String subquery = '';
    if(_queries.isNotEmpty) {
      subquery += ' ${Utils.enumToString(linker)} ';
    }

    subquery += '(${groupQuery.query})';
    _queries.add(subquery);
  }

    void whereNull(String column, [QueryGate linker = QueryGate.AND]) {
    String subquery = '';
    if(_queries.isNotEmpty) {
      subquery += ' ${Utils.enumToString(linker)} ';
    }

    subquery += "($column IS NULL OR $column = 'null')";
    _queries.add(subquery);
  }

  void like(String column, String value, [QueryGate linker = QueryGate.AND]) {
    _checks(value);
  
    String subquery = '';
    if(_queries.isNotEmpty) {
      subquery += ' ${Utils.enumToString(linker)} ';
    }

    subquery += '$column LIKE "%$value%"';
    _queries.add(subquery);
  }

  void whereNotNull(String column, [QueryGate linker = QueryGate.AND]) {
    String subquery = '';
    if(_queries.isNotEmpty) {
      subquery += ' ${Utils.enumToString(linker)} ';
    }

    subquery += '$column IS NOT NULL';
    _queries.add(subquery);
  }

  void _checks(value) {
    if(!(value is String) && !(value is int) && !(value is bool) && !(value is double)) {
      throw('Should pass a primitive data type for query values');
    }
  }

}