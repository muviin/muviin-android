
// extension MapParsing on Map<String, String> {
//   String toQueryParameters() {
//     return Utils.toQueryString(this);
//   }
// }

// /// extend a value rounder on [double]s
// extension ValueRounderToDoubleExtension on num {
//   /// truncate the decimal place of the double by [limit] / [Constants.DECIMAL_PLACES]
//   double toFixed([int limit = Constants.DECIMAL_PLACES]) {
//     String theNum = this.toString();
//     if(theNum.indexOf('.') > -1) {
//       // has decimal
//       String otherHalf = theNum.substring(theNum.indexOf('.') + 1);
//       if(otherHalf.length > limit) {
//         otherHalf = otherHalf.substring(0, limit);
//       }
//       return num.parse(theNum.substring(0, theNum.indexOf('.')) + '.' + otherHalf);
//     } else {
//       return num.parse('$this.0');
//     }
//     // return Utils.parseDouble(this.toStringAsFixed(limit));
//   }
// }
