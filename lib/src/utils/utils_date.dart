import 'dart:core';
import 'package:intl/intl.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class DateUtils {
  
  static bool isTokenExpired(String expiry) {
    try {
      // final expiryDate = DateFormat("MMM dd, yyyy, hh:mm:ss a", 'en').parse(expiry);
      final expiryDate = DateFormat("MMM dd, yyyy, hh:mm:ss a").parse(expiry);
      final now = DateTime.now();
      final difference = expiryDate.difference(now).inDays;
      return difference <= 5;
    } catch (e) {
      Utils.log(e);
      return true;
    }
  }

  static String getReadableDate(String dateString, {String locale = 'en'}) {
    return formatDateTimeString(dateString, "d MMM, yyyy.", locale: locale);
  }

  static String getReadableDateAndTime(String dateString, {String locale = 'en'}) {
    return formatDateTimeString(dateString, "d MMM, yyyy HH:mm", locale: locale);
  }

  static DateTime parseDateTimeString(String dateString, {String sourceFormat}) {
    final List<String> possibleFormats = const [
      Constants.DEFAULT_TIMESTAMP_FORMAT,
      'yyyy-MM-dd',
      'yyyy-MM-dd HH:mm:ss.SSS',
      'yyyy-MM-dd HH:mm:ss.SSSS',
      'yyyy-MM-ddTHH:mm:ss',
      'yyyy-MM-dd"T"HH:mm:ss"Z"',
      'MMM dd, yyyy hh:mm:ss a',
    ];

    // initializeDateFormatting('en');

    if(sourceFormat != null) {
      // use that format instead
      return DateFormat(sourceFormat).parseUTC(dateString);
    }

    // try local parsing
    if(dateString.contains('T')) {
      try {
        return DateTime.parse(dateString);
      } catch (e) {
        // Utils.log('DATE => ' + e.toString());
      }
    }

    for(var possibleFormat in possibleFormats) {
      try {
        return DateFormat(possibleFormat).parseUTC(dateString);
      } catch (e) {
        // Utils.log('DATE => ' + e.toString());
      }
    }
    Utils.log('Date format not in possible dates formats array: $dateString');
    return null;
  }

  static DateTime parse(dynamic object, {String format}) {
    if(object is String) {
      return parseDateTimeString(object, sourceFormat: format);
    }

    if(object is int) {
      return parseEpoch(object);
    }

    return null;
  }

  static String formatDateTimeString(String dateString, String format, {String locale = 'en'}) {
    final parsedDate = parseDateTimeString(dateString);
    return parsedDate != null ? DateFormat(format, locale).format(parsedDate) : '';
  }

  static DateTime parseEpoch(int milliseconds) {
    return DateTime.fromMillisecondsSinceEpoch(milliseconds * 1000);
  }

  static String formatDateTime(DateTime date, {String format = Constants.DEFAULT_TIMESTAMP_FORMAT, String locale = 'en'}) {
    return DateFormat(format, locale).format(date);
  }

  static int toUnixTimestamp(DateTime dateTime) {
    return (dateTime.millisecondsSinceEpoch / 1000).ceil();
  }

  static bool isInSameMinute(DateTime date1, DateTime date2) {
    return date1.year == date2.year
      && date1.month == date2.month
      && date1.day == date2.day
      && date1.hour == date2.hour
      && date1.minute == date2.minute;
  }

  /// 
  /// strips off the time from a DateTime
  /// 
  static DateTime stripOffTimeFromDate(DateTime dateTime) {
    return DateTime(dateTime.year, dateTime.month, dateTime.day);
  }

  /// 
  /// converts a duration to time format mm:ss
  /// 
  static String printDuration(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    final twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    final twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  static String timeAgoSinceDate(String dateString, {bool numericDates = true, String format = "d-MMM-yyyy hh:mm a"}) {
    DateTime notificationDate = DateFormat(format).parse(dateString);
    final date2 = DateTime.now();
    final difference = date2.difference(notificationDate);

    if (difference.inDays > 8) {
      return dateString;
    } else if ((difference.inDays / 7).floor() >= 1) {
      return (numericDates) ? '1 week ago' : 'Last week';
    } else if (difference.inDays >= 2) {
      return '${difference.inDays} days ago';
    } else if (difference.inDays >= 1) {
      return (numericDates) ? '1 day ago' : 'Yesterday';
    } else if (difference.inHours >= 2) {
      return '${difference.inHours} hours ago';
    } else if (difference.inHours >= 1) {
      return (numericDates) ? '1 hour ago' : 'An hour ago';
    } else if (difference.inMinutes >= 2) {
      return '${difference.inMinutes} minutes ago';
    } else if (difference.inMinutes >= 1) {
      return (numericDates) ? '1 minute ago' : 'A minute ago';
    } else if (difference.inSeconds >= 3) {
      return '${difference.inSeconds} seconds ago';
    } else {
      return 'Just now';
    }
  }

}
