import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/res/dimens.dart';

class ResponsiveUtils {

  // static MediaQueryData _mediaQueryData;

  static double deviceWidth;
  static double deviceHeight;

  // block sizes
  static double safeBlockHorizontal;
  static double safeBlockVertical;
  static double _safeAreaHorizontal;
  static double _safeAreaVertical;

  // Small phones (5c)
  static const double XS = 390.0;

  // Medium phones
  static const double SM = 576.0;

  // Large phones (iPhone X)
  // static const double MD = 768.0;
  static const double MD = 900.0;

  // Tablets
  // static const double LG = 992.0;
  static const double LG = 1280.0;

  // Laptops
  // static const double XL = 1200.0;
  static const double XL = 1600.0;

  // Desktops and TVs 4K
  static const double UL = 2000.0;

  ///
  /// Call this init() method at the first line in the Widget's build() method
  /// 
  void init(BuildContext context) {
    final _mediaQueryData = MediaQuery.of(context);
    
    deviceWidth = _mediaQueryData.size.width;
    deviceHeight = _mediaQueryData.size.height;
    
    // deviceWidth = 500; // TODO remve - testing purposes

    _safeAreaHorizontal = _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical = _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (deviceWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (deviceHeight - _safeAreaVertical) / 100;

    // Utils.log("DEVICE: width => $deviceWidth");
    // Utils.log("DEVICE: height => $deviceHeight");
  }

  static double getWidth(double width) {
    return width * safeBlockHorizontal;
  }

  static double getHeight(double height) {
    return height * safeBlockVertical;
  }

  void test(double width, double height) {
    deviceWidth = width;
    deviceHeight = height;
  }

  static bool get isMobileSmall => checkMobileSmall(deviceWidth); // TODO temp
  static bool get isMobile => checkMobile(deviceWidth);
  static bool get isMobileOnly => checkMobileOnly(deviceWidth);
  static bool get isTabletSmall => checkTabletSmall(deviceWidth);
  static bool get isTablet => checkTablet(deviceWidth);
  static bool get isLaptop => checkLaptop(deviceWidth);
  static bool get isUltraWide => checkUltraWide(deviceWidth);


  static bool checkMobileSmall(double width) {
    return width < SM;
  }

  static bool checkMobile(double width) {
    return width < MD;
  }

  static bool checkMobileOnly(double width) {
    return width >= SM && width < MD;
  }
  
  static bool checkTabletSmall(double width) {
    return width >= MD && width < LG;
  }

  static bool checkTablet(double width) {
    return width >= LG && width < XL;
  }

  static bool checkLaptop(double width) {
    return width >= XL && width < UL;
  }

  static bool checkUltraWide(double width) {
    return width >= UL;
  }


  // static double get appbarHeight = 60.0;

  static double get appbarHeight {
    // tablet & up
    if(isMobile || isTablet || isTabletSmall || isLaptop || isUltraWide) {
      return 60.0;
    }
    // mobile
    else {
      return 50.0;
    }
  }

  static double get sidebarWidth {
    // tablet & up
    if(isTablet || isLaptop || isUltraWide) {
      return 95.0;
    }
    // mobile
    else {
      return 70.0;
    }
  }

  static double get defaultMargin {
    // tablet & up
    if(isTablet || isLaptop || isUltraWide) {
      return Dimens.marginBig;
    }
    else {
      return Dimens.marginRegular;
    }
  }

  static double get dialogHeight {
    // tablet & up
    if(isTablet || isLaptop || isUltraWide) {
      return deviceHeight * 0.8;
      // return 540;
    }
    
    // small tablet
    if(isTabletSmall || isTablet || isLaptop || isUltraWide) {
      return deviceHeight * 0.88;
    }

    // mobile
    return deviceHeight * 0.95;
  }

  static double get dialogWidth {
    // tablet & up
    if(isTablet || isLaptop || isUltraWide) {
      // return _mediaQueryData.size.width * 0.6;
      return 660;
    }

    // mobile
    return deviceWidth * 0.94;
  }

  static double textScale(double oldValue) => isMobile ? oldValue - 0.12 : oldValue;
} 