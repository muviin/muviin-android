import 'dart:convert' show utf8, base64;
import 'dart:io';
import 'package:crypto/crypto.dart' as crypto;
import 'package:flutter/foundation.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:math';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rentme_ghana/src/res/enums.dart';

class Utils {

  static bool isEmptyOrNull(Object obj) {
    // return true if obj is null
    if(obj == null) {
      return true;
    }

    // if its a number and is zero
    if(obj is int || obj is double) {
      return obj == 0;
    }

    // if its an empty string
    if(obj is String) {
      return obj.trim() == '';
    }

    // if its an empty array
    if(obj is Iterable) {
      return obj.length == 0;
    }

    return false;
  }

  static bool isBlank(String string) {
    return string == null || string == "" || string.trim() == "";
  }

  static String fontName(){
    return 'Avenir';
  }

  static String joinOtp(String prefix, String suffix){
    String actualOtp = "$prefix-$suffix";
    return actualOtp;
  } 

  static String enumToString(Object e) => e.toString().split('.').last;

  static String encodePasscode(String code){
    return base64.encode(utf8.encode(code));
  }

  static String decodePasscode(String code){
    final decodeVal = utf8.decode(base64.decode(code));
    return decodeVal;
  }


  static String getReadableDate(String dateString) {
    return formatDateTimeString(dateString, "d MMM, yyyy.");
  }

  static String getReadableDateAndTime(String dateString) {
    return formatDateTimeString(dateString, "d MMM, yyyy. HH:mm");
  }

  static DateTime parseDateTimeString(String dateString) {
    List<String> possibleFormats = [
      'yyyy-MM-ddTHH:mm:ss',
      'MMM dd, yyyy hh:mm:ss a',
    ];

    for(var possibleFormat in possibleFormats) {
      try {
        return DateFormat(possibleFormat, 'en').parse(dateString);
      } catch (e) {
        // log(e);
      }
    }
    print('Date format not in possible dates formats array: $dateString');
    return null;
  }

  static String formatDateTimeString(String dateString, String format) {
    final parsedDate = parseDateTimeString(dateString);
    return parsedDate != null ? DateFormat(format, 'en').format(parsedDate) : '';
  }

  static String formatDateTime(DateTime date, String format) {
    return DateFormat(format, 'en').format(date);
  }

  // static String formatMoney(double money) {
  //   final currencyFormat = new NumberFormat("#,##0.00", "en_US");
  //   return currencyFormat.format(money);
  // }

  static String getInitails(String text) {
    if (Utils.isEmptyOrNull(text) || Utils.isBlank(text))
      return "";
      
    var item = text.trim();

    try {

      item = item.replaceAll(new RegExp("[^a-zA-Z0-9\\s]"), "");

      var initials = StringBuffer();
      if (item.contains(" ")) {
        var parts = item.split(" ");

        initials.write(parts[0][0]);
        initials.write(parts[1][0]);
      } else {
        initials.write(item.substring(0, 2));
      }
      return initials.toString().toUpperCase();
    } catch (e) {
      return item[0].toString();
    }
  }

  static int randomNumber({int numDigits = 2}) {
    final rnd = new Random();
    return rnd.nextInt(10 ^ numDigits);
  }

  static String generateMd5(String input) {
    var md5 = crypto.md5;
    return md5.convert(utf8.encode(input)).toString();
  }

  static Future<File> saveImageFile(String path) async {
    final docsPath = await getDocumentsPath();
    final rand = DateTime.now().millisecondsSinceEpoch.toString();
    return File(path).copy('$docsPath/userPhoto_$rand.jpeg');
  }

  static Future<String> getDocumentsPath() async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static Future captureImageFromCamera({int quality = 60, CameraOptions type}) async {

    try {
      return await ImagePicker.pickImage(
        source: CameraOptions.CAMERA == type ? ImageSource.camera : ImageSource.gallery,
        imageQuality: quality,
      );
    } catch (e) {
      return null;
    }

  }

    static int parseInt(dynamic obj) {
    if(isEmptyOrNull(obj)) return 0;

    if(obj.toString().indexOf('.') >= 0) {
      obj = obj.toString().substring(0, obj.toString().indexOf('.'));
    }
    return int.parse(obj.toString());
  }

    static double parseDouble(dynamic obj) {
    return double.parse(isEmptyOrNull(obj) ? '0' : obj.toString());
  }

    static isProductionMode() {
    // return (Constants.POS_API_BASE_URL == Constants.POS_API_BASE_URL_LIVE);
    return kReleaseMode; // if we built for release
  }

    static log(Object value) {
    if(!isProductionMode()) {
      print('CONSOLE :: ' + value.toString());
    }
     print('CONSOLE :: ' + value.toString());
  }

    static logAll(Object value) {
    if(!isProductionMode()) {
      final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      var all = pattern.allMatches(value.toString());
      for(int i = 0; i < all.length; i++) {
        if(i == 0) {
          print('CONSOLE :: ' + all.elementAt(i).group(0).toString());
        } else {
          print(all.elementAt(i).group(0).toString());
        }
      }
    }
  }

    static String toQueryString(Map<String, dynamic> map, {SortDirection sort}) {
    Iterable<String> keys = map.keys;
    List keysList = keys.toList();

    if(sort != null) {
      if(sort == SortDirection.ASC) {
        keysList.sort((a,b) => a.compareTo(b));
      } else {
        keysList.sort((a,b) => b.compareTo(a));
      }
    }
    
    var generalString = List();
    for (var i = 0; i < keysList.length; i++) {
      var key = keysList[i];
      var value = map[key];
      var data = "$key=$value";
      generalString.add(data);
    }
    var result = generalString.join("&");
    return result;
  }


  static convertToArr(String arrayString){
    var array = json.decode(arrayString) as List;
    return array;
  }

    ///
  /// Reduce the quality of an image file and save in internal documents directory
  ///
  static Future<File> compressImageFile(File file, int quality) async {
    final docsPath = await getDocumentsPath();
    final rand = DateTime.now().millisecondsSinceEpoch.toString();
    var result = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      '$docsPath/userPhoto_$rand.jpeg',
      quality: quality,
    );
    return result;
  }


  



}