
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rentme_ghana/src/model/country.dart';
import 'package:rentme_ghana/src/res/abstracts.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/utils_verifications.dart';
import 'package:rentme_ghana/src/utils/validators.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TextController extends TextEditingController {
  // bool Function(String) validation;
  List<Validator> validations;
  List<TextInputFormatter> formatters;
  bool lastCheckedStatus = true;
  String errorText = '';
  bool isTouched = false;


  Country _country;
  List<Country> countryList = [];
  static const String DEFAULT_COUNTRY_CODE = 'GH'; 

  // begin of phone info TODO:
  Country getSelectedCountry([UserSettings userSettings]) {
    // first return the selected country if any
    if(_country != null) {
      return _country;
    }
    
    // else get country from cache storage
    var defaultCountry = userSettings?.getCountry();
    if(defaultCountry != null && defaultCountry.isValid) {
      // keep and return
      _country = defaultCountry;
      return defaultCountry;
    }
    
    // else get from passed list if any
    if(countryList.length > 0) {
      return countryList.first;
    }

    /// else just return null/[_country]
    return _country;
  }

  Future<String> getParsedPhone() async {
    final phone = await VerificationUtils.parsePhoneNumber(text, getSelectedCountry().countryCode);
    return phone.e164.replaceAll('+', '');
  }


  // END OF PHONE INFO

  bool get isRequired => validations.where((e) {
    return e is RequiredValueValidation 
      || (e is RequiredIfValueValidation && e.shouldBeRequired.status == true);
  }).length > 0;

  TextController({String text, this.validations = const [], this.formatters = const []}) : super() {
    value = text == null ? TextEditingValue.empty : TextEditingValue(text: text);
  }

  bool get isEmpty => Utils.isEmptyOrNull(this.text);

  // bool get isValid => this.validate();
  // bool get isValid => !Utils.isEmptyOrNull(errorText);

  void setError(String text) {
    errorText = text;
    notifyListeners();
  }

  Future<bool> validate() async {
    bool status = true;
    errorText = '';

    // validations.forEach((validator) async {
    for(int i = 0; i < validations.length; i++) {
      if(status == true) {
        // determine if async validator
        if(validations[i].isAsync) { 
          errorText = await validations[i].validateAsync(
            // check amount instead of text for value limits
            (validations[i] is ValueLimitValidation)
              ? this.amount.toString()
              : this.text
          );
        } else {
          errorText = validations[i].validate(
            // check amount instead of text for value limits
            (validations[i] is ValueLimitValidation)
              ? this.amount.toString()
              : this.text
          );
        }

        status = errorText.isEmpty;
      }
    }

    lastCheckedStatus = status;
    notifyListeners();
    
    return lastCheckedStatus;
  }
  
  void resetErrorState() {
    errorText = '';
    lastCheckedStatus = true;
  }

  @override
  String get text => value.text;
  
  // return a double parsed value
  double get amount {
    var figure = value.text.trim().replaceAll(',', '');
    
    if(Utils.isEmptyOrNull(figure)) {
      return 0;
    }

    return double.parse(figure);
  }
  

  @override
  set value(newValue) {
    // if only the text changed, clear error (probably retyping to fix error)
    if(value.text != newValue.text) {
      // mark as touched field
      isTouched = true;

      // clear error state
      resetErrorState();
    }

    // apply input formatters here instead when typing/updating text
    for(var formatter in formatters) {
      newValue = formatter.formatEditUpdate(value, newValue);
    }

    super.value = newValue;
  }

  @override
  void clear() {
    // mark as touched field
    isTouched = true;
    
    resetErrorState();
    super.clear();
  }

}


class StatusToggleController extends ValueNotifier<bool>  {
  StatusToggleController(bool status) : super(status);

  bool get status => value;

  toggle() {
    value = !(value == true);
  }

  hide() {
    value = false;
  }

  show() {
    value = true;
  }
}
