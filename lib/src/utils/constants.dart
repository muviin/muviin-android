class Constants {

  static const RM_API_BASE_URL_STAGING = "http://192.168.8.158:25060";
  static const RM_API_BASE_URL_UAT = "http://192.168.8.158:25060";
  static const RM_API_BASE_URL_LIVE = "http://159.89.25.63";
  static const RM_API_BASE_URL = RM_API_BASE_URL_UAT;
  static const SPACE_URL = "https://rentme.fra1.digitaloceanspaces.com/";

  static const String GET = 'GET';
  static const String POST = 'POST';
  static const String PUT = 'PUT';
  static const String DELETE = 'DELETE';

  static const RM_DATABASE_PATH = "mvn_database_008.sqlite";

  static const String FALLBACK_ERROR_MESSAGE = 'Something went wrong. Please try again later.';

  static const String DEFAULT_TIMESTAMP_FORMAT = "yyyy-MM-ddTHH:mm:ss'Z'";
  static const String DISPLAY_TIMESTAMP_FORMAT = 'yyyy-MM-dd hh:mm a';
  static const String DISPLAY_TIMESTAMP_SIMPLE_FORMAT = 'dd MMM, yyyy';
  static const String EMAIL_REGEX = r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
  static const String DISPLAY_HOUSE = 'House';
  static const String DISPLAY_OFFICE = 'Office';
  static const String DISPLAY_EVENT = 'Event Center';

   static const int DEFAULT_PAGE_SIZE = 20;


}