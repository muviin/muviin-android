
import 'package:flutter/widgets.dart';
import 'package:phone_number/phone_number.dart';
import 'package:rentme_ghana/src/res/abstracts.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
/// value cannot be empty or null
class RequiredValueValidation extends Validator {

  RequiredValueValidation(BuildContext context) : super(context);

  @override
  String validate(String text, [String optional]) {
    if(Utils.isEmptyOrNull(text)) {
      return "field is required"; //AppLocalizations.of(context).thisFieldIsRequired;
    }
    return '';
  }

  @override
  Future<String> validateAsync(String value, [String optional]) {
    throw 'RequiredValueValidation is not an async validator';
  }
}

/// value cannot be empty or null
class RequiredIfValueValidation extends Validator {
  StatusToggleController shouldBeRequired;

  RequiredIfValueValidation(BuildContext context, {@required this.shouldBeRequired}) : super(context);

  @override
  String validate(String text, [String otherValue]) {
    if(Utils.isEmptyOrNull(text) && shouldBeRequired.status) {
      return "field is required"; //AppLocalizations.of(context).thisFieldIsRequired;
    }
    return '';
  }

  @override
  Future<String> validateAsync(String value, [String optional]) {
    throw 'RequiredIfValueValidation is not an async validator';
  }
}


/// value cannot exceed limits
class LengthLimitValidation extends Validator {
  final int min;
  final int max;

  LengthLimitValidation(BuildContext context, {this.min = 0, this.max = 0}) : super(context);

  @override
  String validate(String text, [String optional]) {
    if(!Utils.isEmptyOrNull(min) && text.trim().length < min) {
      return "should not be less than ${min.toString()}"; //AppLocalizations.of(context).shouldNotBeLessThanCharacters(min.toString());
    }
  
    if(!Utils.isEmptyOrNull(max) && text.trim().length > max) {
      return "should not be more than ${min.toString()}"; //AppLocalizations.of(context).shouldNotBeMoreThanCharacters(max.toString());
    }

    return '';
  }

  @override
  Future<String> validateAsync(String value, [String optional]) {
    throw 'LengthLimitValidation is not an async validator';
  }
}


/// Valid Phone Number
class PhoneNumberValidation extends Validator {
  
  PhoneNumberValidation(BuildContext context) : super(context, isAsync: true);

  @override
  String validate(String text, [String optional]) {
    throw 'PhoneNumberValidation is an async validator';
  }

  @override
  Future<String> validateAsync(String text, [String optional]) async {
    // opptional is our Country Code
    if(Utils.isEmptyOrNull(optional)) {
      throw('Country Code [optional] arg is needed for PhoneNumberValidation');
    }

    String phone = text?.toString() ?? '';

    if (Utils.isEmptyOrNull(phone)) {
      return "phone number must not be empty"; //AppLocalizations.of(context)?.phoneNumberIsEmpty;
    }
      
    try {
      bool _status = true;
      await PhoneNumber().parse(phone, region: optional);
      PhoneNumber().parse(phone, region: optional).then((value) {
        _status = true;
      }).catchError((err) {
        _status = false;
      });
      if(_status) {
        return '';
      }
    } catch (e) {
      Utils.log(e);
    }
    
    return "phone number not valid"; //AppLocalizations.of(context)?.phoneNumberIsNotValid;
  }
}

/// Valid values by comparision
class MatchingValueValidation extends Validator {
  TextController otherValue;
  
  MatchingValueValidation(BuildContext context, this.otherValue) : super(context);

  @override
  String validate(String text, [String optional]) {
    if(text != otherValue.text) {
      return "Fields do not match"; //AppLocalizations.of(context).fieldsDoNotMatch;
    }
    return '';
  }

  @override
  Future<String> validateAsync(String value, [String optional]) {
    throw 'MatchingValueValidation is not an async validator';
  }
}

/// Value limiting validation
class ValueLimitValidation<T> extends Validator {
  T minValue;
  T maxValue;
  
  ValueLimitValidation(BuildContext context, {this.minValue, this.maxValue}) : super(context);

  @override
  String validate(String text, [String optional]) {
    var value;

    if(T == double || T == num) {
      value = Utils.parseDouble(text) as T;
    } else if(T == int) {
      value = Utils.parseInt(text) as T;
    } else {
      throw('Value should be of type int/double');
    }

    if(minValue != null && value < minValue) {
      return 'Enter a value greater than $minValue';
    }
    if(maxValue != null && value > maxValue) {
      return 'Enter a value less than $maxValue';
    }
    return '';
  }

  @override
  Future<String> validateAsync(String value, [String optional]) {
    throw 'ValueLimitValidation is not an async validator';
  }
}

/// value cannot be empty or null
class EmailValidation extends Validator {

  EmailValidation(BuildContext context) : super(context);

  @override
  String validate(String text, [String optional]) {
    if(!Utils.isEmptyOrNull(text) && RegExp(Constants.EMAIL_REGEX).hasMatch(text) != true) {
      return "enter a valid email address"; //AppLocalizations.of(context).enterAValidEmailAddress;
    }
    return '';
  }

  @override
  Future<String> validateAsync(String value, [String optional]) {
    throw 'EmailValidation is not an async validator';
  }
}