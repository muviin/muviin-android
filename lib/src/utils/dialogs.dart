import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/model/stream_progress_response.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

Future<void> showMessage(BuildContext context, String message, {bool dismiss = true, String dismissLabel, String header}) async {
  // checks (dont show dialog if out of context)
  if(context == null) {
    return Future.error('Out of context!');
  }
  String headerMsg;
  if(header == null){
    headerMsg = "Something went wrong";
  }
  else{
    headerMsg = header;
  }
  return showDialog<void>(
    context: context,
    barrierDismissible: dismiss,
    builder: (BuildContext ctx) {
      return SimpleDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        children: <Widget>[
          header != null ?Align(
            alignment: Alignment.center,
            child: Padding(
              padding: EdgeInsets.only(left: 0, top: Dimens.marginRegular), //EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Text(
                  headerMsg,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18
                  ),
                ),
            ),
          ):Container(),
          Padding(
            padding: EdgeInsets.only(top: Dimens.marginRegular, left: 20, right: 20), //EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Center(
              child: Text(
                message,
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: RentmeButton(
                text: !Utils.isEmptyOrNull(dismissLabel) ? dismissLabel : "Ok",
                size: ButtonSize.SMALL,
                textColor: RentMeColors.white,
                primaryColor: RentMeColors.colorPrimary,
                onPressed: () {
                    Navigator.of(ctx).pop();
                },
              ),
            ),
          )
        ],
      );
    }
  );
}

// header not applied here
Future<void> showNoOptionDialog(BuildContext context, String message, {bool dismissible = true}) async {
  // checks (dont show dialog if out of context)
  if(context == null) {
    return Future.error('Out of context!');
  }

  return showDialog<void>(
    context: context,
    barrierDismissible: dismissible,
    builder: (BuildContext ctx) {
      return SimpleDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.marginRegular, horizontal: Dimens.marginRegular),
            child: Center(
              child: Text(
                message,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      );
    }
  );
}
// not apllied here too umm
Future<bool> showSingleOptionDialog(BuildContext context, String message, String actionLabel, action(), {bool dismissable = false}) async {
  return showDialog<bool>(
    context: context,
    barrierDismissible: dismissable,
    builder: (BuildContext ctx) {
      return SimpleDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
            child: Center(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Text(
                      message,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.marginMedium),
                    child: RentmeButton(
                      text: actionLabel.toUpperCase(),
                      size: ButtonSize.MEDIUM,
                      textColor: RentMeColors.white,
                      primaryColor: RentMeColors.colorPrimary,
                      onPressed: () {
                         Navigator.of(ctx).pop();
                        action();
                        // Navigator.of(ctx).pop();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      );
    }
  );
}

// header not applied here
Future<bool> showSingleOptionUpdateDialog(BuildContext context, String message, String actionLabel, action(), {bool dismissable = false, bool update = false}) async {
  BackButtonInterceptor.add((stopDefaultButtonEvent, routeInfo) => true);
  return showDialog<bool>(
    context: context,
    barrierDismissible: dismissable,
    builder: (BuildContext ctx) {
      return SimpleDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
            child: Center(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Text(
                      message,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.marginMedium),
                    child: RentmeButton(
                      text: actionLabel.toUpperCase(),
                      size: ButtonSize.MEDIUM,
                      textColor: RentMeColors.white,
                      primaryColor: RentMeColors.colorPrimary,
                      onPressed: () {
                        if(update == false){
                         Navigator.of(ctx).pop();
                        }
                        action();
                        // Navigator.of(ctx).pop();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      );
    }
  );
}

Future<bool> showDoubleOptionDialog(BuildContext context, String message, action(bool option), {String actionLabel, String cancelLabel, String header}) async {
  String headerMsg;
  if(header == null){
    headerMsg = "Something went wrong";
  }
  else{
    headerMsg = header;
  }
  return showDialog<bool>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext ctx) {
      return SimpleDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  header != null ? Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: EdgeInsets.only(left: 0, top: Dimens.marginRegular), //EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                        child: Text(
                          headerMsg,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18
                          ),
                        ),
                    ),
                  ): Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: EdgeInsets.only(top: Dimens.marginRegular),
                      child: Text(
                        message,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.marginMedium),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        RentmeButton(
                          text: !Utils.isEmptyOrNull(cancelLabel) ? cancelLabel : "no",
                          size: ButtonSize.MEDIUM,
                          textColor: RentMeColors.white,
                          primaryColor: RentMeColors.colorPrimary,
                          onPressed: () {
                            Navigator.of(ctx).pop();
                            // return cancelled
                            action(false);
                          },
                        ),
                        SizedBox(width: 20),
                        RentmeButton(
                          text: !Utils.isEmptyOrNull(actionLabel) ? actionLabel : "yes",
                          size: ButtonSize.MEDIUM,
                          textColor: RentMeColors.white,
                          primaryColor: RentMeColors.colorPrimary,
                          onPressed: () {
                            Navigator.of(ctx).pop();
                            // return not cancelled
                            action(true);
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      );
    }
  );
}

bool _backButtonInterceptor(bool stopButtonEvent) {
   return true;
}

//header not needed here
Future<void> showProgressDialogMessage(BuildContext context, String message) async {
  // BackButtonInterceptor.add(_backButtonInterceptor);
  BackButtonInterceptor.add((stopDefaultButtonEvent, routeInfo) => true);
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return SimpleDialog(
        backgroundColor: RentMeColors.white.withOpacity(1),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  top: Dimens.marginRegular,
                  bottom: Dimens.marginMedium,
                ),
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                width: 20,
                height: 20,
                padding: EdgeInsets.only(
                  top: Dimens.marginMedium,
                  bottom: Dimens.marginRegular,
                ),
                child: RentmeProgressIndicator(),
              ),
            ],
          ),
        ],
      );
    }
  )..then((_) {
    // BackButtonInterceptor.remove(_backButtonInterceptor);
    BackButtonInterceptor.remove((stopDefaultButtonEvent, routeInfo) => true);
  })
  ..catchError((_) {
    // BackButtonInterceptor.remove(_backButtonInterceptor);
    BackButtonInterceptor.remove((stopDefaultButtonEvent, routeInfo) => true);
  });
}

//header not needed here
Future<StreamProgressResponse<T>> showStreamProgressDialogMessage<T>(BuildContext context, String message, Stream stream) async {
  // BackButtonInterceptor.add(_backButtonInterceptor);
  BackButtonInterceptor.add((stopDefaultButtonEvent, routeInfo) => true);
  return showDialog<StreamProgressResponse<T>>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext subCtx) {
      stream.listen((data) {
        final response = StreamProgressResponse<T>();
        response.status = true;
        response.responseObject = data;

        Navigator.of(subCtx, rootNavigator: true).pop(response);
      }).onError((err) {

        final response = StreamProgressResponse<T>();
        response.status = false;
        response.errorMessage = err.toString();
        
        Navigator.of(subCtx, rootNavigator: true).pop(response);
      });

      return SimpleDialog(
        backgroundColor: RentMeColors.white.withOpacity(1),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(Dimens.marginRegular),
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: Dimens.marginSmall,
                  bottom: Dimens.marginSmall,
                ),
                child: SizedBox(
                  width: 20,
                  height: 20,
                  child: RentmeProgressIndicator(),
                ),
              ),
            ],
          ),
        ],
      );
    }
  )..then((_) {
    // BackButtonInterceptor.remove(_backButtonInterceptor);
    BackButtonInterceptor.remove((stopDefaultButtonEvent, routeInfo) => true);
  })
  ..catchError((_) {
    // BackButtonInterceptor.remove(_backButtonInterceptor);
    BackButtonInterceptor.remove((stopDefaultButtonEvent, routeInfo) => true);
  });
}


  showInSnackBar(String message, BuildContext context, GlobalKey<ScaffoldState> key) {
    key.currentState.showSnackBar(new SnackBar(
        content: new Text(message)
    ));
  }

// Future<void> _neverSatisfied(BuildContext context) async {
//     return showDialog<void>(
//       context: context,
//       barrierDismissible: false, // user must tap button!
//       builder: (BuildContext context) {
//         return AlertDialog(
//           title: Text('Rewind and remember'),
//           content: SingleChildScrollView(
//             child: ListBody(
//               children: <Widget>[
//                 Text('You will never be satisfied.'),
//                 Text('You\’re like me. I’m never satisfied.'),
//               ],
//             ),
//           ),
//           actions: <Widget>[
//             FlatButton(
//               child: Text('Regret'),
//               onPressed: () {
//                 Navigator.of(context).pop();
//               },
//             ),
//           ],
//         );
//       },
//     );
//   }

//   Future<void> _askedToLead(BuildContext context) async {
//     switch (await showDialog<Department>(
//       context: context,
//       builder: (BuildContext context) {
//         return SimpleDialog(
//           title: const Text('Select assignment'),
//           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
//           children: <Widget>[
//             SimpleDialogOption(
//               onPressed: () { Navigator.pop(context, Department.treasury); },
//               child: const Text('Treasury department'),
//             ),
//             SimpleDialogOption(
//               onPressed: () { Navigator.pop(context, Department.state); },
//               child: const Text('State department'),
//             ),
//           ],
//         );
//       }
//     )) {
//       case Department.treasury:
//         // Let's go.
//         // ...
//       break;
//       case Department.state:
//         // ...
//       break;
//     }
  // }