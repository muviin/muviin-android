

import 'package:rentme_ghana/src/model/model.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class Country extends Model {

  String id = "";
  String name = "";
  String countryCode = "";
  String currencyCode = "";
  String currencyName = "";
  String currencyShortName = "";

  Country();

  bool get isValid => !Utils.isEmptyOrNull(countryCode);

  Country.fromJson(Map<String, dynamic> map) {
    id = parseString(map['id']);
    name = parseString(map['name']);
    countryCode = parseString(map['country_code']);
    currencyCode = parseString(map['currency_code']);
    currencyName = parseString(map['currency_name']);
    currencyShortName = parseString(map['currency_name_short']);
  }

      
  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'country_code': countryCode,
    'currency_code': currencyCode,
    'currency_name': currencyName,
    'currency_name_short': currencyShortName,
  };
}