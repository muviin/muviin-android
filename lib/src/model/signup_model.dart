


import 'package:rentme_ghana/src/model/model.dart';

class SignUp extends Model {
  String name = '';
  String email = '';
  String phone = '';
  String password = '';
  String country = '';

    Map<String, dynamic> toJson() => {
    'email': email,
    'name': name,
    'country': country,
    'phone': phone,
    'password': password,
  };

}
class LoginModel extends Model {
  String email = '';
  String password = '';

    Map<String, dynamic> toJson() => {
    'email': email,
    'password': password,
  };

}
