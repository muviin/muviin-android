import 'package:rentme_ghana/src/model/model.dart';

class Device extends Model {

  String id = "";
  String userId = "";
  String votersId = "";
  String fcmToken = "";
  String model = "";
  String brand = "";
  String manufacturer = "";
  String osInfo = "";
  String createdAt = "";
  String updatedAt = "";

  Device();

  Device.fromJson(Map<String, dynamic> json) {
    id = "${json['id']}";
    userId = json['user_id'];
    votersId = json['voters_id'];
    fcmToken = json['fcm_token'];
    model = json['model'];
    brand = json['brand'];
    manufacturer = json['manufacturer'];
    osInfo = json['os_info'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }
      
  Map<String, dynamic> toJson() => {
    'id': id,
    'user_id': userId,
    'voters_id': votersId,
    'fcm_token': fcmToken,
    'model': model,
    'brand': brand,
    'manufacturer': manufacturer,
    'os_info': osInfo,
    'created_at': createdAt,
    'updated_at': updatedAt,
  };
}