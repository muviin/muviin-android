

import 'package:rentme_ghana/src/model/model.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class User extends Model {

  int id = 0;
  String email = "";
  String phone = "";
  String name = "";
  String country = "";
  String createdAt = "";
  String verify = "";
  String display = "";
  String active = "";
  String limit = "";
  String profile = "";
  String bio = "";
  String phoneVerified = "";
  String emailVerified = "";
  String smsNot = "";
  String emailNot = "";
  String fcmNot = "";

  User();

  bool isUserLoggedIn(){
    if(!Utils.isEmptyOrNull(email) && !Utils.isEmptyOrNull(country)){
      return true;
    }
    return false;
  }

  User.fromJson(Map<String, dynamic> map) {
    id = map['id']; //parseString(map['id']);
    email = parseString(map['email']); 
    phone = map['phone'];
    name = map['name'];
    country = map['country'];
    createdAt = map['created_at'];  //parseDateTime(map['last_seen']); 
    verify = map['verify']; 
    display = map['display'];
    active = map['active']; 
    limit = map['upload_limit']; 
    profile = map['profile']; 
    bio = map['bio']; 
    phoneVerified = map['phone_verified']; 
    emailVerified = map['email_verified']; 
    smsNot = map['sms_notif'];
    emailNot = map['email_notif'];
    fcmNot = map['fcm_notif'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'phone': phone,
    'profile': profile,
    'email': email,
    'name': name,
    'country': country,
    'created_at': createdAt,
    'verify': verify,
    'display': display,
    'active': active,
    'upload_limit': limit,
    'bio' : bio,
    'sms_notif' : smsNot,
    'email_notif' : emailNot,
    'fcm_notif' : fcmNot,
    'email_verified': emailVerified,
    'phone_verified' : phoneVerified 
  };



}