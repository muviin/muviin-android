

import 'package:rentme_ghana/src/model/model.dart';

class Notifications extends Model {

  String id = "";
  String message = "";
  String date = "";
  String type = "";
  
  Notifications();


  Notifications.fromJson(Map<dynamic, dynamic> map) {
    id = "${map['message_id']}";
    message = parseString(map['message']);
    date = parseString(map['date']);
    type = parseString(map['type']);
  
  }

      
  Map<String, dynamic> toJson() => {
    'id': id,
    'message': message,
    'date': date,
    'type': type,
  
  };
}