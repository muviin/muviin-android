import 'dart:convert';

class FcmPayload {

  String pushType = "";
  String topic = "";
  String title = "";
  String description = "";
  // userActivity.Activity activity;
  
  FcmPayload();

  FcmPayload.fromJson(Map<String, dynamic> data) {
    pushType = data['push_type'];
    topic = data['topic'];
    title = data['title'];
    description = data['description'];

    if (data['activity'] != null) {

      Map<String, dynamic> activityMap = json.decode(data['activity']);
      if (activityMap != null) {
        // activity = userActivity.Activity.fromJson(activityMap);
      }
    }
  }
      
  Map<String, dynamic> toJson() => {
    'push_type': pushType,
    'topic': topic,
    'title': title,
    'description': description,
    // 'activity': activity
  };
}