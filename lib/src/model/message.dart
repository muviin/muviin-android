

import 'dart:io';

import 'package:rentme_ghana/src/model/model.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class Message extends Model {

  String id = "";
  String message = "";
  String productName = "";
  String senderId = "";
  String receiverId = "";
  String senderName = "";
  String receiverName = "";
  String date = "";
  String messageStatus = "";
  String senderImage = "";
  String receiverimage = "";
  String messageFile = ""; 
  File imageFile;

  Message();


  Message.fromJson(Map<dynamic, dynamic> map) {
    id = "${map['message_id']}";
    message = parseString(map['message']);
    productName = parseString(map['product_name']);
    senderId = "${map['sender_id']}";
    receiverId = "${map['receiver_id']}";
    date = parseString(map['date']);
    messageStatus = "${map['message_status']}";
    senderName = parseString(map['sender_name']);
    receiverName = parseString(map['receiver_name']);
    senderImage =  parseString(map['sender_image']);
    receiverimage =  parseString(map['receiver_image']);
    messageFile =  parseString(map['message_file']);
  }

      
  Map<String, dynamic> toJson() => {
    'id': id,
    'message': message,
    'product_name': productName,
    'sender_id': senderId,
    'receiver_id': receiverId,
    'date': date,
    'message_status': messageStatus,
    'sender_name': senderName,
    'receiver_name': receiverName,
    'sender_image': senderImage,
    'receiver_image': receiverimage,
    'message_file': messageFile,
  };
}