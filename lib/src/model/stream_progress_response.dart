class StreamProgressResponse<T> {
  bool status;
  T responseObject;
  String errorMessage;
}
