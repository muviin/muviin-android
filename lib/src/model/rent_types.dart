
import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/res/enums.dart';

class ServicesModel {
    String name = "";
    Color color;
    RentType type;
    String ico = "";


    ServicesModel({this.name, this.type, this.color, this.ico});
    


    List<ServicesModel> getServices(){
      List<ServicesModel> services = [
        ServicesModel(name: "RENT", type: RentType.RENT,color: Colors.red, ico: "assets/images/house_ic.png"),
        ServicesModel(name: "SALE", type: RentType.SALE, color: Colors.yellow, ico: "assets/images/sale_ico.png"),
        ServicesModel(name: "OFFICE", type: RentType.OFFICE, color: Colors.blue, ico: "assets/images/office_ico.png"),
        ServicesModel(name: "HOTELS", type: RentType.HOTEL, color: Colors.purple, ico: "assets/images/hotel_ico.png"),
        ServicesModel(name: "EVENTS CENT..", type: RentType.EVENT, color: Colors.pink, ico: "assets/images/event_ico.png"),
        ServicesModel(name: "WAREHOUSES", type: RentType.WAREHOUSE, color: Colors.blueAccent, ico: "assets/images/warehouse_ico.png"),
        ServicesModel(name: "ALL", type: RentType.ALL, color: Colors.blueAccent, ico: "assets/images/house_ic.png")
      ];
      return services;
    }
}