

import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/utils/utils_db.dart';
import 'package:sqlcool/sqlcool.dart';

class Schema {
  String name;
  String primaryColumn = 'id';
  String sortColumn = 'created_at';
  SortDirection sortDirection = SortDirection.DESC;
  DbTable schema;
  List<DbMigration> migrations = [];
}