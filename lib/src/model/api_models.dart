import 'dart:io';

import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/utils/constants.dart';

class ApiRequest {

  ApiRequest(this.path, this.method, {String body, List<File> file, Map<String, String> headers, AuthTokenType authTokenType = AuthTokenType.USER, bool encryptBody = true, Map<String, dynamic> bodyMap}) {
    this.headers = headers;
    // this.authTokenType = authTokenType;
    this.encryptBody = encryptBody;
    this.bodyMap = bodyMap;

    if (Constants.POST == method) {
      if(file != null) {
        this.file = file;
      } else {
        this.body = body;
      }
    }
  }

  String method;
  String path;
  String body;
  Map<String, dynamic> bodyMap;
  List<File> file;
  // AuthTokenType authTokenType;
  Map<String, String> headers = {};
  bool encryptBody;
}

class ApiResponse {

  ApiResponse();

  int status;
  dynamic data;
  BaseError error;

  bool get hasError => error != null;

  ApiResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'];

    if (json['error'] != null) {
      error = BaseError.fromJson(json['error']);
    }
  }

  Map<String, dynamic> toJson() => {
    'status': status,
    'data': data,
    'error': error.toJson()
  };
}

class BaseError {

  BaseError();

  String url;
  int errorCode;
  String errorMessage;

  BaseError.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    errorCode = json['error_code'];
    errorMessage = json['message'];
  }

  Map<String, dynamic> toJson() => {
    'url': url,
    'error_code': errorCode,
    'error_message': errorMessage
  };
  
}
// class GeneralError extends Model {
  
//   GeneralError();

//   String timestamp;
//   int status;
//   GeneralErrorDetail error;
//   String message;
//   String path;

//   GeneralError.fromJson(Map<String, dynamic> map) {
//     timestamp = parseString(map['timestamp']);
//     status = parseInt(map['status_code'], 0);
//     error = parseMap(map['error'], (data) => GeneralErrorDetail.fromJson(data));
//     message = parseString(map['status_message']);
//     path = parseString(map['path']);
//   }

//   Map<String, dynamic> toJson() => {
//     'timestamp': timestamp,
//     'status_code': status,
//     'error': error,
//     'status_message': message,
//     'path': path,
//   };

//   toString() => 'Error $status: $error';
// }

// class GeneralErrorDetail extends Model {
  
//   GeneralErrorDetail();

//   String url;
//   int errorCode;
//   dynamic errorMessage;

//   GeneralErrorDetail.fromJson(Map<String, dynamic> map) {
//     url = parseString(map['url']);
//     errorCode = parseInt(map['error_code']);
//     errorMessage = parseString(map['error_message']);
//   }

//   Map<String, dynamic> toJson() => {
//     'url': url,
//     'error_code': errorCode,
//     'error_message': errorMessage,
//   };

//   toString() => 'Error $errorCode: $errorMessage';
// }
