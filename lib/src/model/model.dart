
import 'dart:convert';

import 'package:rentme_ghana/src/res/abstracts.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/utils_date.dart';

class Model {
  Model();

  toJson() {
    throw('Unimplemented method');
  }
  
  Model.fromJson(Map<String, dynamic> map) {
    throw('Unimplemented method');
  }

  int parseInt(dynamic value, [int defaultValue = 0]) {
    if(Utils.isEmptyOrNull(value)) return defaultValue;
    return Utils.parseInt(value);
  }

  String parseString(dynamic value, [String defaultValue]) {
    if(Utils.isEmptyOrNull(value)) return defaultValue;
    return '$value';
  }

  double parseDouble(dynamic value, [double defaultValue = 0]) {
    if(Utils.isEmptyOrNull(value)) return defaultValue;
    return Utils.parseDouble(value);
  }

  bool parseBoolean(dynamic value, [bool defaultValue = false]) {
    if(Utils.isEmptyOrNull(value)) return defaultValue;
    return value == true || value == 'true';
  }

  T parseMap<T>(dynamic value, ModelObjectCast<T> dataObject) {
    if(Utils.isEmptyOrNull(value)) return null;
    
    if(value is T) {
      // convert to json object if possible
      return value;
    }

    if(value is Map) {
      return dataObject(value as Map<String, dynamic>);
    }
    
    // check if its really a json string
    if(value is String && (value.contains('{') || value.contains('['))) {
      // convert to json object if possible
      value = json.decode(value);
      return dataObject(value as Map<String, dynamic>);
    }

    // return null if couldnt get a map/json out of the value being passed
    return null;
  }

  List<T> parseMapList<T>(dynamic value, ModelObjectCast<T> dataObject) {
    if(Utils.isEmptyOrNull(value)) return [];
    
    // guess its a json string (convert to json object/map)
    if(value is String) {
      value = json.decode(value);
    }

    final List<T> list = [];
    for(var item in value) {
      /// if is already a [T] item
      if(item is T) {
        list.add(item);
      } else {
        list.add(dataObject(item));
      }
    }

    return list;
  }
  
  List<T> parseList<T>(dynamic value, Function(T) dataObject) {
    if(Utils.isEmptyOrNull(value)) return [];
    
    // guess its a json string
    if(value is String) {
      value = json.decode(value);
    }

    final List<T> list = [];
    for(var item in value) {
      list.add(dataObject(item));
    }

    return list;
  }

  DateTime parseDateTime(dynamic value, [DateTime defaultValue]) {
    if(Utils.isEmptyOrNull(value)) return defaultValue;
    return DateUtils.parse(value);
  }

  String toDateTimeString(DateTime value, [String defaultValue]) {
    if(Utils.isEmptyOrNull(value.toString())) return defaultValue;
    return DateUtils.formatDateTime(value);
  }

  // int toUnixTimestamp(DateTime value) {
  //   return value?.unixTimestamp ?? 0;
  // }

}