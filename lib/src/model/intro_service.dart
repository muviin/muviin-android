
import 'package:rentme_ghana/src/res/icons.dart';

class IntroService {
    String message = "";
    AppIcon icon;


    IntroService({String message, AppIcon icon}){
      this.message = message;
      this.icon = icon;
    }
    


    List<IntroService> getServices(){
      List<IntroService> services = [
        IntroService(icon:  AppIcon.upload, message: "Upload Your House for sale or for rent purposes"),
        IntroService(icon:  AppIcon.search, message: "Search for nearby office space to rent for your business"),
        IntroService(icon:  AppIcon.searchPlace, message: "Get available places nationwide for your comfort")
      ];
      return services;
    }
}