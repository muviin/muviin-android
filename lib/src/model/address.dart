

import 'package:rentme_ghana/src/utils/utils.dart';

class GhPost {

  String address = '';
  String streetName = '';
  String region = '';
  String district = '';
  String postCode = '';
  List cordinate = List();


  GhPost();



  GhPost.fromJson(Map<String, dynamic> json) {
    address =  json['address'];
    streetName = json['streetName'] != null ? json['streetName'] : json['street_name'];
    region = json['region'];
    district = json['district'];
    postCode = json['postCode'] != null ? json['postCode'] : json['post_code'];
    if(json['cordinate'] is String){
      List<dynamic> list = List();
      list.add(json['cordinate']);
      cordinate = list;
    }
    else{
      cordinate = json['cordinate'];
    }



  }

  Map<String, dynamic> toJson() => {
    'address': address,
    'street_name': streetName,
    'region': region,
    'district': district,
    'post_code': postCode,
    'cordinate': cordinate,
  };


  bool isNotEmpty(){
    if(
      !Utils.isEmptyOrNull(streetName )
      && !Utils.isEmptyOrNull(region) 
      && !Utils.isEmptyOrNull(district)
      && !Utils.isEmptyOrNull(postCode)
      && !Utils.isEmptyOrNull(cordinate))
      {
        return true;
    }
    return false;
  }



}