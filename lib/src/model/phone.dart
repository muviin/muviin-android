import 'package:rentme_ghana/src/model/model.dart';

class ParsedPhone extends Model {
  
  int countryCode;
  String e164;
  String national;
  String type;
  String international;
  String nationalNumber;

  ParsedPhone();

  ParsedPhone.fromJson(Map<String, dynamic> map) {
    countryCode = parseInt(map['country_code']);
    e164 = parseString(map['e164']);
    national = parseString(map['national']);
    type = parseString(map['type']);
    international = parseString(map['international']);
    nationalNumber = parseString(map['national_number']);
  }
      
  Map<String, dynamic> toJson() => {
    'country_code': countryCode,
    'e164': e164,
    'national': national,
    'type': type,
    'international': international,
    'national_number': nationalNumber,
  };
}