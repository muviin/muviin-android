


import 'package:rentme_ghana/src/model/schemas/rents_schema.dart';
import 'package:rentme_ghana/src/utils/utils_db.dart';
import 'package:sqlcool/sqlcool.dart';

class AllSchema {

  /// 
  /// LIST OF ALL TABLE SCHEMAS ([DbTable])
  ///
  static final List<DbTable> list = [ 
    RentsSchema().schema,
  ];


  ///
  /// Migration table schemas
  /// A list of [DbMigration] queries to be run
  /// 
  static final List<DbMigration> migration = [
    // ...BranchSchema().migrations,
  ];

}
