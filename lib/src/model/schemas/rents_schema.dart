
import 'package:rentme_ghana/src/model/schema.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:sqlcool/sqlcool.dart';

class RentsSchema extends Schema {
  RentsSchema() {

    name = 'rents_table';
    primaryColumn = 'rent_id';
    sortColumn = 'rent_type';
    sortDirection = SortDirection.ASC;

    schema = DbTable(name)
      ..varchar('rent_id', unique: true)
      ..varchar('poster_id')
      ..varchar('poster_email')
      ..varchar('poster_name')
      ..varchar('poster_phone')
      ..varchar('poster_image')
      ..varchar('long')
      ..varchar('lat')
      ..varchar('description')
      ..varchar('price')
      ..varchar('location')
      ..varchar('rent_type')
      ..varchar('status')
      ..varchar('street')
      ..varchar('type')
      ..varchar('verify')
      ..varchar('country')
      ..varchar('thumbnail')
      ..text('thumbnail_array')
      ..varchar('countryCode')
      ..varchar('charge_type')
      ..varchar('upLoad_type')
      ..varchar('approve')
      ..varchar('created_at');
  }  
  
  // static final migrateAddTimestamps = DbMigration(name, 'products_add_timestamps');

}
