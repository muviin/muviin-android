import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class SocketConnection{
  
  SocketIO socketIO;
  connect(){
    socketIO = SocketIOManager().createSocketIO("http://192.168.8.158:3000", "", query: "userId=21031", socketStatusCallback: socketStatus);

    //call init socket before doing anything
    socketIO.init();

    //subscribe event
    socketIO.subscribe("resp", (callback){
      Utils.log(callback);
    });
    //connect socket
    socketIO.connect();
  }

  socketStatus(dynamic data) {
    print("Socket status>>>>>>>: " + data);
  }

  sendMessage(){
    socketIO.sendMessage("send_message", "hello world");

  }



}