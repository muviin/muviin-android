

import 'dart:convert';

import 'package:rentme_ghana/src/model/country.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class Rents {

      String id = "";
      String posterId = "";
      String posterEmail = "";
      String posterName  = "";
      String posterPhone = "";
      String posterImage = "";
      String status = "";
      String country = "";
      String description = "";
      String lat = "";
      String long = "";
      String price = "";
      List<dynamic> thumbnailArray = List();
      String street = "";
      String location = "";
      String createdAt = "";
      String rentType = "";
      String type= "";
      String verify= "";
      String thumbnail = "";
      String countryCode = "";
      String approve = "";
      String chargeType = "";
      String upLoadType = "";



  Rents();

  Rents.fromJson(Map<dynamic, dynamic> snapshot) {
    id = "${snapshot['id']}";
    posterId = snapshot['poster_id'];
    description = snapshot['description'];
    posterEmail = snapshot['poster_email'];
    posterName = snapshot['poster_name'];
    posterPhone = snapshot['poster_phone'];
    posterImage =  snapshot['poster_image'];
    long = snapshot['long'];
    lat = snapshot['lat'];
    createdAt = snapshot['created_at'];
    price = snapshot['price'];
    location = snapshot['location'];
    rentType = snapshot['rent_type'];
    status = snapshot['status'];
    street = snapshot['street'];
    thumbnail = snapshot['thumbnail'];
    type = snapshot['type'];
    verify = snapshot['verify'];
    thumbnailArray = validate(snapshot['thumbnail_array']); //snapshot['thumbnail_array'] is String ?  Utils.convertToArr(snapshot['thumbnail_array']):snapshot['thumbnail_array'];
    country = snapshot['country'];
    chargeType = snapshot['charge_type'];
    upLoadType = snapshot['upLoad_type'];
    approve = snapshot['approve'];
  }

  List<dynamic> validate(dynamic value){
    if(value is String){
      return  Utils.convertToArr(value);
    }
    return value;
  }

  String getCalculatedPrice(UserSettings settings){
    var countiesList = settings.getCountriess();
    List<Country> list = List();
    for (var i = 0; i < countiesList.length; i++) {
      list.add(Country.fromJson(countiesList[i]));
    }

    // now loop throug list and get single country
    for (var country in list) {
        if(country.name.toUpperCase() == this.country.toUpperCase()){
          Utils.log("value");
          if(rentType == "Sale"){
            return price != null? "$price ${country.currencyCode}":"0 ${country.currencyCode}";
          }
          return price != null ? "$price ${country.currencyCode} $chargeType":"0 ${country.currencyCode} $chargeType";
        }
    }
    return "";
  }

    Map<String, dynamic> toJson() => {
    "poster_id" : posterId,
    "poster_email" : posterEmail,
    "poster_name" : posterName,
    "poster_phone" : posterPhone,
    "poster_image" :  posterImage,
    "long": long,
    "lat": lat,
    "created_at" : createdAt,
    "price" : price,
    "location" : location,
    "rent_type" : rentType,
    "status" : status,
    "street" :  street,
    "type" : type,
    "verify" : verify,
    "country": country,
    "thumbnail": thumbnail,
    "thumbnail_array": thumbnailArray,
    "description": description,
    "countryCode": countryCode,
    "charge_type": chargeType,
    "upLoad_type": upLoadType,
    "approve": approve,
  };

    Map<String, dynamic> toDb() => {
    'rent_id': id,
    "poster_id" : posterId,
    "poster_email" : posterEmail,
    "poster_name" : posterName,
    "poster_phone" : posterPhone,
    "poster_image" :  posterImage,
    "long": long,
    "lat": lat,
    "created_at" : createdAt,
    "price" : price,
    "location" : location,
    "rent_type" : rentType,
    "status" : status,
    "street" :  street,
    "type" : type,
    "verify" : verify,
    "country": country,
    "thumbnail": thumbnail,
    "thumbnail_array": json.encode(thumbnailArray),
    "description": description,
    "countryCode": countryCode,
    "charge_type": chargeType,
    "upLoad_type": upLoadType,
    "approve": approve,
  };


}