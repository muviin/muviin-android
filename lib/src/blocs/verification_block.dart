import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_place/google_place.dart';
import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rentme_ghana/src/model/device.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:url_launcher/url_launcher.dart';

class VerificationBloc{
    pushPermissionCheck({onSuccess(value), onFailure(reason)}) async{
    var status = await Permission.location.serviceStatus.isEnabled;
    var request = await Permission.location.request();
    if (!status && request.isGranted) {
      // location is turned off, bh apps permission to use location is granted
      // prompt user to turn on location
      onFailure(PermissionState.ENABLE);
    }
    else if (status && request.isDenied){
      // request for permission
      onFailure(PermissionState.REQUEST);
    }
    else{
      onSuccess(true);
    }
  }


  Future<PermissionState> pushPermissionChecknEW() async{

    var status = await Permission.location.serviceStatus.isEnabled;
    var request = await Permission.location.request();

    if (!status && request.isGranted) {
      // location is turned off, bh apps permission to use location is granted
      // prompt user to turn on location
      return PermissionState.ENABLE;
    }
    else if (status && request.isDenied){
      // request for permission
      return PermissionState.REQUEST;
    }
    else{
      return  PermissionState.SUCCESS;
    }
  }



  Future<String> autoCompleteSearch({String value, GooglePlace googlePlace}) async {
    var result = await googlePlace.autocomplete.get(value);
    if (result != null && result.predictions != null) {
      return result.predictions.first.placeId;
    }  
    return "";  
  }

  // List<AutocompletePrediction> predictions = [];
  // DetailsResult res;

    Future<Location> getDetils({String placeId, GooglePlace googlePlace}) async {
      var result = await googlePlace.details.get(placeId);
      if (result != null && result.result != null) {
        print(result.result.geometry.location);
        return result.result.geometry.location;
      }
      return Location(lat: 123, lng: 123);
  }


  Future<Device> buildDevice(UserSettings userSettings) async {
    FirebaseMessaging firebaseMessaging = new FirebaseMessaging();
    
    String fcmToken = await firebaseMessaging.getToken();
    Utils.log(">>> FCM Token: $fcmToken");

    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    Device device = userSettings.getDevice();
    device.userId = "${userSettings.getUser().id}";

    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      
      device.fcmToken = fcmToken;
      device.model = androidInfo.model;
      device.brand = androidInfo.brand;
      device.manufacturer = androidInfo.manufacturer;
      device.osInfo = "Android SDK INT ${androidInfo.version.sdkInt}";

    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;

      device.fcmToken = fcmToken;
      device.model = iosInfo.model;
      device.brand = iosInfo.name;
      device.manufacturer = "Apple";
      device.osInfo = "${iosInfo.systemName} ${iosInfo.systemVersion}";
    }

    userSettings.setDevice(device);
    return device;
  }


  checkForUpdate(BuildContext context, {onSuccess(reason), onFailure(reason)}) async {
    //Get Current installed version of app
    final PackageInfo info = await PackageInfo.fromPlatform();
    double currentVersion = double.parse(info.version.trim().replaceAll(".", ""));

    //Get Latest version info from firebase config
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    try {
      // Using default duration to force fetching from remote server.
      await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      await remoteConfig.activateFetched();
      var response = remoteConfig.getString('force_update_current_version');
      double newVersion = double.parse(response
          .trim()
          .replaceAll(".", ""));
      if (newVersion > currentVersion) {
        onSuccess(true); // update required
      }
      else{
        onSuccess(false); // go on
      }
    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
      Utils.log(exception);
    } catch (exception) {
      onFailure(false);
      Utils.log('Unable to fetch remote config. Cached or default values will be '
          'used');
    }
  }

showVersionDialog(context, String url) async {
  showSingleOptionUpdateDialog(
    context,
    "There is a new version",
    "Update now",
    ()=>_launchURL(url),
    update: true
  );
}
_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}


  startStreetView({double lat, double long, onSuccess(valueMap), onFailure(reason)}) async {
    try {
      const _platform = const MethodChannel('com.muviin.muviin_app/street');

        var arguments = <String, double>{
        "latitude": lat,
        "longitude": long,
      };

      final Map<String, dynamic> scannedResultMap = await _platform.invokeMapMethod('streetview', arguments);
      // scannedResultMap.addAll(
      //   {
      //     "country_code": countryCode,
      //     "document_type": documentType,
      //   }
      // );
      onSuccess(scannedResultMap);
    } on PlatformException catch (e) {
      onFailure(e);
    }
  }

  Future<String>appVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    String appName = packageInfo.appName;
    String version = packageInfo.version;
    String appInformation = "$appName $version";
    return appInformation;
  }

  Future<bool> checkConnectivity()async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network.
      Utils.log("I am connected to a mobile network.");
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.
      Utils.log("I am connected to a wifi network.");
      return true;
    }
    else if (connectivityResult == ConnectivityResult.none) {
      // I am not connected to a  network.
      Utils.log("I am connected to a wifi network.");
      return false;
    }
    return false;
  }





}