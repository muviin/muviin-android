
import 'package:rentme_ghana/src/model/signup_model.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/repository/auth_repository.dart';
import 'package:rxdart/subjects.dart';

class SignupBloc {

  final _authRepository = AuthRepository();
  final _signupUserFetcher = PublishSubject<User>();



  Stream<User> get signupuser => _signupUserFetcher.stream;



  signupUser(SignUp signUp) async {
    final apiResponse = await _authRepository.signUp(signUp);

    if (apiResponse.status == 0) {
      _signupUserFetcher.sink.add(User.fromJson(apiResponse.data));
    } else {
      _signupUserFetcher.sink.addError(apiResponse.error.errorMessage);
    }   
  }


  dispose() {
    _signupUserFetcher.close();
  }
}
