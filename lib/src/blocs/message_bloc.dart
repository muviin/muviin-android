
import 'package:rentme_ghana/src/model/message.dart';
import 'package:rentme_ghana/src/model/notifications.dart';
import 'package:rentme_ghana/src/repository/message_repository.dart';
import 'package:rxdart/subjects.dart';

class MessageBloc {

  final _messageRepository = MessageRepository();

  final _getMsgFetcher = PublishSubject<List<Message>>();
  final _getMsgHistoryFetcher = PublishSubject<List<Message>>();
  final _sendMsgFetcher = PublishSubject<Message>();
  final _changedMsgStatusFetcher = PublishSubject<bool>();
  final _getNotifsFetcher = PublishSubject<Notifications>();



  Stream<List<Message>> get history => _getMsgHistoryFetcher.stream;
  Stream<List<Message>> get messages => _getMsgFetcher.stream;
  Stream<Message> get messageSent => _sendMsgFetcher.stream;
  Stream<bool> get messageStatus => _changedMsgStatusFetcher.stream;
  Stream<Notifications> get notifs => _getNotifsFetcher.stream;



  sendMessage(Message message) async {
    final apiResponse = await _messageRepository.sendMessage(message);

    if (apiResponse.status == 0) {
      _sendMsgFetcher.sink.add(Message.fromJson(apiResponse.data));
    } else {
      _sendMsgFetcher.sink.addError(apiResponse.error.errorMessage);
    }   
  }
  getMessages(String id,posterId) async {
    final apiResponse = await _messageRepository.getMessages(id, posterId);

    if (apiResponse.status == 0) {
      List<Message> messageList = List();
      for (var i = 0; i < apiResponse.data.length; i++) {
        messageList.add(Message.fromJson(apiResponse.data[i]));
      }
      _getMsgFetcher.sink.add(messageList);
    } else {
      _getMsgFetcher.sink.addError(apiResponse.error.errorMessage);
    }   
  }
  getMessagesHistory(String id) async {
    final apiResponse = await _messageRepository.getMessagesHistory(id);

    if (apiResponse.status == 0) {
      List<Message> messageList = List();
      for (var i = 0; i < apiResponse.data.length; i++) {
        messageList.add(Message.fromJson(apiResponse.data[i]));
      }
      _getMsgHistoryFetcher.sink.add(messageList);
    } else {
      _getMsgHistoryFetcher.sink.addError(apiResponse.error.errorMessage);
    }   
  }

  changeMsgStatus(List<String> ids) async {
    final apiResponse = await _messageRepository.changeMessageStatus(ids);

    if (apiResponse.status == 0) {
      _changedMsgStatusFetcher.sink.add(true);
    } else {
      _changedMsgStatusFetcher.sink.addError(apiResponse.error.errorMessage);
    }   
  }

  getNotification() async {
    final apiResponse = await _messageRepository.getNotification();

    if (apiResponse.status == 0) {
      _getNotifsFetcher.sink.add(Notifications.fromJson(apiResponse.data));
    } else {
      _changedMsgStatusFetcher.sink.addError(apiResponse.error.errorMessage);
    }   
  }
 




  dispose() {
    _sendMsgFetcher.close();
    _getMsgFetcher.close();
    _getMsgHistoryFetcher.close();
    _changedMsgStatusFetcher.close();
  }
}
