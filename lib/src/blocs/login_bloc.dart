
import 'dart:io';

import 'package:rentme_ghana/src/model/signup_model.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/repository/auth_repository.dart';
import 'package:rxdart/subjects.dart';

class LoginBloc {

  final _authRepository = AuthRepository();
  final _loginUserFetcher = PublishSubject<User>();
  final _setProfilePicFetcher = PublishSubject<User>();
  final reqPwdFetcher = PublishSubject<bool>();
  final resetPwdFetcher = PublishSubject<bool>();



  Stream<User> get loginuser => _loginUserFetcher.stream;
  Stream<User> get profilePicResponse => _setProfilePicFetcher.stream;
  Stream<bool> get passwordRequest => reqPwdFetcher.stream;
  Stream<bool> get reset => resetPwdFetcher.stream;




  loginUser(LoginModel login) async {
    final apiResponse = await _authRepository.login(login);

    if (apiResponse.status == 0) {
      _loginUserFetcher.sink.add(User.fromJson(apiResponse.data));
    } else {
      _loginUserFetcher.sink.addError(apiResponse.error.errorMessage);
    }   
  }
  setProfilePic(String email, File file) async {
    final apiResponse = await _authRepository.setProfilePic(email, file);

    if (apiResponse.status == 0) {
      _setProfilePicFetcher.sink.add(User.fromJson(apiResponse.data));
    } else {
      _setProfilePicFetcher.sink.addError(apiResponse.error);
    }   
  }

  requestPwd(String email) async {
    final apiResponse = await _authRepository.requestPwd(email);

    if (apiResponse.status == 0) {
      reqPwdFetcher.sink.add(true);
    } else {
      reqPwdFetcher.sink.addError(apiResponse.error.errorMessage);
    }   
  }
  resetPwd(String code, pwd, email) async {
    final apiResponse = await _authRepository.resetPwd(code, pwd, email);

    if (apiResponse.status == 0) {
      resetPwdFetcher.sink.add(true);
    } else {
      resetPwdFetcher.sink.addError(apiResponse.error.errorMessage);
    }   
  }






  dispose() {
    _loginUserFetcher.close();
    _setProfilePicFetcher.close();
    reqPwdFetcher.close();
    resetPwdFetcher.close();
  }
}
