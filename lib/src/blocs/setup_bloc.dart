import 'dart:async';
import 'dart:core';

import 'package:rentme_ghana/src/model/api_models.dart';
import 'package:rentme_ghana/src/model/country.dart';
import 'package:rentme_ghana/src/model/device.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/repository/auth_repository.dart';
import 'package:rentme_ghana/src/repository/message_repository.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rxdart/subjects.dart';


class SetupBloc {

  final _authRepository = AuthRepository();
  final _messageRepository = MessageRepository();
  final countriesFetcher = PublishSubject<List<Country>>();
  final deviceFetcher = PublishSubject<Device>();
  final userFetcher = PublishSubject<User>();

  final pwdFetcher = PublishSubject<bool>();

 
  Stream<List<Country>> get countries => countriesFetcher.stream;
  Stream<Device> get device => deviceFetcher.stream;

  Stream<bool> get profile => pwdFetcher.stream;
  Stream<User> get user => userFetcher.stream;

  fetchCountries() async {
    final apiResponse = await _authRepository.fetchCountries();

    if (apiResponse.status == 0) {
      Utils.log(apiResponse.data);
      Utils.log("value");
      List<Country> rrr = List();
      for (var i = 0; i < apiResponse.data.length; i++) {
        rrr.add(Country.fromJson(apiResponse.data[i]));
      }
      countriesFetcher.sink.add(rrr);
    } else {
      countriesFetcher.sink.addError(apiResponse.error);
    } 
  }

  saveDevice(Device device) async {
    ApiResponse apiResponse = await _messageRepository.saveDevice(device);
      
    if (apiResponse.status == 0) {
      Utils.log(apiResponse.data);
      deviceFetcher.sink.add(Device.fromJson(apiResponse.data));
    } else {
      deviceFetcher.sink.addError(apiResponse.error.errorMessage);
    } 
  }


  logouActiont() async {
    // await _authRepository.logout();
    //  _logoutFetcher.sink.add(true);  
  }

  getUser(String id) async {

    ApiResponse apiResponse = await _authRepository.getUser(id);

    if (apiResponse.status == 0) {
      Utils.log(apiResponse.data);
      userFetcher.sink.add(User.fromJson(apiResponse.data));
    } else {
      userFetcher.sink.addError(apiResponse.error.errorMessage);
    } 


  }

  updateProfile(Map value) async{
    ApiResponse apiResponse = await _authRepository.updateProfile(value);

    if (apiResponse.status == 0) {
      Utils.log(apiResponse.data);
      pwdFetcher.sink.add(true);
    } else {
      pwdFetcher.sink.addError(apiResponse.error.errorMessage);
    } 
  }

  updateEmail(String email) async{
    // bool response = await _authRepository.updateEmail(email);
    // if(response){
    //   _updateEmailFetcher.sink.add(true);
    // }
    // else{
    //    _updateEmailFetcher.sink.addError("Failed", null);
    // }
  }

  updatePhone(String phone) async{
    // bool response = await _authRepository.updatePhone(phone);
    // if(response){
    //   _updatePhoneFetcher.sink.add(true);
    // }
    // else{
    //    _updatePhoneFetcher.sink.addError("Failed", null);
    // }
  }

   setProfilePic(String url) async{
    // bool response = await _authRepository.setProfilePic(url);
    // if(bool != null){
    //   _profilePicFetcher.sink.add(true);
    // }
    // else{
    //   //TODO: add error handler
    // }
  }

  dispose() {
    countriesFetcher.close();
    deviceFetcher.close();
    userFetcher.close();
    pwdFetcher.close();
    
  }



}
