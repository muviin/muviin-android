import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/persistence/rents_persistence.dart';
import 'package:rentme_ghana/src/repository/rent_repository.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

import 'package:rxdart/subjects.dart';

class RentBloc {

  final _rentRepository = RentRepository();

  final _allRentFetcher = PublishSubject<List<Rents>>();
  final _rentFetcher = PublishSubject<List<Rents>>();
  final _userRentFetcher = PublishSubject<List<Rents>>();
  final _uploadRentFetcher = PublishSubject<dynamic>();
  final _searchRentFetcher = PublishSubject<List<Rents>>();
  final _onlyRentsFetcher = PublishSubject<List<Rents>>();
  final _onlySalesFetcher = PublishSubject<List<Rents>>();
  final _similarRentFetcher = PublishSubject<List<Rents>>();

  RentPersistence _rentPersistence = RentPersistence();


  Stream<List<Rents>> get allRents => _allRentFetcher.stream;
  Stream<List<Rents>> get rents => _rentPersistence.rents; //_rentFetcher.stream;
  Stream<List<Rents>> get searchedRents => _searchRentFetcher.stream;
  Stream<List<Rents>> get userRents => _userRentFetcher.stream;
  Stream<List<Rents>> get onlyRents => _onlyRentsFetcher.stream;
  Stream<List<Rents>> get onlySales => _onlySalesFetcher.stream;
  Stream<List<Rents>> get similarRents => _similarRentFetcher.stream;
  Stream<dynamic> get uploadRentResponse => _uploadRentFetcher.stream;


  fetchAllRents() async {
    final apiResponse = await _rentRepository.fetchAllRents();

    if (apiResponse.status == 0) {
      Utils.log(apiResponse.data);
      Utils.log("value");
      List<Rents> rrr = List();
      for (var i = 0; i < apiResponse.data.length; i++) {
        rrr.add(Rents.fromJson(apiResponse.data[i]));
      }
      _allRentFetcher.sink.add(rrr);
    } else {
      _allRentFetcher.sink.addError(apiResponse.error);
    } 
  }

  notifyOwner(String posterId) async {
    final apiResponse = await _rentRepository.notifyOwner(posterId);

    if (apiResponse.status == 0) {

    } else {
      _allRentFetcher.sink.addError(apiResponse.error);
    } 
  }

  // on home page
  fetchRent(String country,type, {filter,location, rentType}) async {
    // persistebce
    _rentPersistence.fetchProperties(
      location: location, 
      type: rentType, 
      filter: filter, 
      pageIndex: 0,
      pageSize: 100,
    );

    // netwrk
    final apiResponse = await _rentRepository.getRentsByCountry(country, type);
    if (apiResponse.status == 0) {
      Utils.log(apiResponse.data);
      Utils.log("value");
      List<Rents> rrr = List();
      for (var i = 0; i < apiResponse.data.length; i++) {
        rrr.add(Rents.fromJson(apiResponse.data[i]));
      }
      _rentPersistence.storeProductList(rrr);
      // _rentFetcher.sink.add(rrr);
    } else {
      _rentFetcher.sink.addError(apiResponse.error);
    } 
  }


  uploadRent(Rents rent, List<File> files) async{
    final apiResponse = await _rentRepository.uploadRent(rent, files);
    if (apiResponse.status == 0) {
      Utils.log(apiResponse.data);
      _uploadRentFetcher.sink.add(apiResponse.data);
    } else {
      _uploadRentFetcher.sink.addError(apiResponse.error.errorMessage);
    } 
  }




  fetchUserPost(String email) async {
    final apiResponse = await _rentRepository.fetchUserRents(email);

    if (apiResponse.status == 0) {
      Utils.log(apiResponse.data);
      Utils.log("value");
      List<Rents> rrr = List();
      for (var i = 0; i < apiResponse.data.length; i++) {
        rrr.add(Rents.fromJson(apiResponse.data[i]));
      }
      _userRentFetcher.sink.add(rrr);
    } else {
      _userRentFetcher.sink.addError(apiResponse.error);
    } 
    // Query response =  await _rentRepository.fetchUserRents(email);
    // if (response != null) {

    //   response.onValue.listen((data){
    //     var snapshot = data.snapshot;
    //     List<Rents> posts = List();

    //     Map<dynamic, dynamic> resp = snapshot.value;
    //     for (var i = 0; i < resp.length; i++) {
    //       String key = resp.keys.elementAt(i);
    //       posts.add(Rents.fromJson(resp[key]));
    //     }
    //     _userRentFetcher.sink.add(posts);

    //   });
    // } else {
    //   _userRentFetcher.sink.addError("error", null);
    // }  
  }


  searchRent(String location) async {
    // final FirebaseUser user = await _authRepository.currentUser();
    Query response =  await _rentRepository.searchRents(location);
    if (response != null) {

      response.onValue.listen((data){
        var snapshot = data.snapshot;
        List<Rents> rents = List();

        Map<dynamic, dynamic> resp = snapshot.value;
        for (var i = 0; i < resp.length; i++) {
          String key = resp.keys.elementAt(i);
          print(snapshot.value);
          rents.add(Rents.fromJson(resp[key]));
        }
        _searchRentFetcher.sink.add(rents);

      });
    } else {
      _searchRentFetcher.sink.addError("error", null);
    }  
  }

  fetchRentsOnly(String country) async {
    Query response =  await _rentRepository.fetchRentsOnly(country);
    if (response != null) {

      response.onValue.listen((data){
        var snapshot = data.snapshot;
        List<Rents> rents = List();

        Map<dynamic, dynamic> resp = snapshot.value;
        for (var i = 0; i < resp.length; i++) {
          String key = resp.keys.elementAt(i);
          print(snapshot.value);
          rents.add(Rents.fromJson(resp[key]));
        }
        _onlyRentsFetcher.sink.add(rents);

      });
    } else {
      _onlyRentsFetcher.sink.addError("error", null);
    }  
  }

  fetchSalesOnly(String country) async {
    Query response =  await _rentRepository.fetchSalesOnly(country);
    if (response != null) {

      response.onValue.listen((data){
        var snapshot = data.snapshot;
        List<Rents> rents = List();

        Map<dynamic, dynamic> resp = snapshot.value;
        for (var i = 0; i < resp.length; i++) {
          String key = resp.keys.elementAt(i);
          print(snapshot.value);
          rents.add(Rents.fromJson(resp[key]));
        }
        _onlySalesFetcher.sink.add(rents);

      });
    } else {
      _onlySalesFetcher.sink.addError("error", null);
    }  
  }

  fetchSimilarRents(String location) async {
    final apiResponse = await _rentRepository.fetchSimilarRents(location);

    if (apiResponse.status == 0) {
      Utils.log(apiResponse.data);
      Utils.log("value");
      List<Rents> rrr = List();
      for (var i = 0; i < apiResponse.data.length; i++) {
        rrr.add(Rents.fromJson(apiResponse.data[i]));
      }
      _similarRentFetcher.sink.add(rrr);
    } else {
      _similarRentFetcher.sink.addError(apiResponse.error);
    } 
    // Query response =  await _rentRepository.fetchSimilarRents(location);
    // if (response != null) {

    //   response.onValue.listen((data){
    //     var snapshot = data.snapshot;
    //     List<Rents> rents = List();

    //     Map<dynamic, dynamic> resp = snapshot.value;
    //     for (var i = 0; i < resp.length; i++) {
    //       String key = resp.keys.elementAt(i);
    //       print(snapshot.value);
    //       rents.add(Rents.fromJson(resp[key]));
    //     }
    //     _similarRentFetcher.sink.add(rents);

    //   });
    // } else {
    //   _similarRentFetcher.sink.addError("error", null);
    // }  
  }


  dispose() {
    _rentFetcher.close();
    _allRentFetcher.close();
    _rentFetcher.close();
    _userRentFetcher.close();
    _uploadRentFetcher.close();
    _searchRentFetcher.close();
    _onlyRentsFetcher.close();
    _onlySalesFetcher.close();
  }



}
