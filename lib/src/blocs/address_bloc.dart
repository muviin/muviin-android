
import 'package:rentme_ghana/src/model/address.dart';
import 'package:rentme_ghana/src/repository/address_repository.dart';
import 'package:rxdart/rxdart.dart';

class AddressBloc {

  final _accountRepository = AddressRepository();
  final _addressFetcher = PublishSubject<GhPost>();

  Stream<GhPost> get postAddress => _addressFetcher.stream;

  Future<Null> getPostAddressInfo(String address) async {

    GhPost apiResponse = await _accountRepository.getPostAddress(address);
    if(apiResponse != null){
      GhPost ghPost = apiResponse;
      _addressFetcher.sink.add(ghPost);
    }
    else{
      _addressFetcher.sink.addError("Error", null);
    }  
  }


  

  dispose() {
    _addressFetcher.close();
  }
}
