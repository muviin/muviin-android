import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:http/io_client.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:http/http.dart' as http;
import 'package:rentme_ghana/src/model/address.dart';
import 'package:rentme_ghana/src/model/api_models.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class RentMeApi {
  
  Future makeRequest<T>(String apiBase, ApiRequest apiRequest, {externalCall = false, bool rent = true}) async {
    var request;

    var url = "http://159.89.25.63";
    var mainUrl = url;

    // not a file upload request
    if (apiRequest.file == null) {
      // this checks prevents the addition of the invm url we took from firestore
      // because we sometimes make a call to outside servers,
      // we need to exclude the firestor url
        request = http.Request(apiRequest.method, Uri.parse("$apiBase${apiRequest.path}"));


      var resolvedHeaders = await resolveHeaders(apiRequest.headers,);
      resolvedHeaders.forEach((key, value) {
        request.headers[key] = value;
      });
      if (apiRequest.body != null && apiRequest.body != "") {
          request.body = apiRequest.body;

      }
      _logRequest(request, apiRequest.body);

    } else {      
      // file upload request
       request = http.MultipartRequest(apiRequest.method, Uri.parse("$apiBase${apiRequest.path}"));

      for (var i = 0; i <  apiRequest.file.length; i++) {
        var stream = http.ByteStream(DelegatingStream.typed(apiRequest.file[i].openRead()));
        var length = await apiRequest.file[i].length();

        var resolvedHeaders = await resolveHeaders(apiRequest.headers);
        resolvedHeaders.forEach((key, value) {
          request.headers[key] = value;
        });

        var multipartFile;
        // if (i == 0){
        //   // make multiple checks to see the kind of file upload here
        //   // whether utility, or card
        //   // this check is made here because, if one image is beeing uploaded, this condition only gets called
        //   // but when multiple is uploaded, the second gets called
        //   // hence the first condition will b called the most
        //   if(card){
        //     // multipartFile = http.MultipartFile('card', stream, length, filename: basename(apiRequest.file[i].path));
        //   }
        //   else{
        //     Utils.log(apiRequest.file.first.path);
        //     multipartFile = await http.MultipartFile.fromPath("image", apiRequest.file.first.path); //http.MultipartFile('image', stream, length, filename: apiRequest.file[i].path); //http.MultipartFile.fromPath("image", apiRequest.file[i].path,);
        //   }
        // }
        // else if(i == 1){
        //   // multipartFile = http.MultipartFile('selfie', stream, length, filename: basename(apiRequest.file[i].path));
        // }
        if(rent){
          multipartFile = await http.MultipartFile.fromPath("image", apiRequest.file[i].path);
        }
        else{
          // profile
          multipartFile = await http.MultipartFile.fromPath("image", apiRequest.file[i].path);
        }
        
        request.files.add(multipartFile);
        apiRequest.bodyMap.keys
        .where((k) => apiRequest.bodyMap[k].isEmpty) // filter keys
        .toList() // create a copy to avoid concurrent modifications
        .forEach(apiRequest.bodyMap.remove);

        apiRequest.bodyMap.forEach((key, value) {
          Utils.log(key);
          Utils.log(value);
          request.fields[key] = value;
        });
        Utils.log(request);
        Utils.log(request.fields);

        _logRequest(request, basename(apiRequest.file[i].path),);

        
      }

    }

    assert(request != null);

    try {
      final response = await _client().send(request);
      
      final String responseBody = await response.stream.transform(utf8.decoder).join();

      _logResponse(response, responseBody);

      if (response.statusCode == 200) {
          _logPasredResponse(responseBody);

          if (T == GhPost) {
            Utils.log(responseBody);
            var dec = json.decode(responseBody);
            try {
              if(dec is List){
                if(dec.first["name"] == "TimeoutError"){
                  return GhPost();
                }
              }
            } catch (e) {
              return GhPost();
            }
            return GhPost.fromJson(dec);
          }

          var responses = ApiResponse.fromJson(json.decode(responseBody));
          if(responses.hasError) {
            // return with error localization instead
            responses = await _getError(responses.error.errorCode, message: responses.error.errorMessage);
          }
          return responses;
      }
      else if(response.statusCode == 400){
          _logPasredResponse(responseBody);

          var response = ApiResponse.fromJson(json.decode(responseBody));
          if(response.hasError) {
            // return with error localization instead
            response = await _getError(response.error.errorCode, message: response.error.errorMessage);
          }
          return response;
      }
      else {

    }
    } catch (e) {
      if(e is TimeoutException) {
        var tmpResponse = await _getError(100003); // timeout

        return tmpResponse;
      }
      
      if(e is SocketException) {
        var tmpResponse = await _getError(100002); // couldnt connect
        // if (T == AuthToken) {
        //   Utils.log(tmpResponse.error.errorMessage);
        //   return AuthToken()..setTokenInvalidError(tmpResponse.error.errorMessage);
        // }
        return tmpResponse;
      }

      return _getError(100001);
    }
  }

  /// ### NEW IMPLEMENTATION
  static Future<ApiResponse> _getError(int status, {String message}) async {
    // get settings cache
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // UserSettings userSettings = UserSettings(prefs);

    message = message ?? 'Something went wrong. Try again';

    // get error from code
    // String errorLocale = userSettings.getAppErrorList().getError('$status', userSettings.getLanguage(), defaultMessage: message);

    BaseError error = BaseError();
    error.errorCode = status;
    error.errorMessage =  message;

    ApiResponse apiResponse = ApiResponse();
    apiResponse.status = -1;
    // apiResponse.statusMessage =  message;
    apiResponse.error = error;

    return apiResponse;
  }

  http.Client _client() {
    // hack to allow all https request cert signing - (avoid CERTIFICATE_VERIFY_FAILED exception)
    SecurityContext securityContext = SecurityContext(withTrustedRoots: false);
    // var ioClient = new HttpClient(context: securityContext)
    //     ..badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

        
    var ioClient = new HttpClient(context: securityContext)
        ..badCertificateCallback = ((X509Certificate cert, String host, int port) {
          var host = "investmobile.ecobank.com";
          return (host == host);

        });
    return IOClient(ioClient);
  }

  void _logRequest(http.BaseRequest request, String jsonBody,) {
    Utils.log("InvmApi: >>>> INFO Request URL: ${request.method} : ${request.url}");
    if (!Utils.isEmptyOrNull(jsonBody)) {
      Utils.log("InvmApi: >>>> INFO Request Body: $jsonBody");
    }
    request.headers.forEach((key, value) {
      Utils.log("InvmApi: >>>> INFO Request Header Parameter: $key = $value");
    });
  }

  void _logResponse(http.StreamedResponse response, String json) {
    response.headers.forEach((key, value) {
      Utils.log("InvmApi: <<<< INFO Response Header Parameter: $key = $value");
    });
    Utils.log("InvmApi: <<<< INFO Response Status Code: ${response.statusCode}");
    Utils.log("InvmApi: <<<< INFO Response: $json");
  }

  void _logPasredResponse(String parsedResponse) {
    Utils.log("InvmApi: <<<< INFO Parsed Response: $parsedResponse");
  }

  // MAKE SURE TO REMOVE THE UTILITY PARAM
  Future<Map<dynamic, dynamic>> resolveHeaders(Map<String, String> includeHeaders,) async {

    var headers = {
      'Content-Type': 'application/json',
    };
    var resolvedheaders = {}..addAll(headers)..addAll(includeHeaders ?? {});
    // remove empty or null values from map ie resolvedheaders
    resolvedheaders.keys
    .where((k) => resolvedheaders[k].isEmpty) // filter keys
    .toList() // create a copy to avoid concurrent modifications
    .forEach(resolvedheaders.remove); 


    return  resolvedheaders;
  }

}
