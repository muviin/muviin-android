

// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_database/firebase_database.dart';
import 'dart:convert';
import 'dart:io';

import 'package:rentme_ghana/src/model/api_models.dart';
import 'package:rentme_ghana/src/model/device.dart';
import 'package:rentme_ghana/src/model/message.dart';
import 'package:rentme_ghana/src/model/signup_model.dart';
import 'package:rentme_ghana/src/network/rent_me_api.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class MessageRepository with RentMeApi {

  static const MESSAGEING_SERVICE_BASE_URL = "${Constants.RM_API_BASE_URL}/messaging/";



  Future<ApiResponse> saveDevice(Device device) async {
    var apiRequest = ApiRequest("devices", Constants.POST, body: json.encode(device.toJson()));

    ApiResponse apiResponse = await makeRequest(MESSAGEING_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }



  Future<ApiResponse> sendMessage(Message message) async {
     var apiRequest;
    if(Utils.isEmptyOrNull(message.imageFile)){
      apiRequest = ApiRequest("send_message", Constants.POST, body: json.encode(message.toJson()),);
    }
    else{
      List<File> fileList = List();
      fileList.add(message.imageFile);
      var hed = {
        'Content-type': 'multipart/form-data'
      };
      apiRequest = ApiRequest("send_message", Constants.POST, body: json.encode(message.toJson()),file: fileList,bodyMap: message.toJson(), headers: hed);
    }
    

    ApiResponse apiResponse = await makeRequest(MESSAGEING_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }
  Future<ApiResponse> getMessages(String id,posterId) async {
    var toJson = {
      "id":id,
      "poster_id":posterId
    };
    var apiRequest = ApiRequest("get_messages", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(MESSAGEING_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }
  Future<ApiResponse> getMessagesHistory(String id) async {
    var toJson = {
      "id":id,
    };
    var apiRequest = ApiRequest("get_messages_history", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(MESSAGEING_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }
  Future<ApiResponse> changeMessageStatus(List<String> ids) async {
    var toJson = {
      "ids":ids,
    };
    var apiRequest = ApiRequest("change_msg_status", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(MESSAGEING_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }

  Future<ApiResponse> getNotification() async {
    var apiRequest = ApiRequest("get_notifs", Constants.GET);

    ApiResponse apiResponse = await makeRequest(MESSAGEING_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }





}