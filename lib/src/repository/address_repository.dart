import 'dart:convert';
import 'dart:io';

import 'package:rentme_ghana/src/model/address.dart';
import 'package:rentme_ghana/src/model/api_models.dart';
import 'package:rentme_ghana/src/model/signup_model.dart';
import 'package:rentme_ghana/src/network/rent_me_api.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
  

class AddressRepository with RentMeApi {

  static const LOCATION_SERVICE_BASE_URL = "http://35.190.60.92/gpa"; //"${Constants.RM_API_BASE_URL}/location/";


  Future<GhPost> getPostAddress(String address) async {
    var tojson = Map();
    tojson = {
      "address":address
    };
    var apiRequest = ApiRequest("/gh_post", Constants.POST, body: json.encode(tojson), encryptBody: false);

    GhPost apiResponse = await makeRequest<GhPost>(LOCATION_SERVICE_BASE_URL, apiRequest, externalCall: true);
    return apiResponse;
  }



}