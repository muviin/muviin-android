

// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_database/firebase_database.dart';
import 'dart:convert';
import 'dart:io';

import 'package:rentme_ghana/src/model/api_models.dart';
import 'package:rentme_ghana/src/model/signup_model.dart';
import 'package:rentme_ghana/src/network/rent_me_api.dart';
import 'package:rentme_ghana/src/utils/constants.dart';

class AuthRepository with RentMeApi {

  static const AUTH_SERVICE_BASE_URL = "${Constants.RM_API_BASE_URL}/uaa/";



  Future<ApiResponse> signUp(SignUp signup) async {
    var apiRequest = ApiRequest("signup", Constants.POST, body: json.encode(signup.toJson()));

    ApiResponse apiResponse = await makeRequest(AUTH_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }
  Future<ApiResponse> login(LoginModel login) async {
    var apiRequest = ApiRequest("signin", Constants.POST, body: json.encode(login.toJson()));

    ApiResponse apiResponse = await makeRequest(AUTH_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }

  Future<ApiResponse> fetchCountries() async {
    var apiRequest = ApiRequest("countries", Constants.GET,);

    ApiResponse apiResponse = await makeRequest(AUTH_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }

  Future<ApiResponse> setProfilePic(String email, File file) async {
    List<File> fileList = List();
    fileList.add(file);

    var toMap = {
      "email": email
    };

    var hed = {
      'Content-type': 'multipart/form-data'
    };
    var apiRequest = ApiRequest("update_profile_pic", Constants.POST, body: json.encode(toMap), file: fileList, headers: hed, bodyMap: toMap);

    ApiResponse apiResponse = await makeRequest(AUTH_SERVICE_BASE_URL, apiRequest, rent: false);
    return apiResponse;
  }


  Future<ApiResponse> getUser(String id) async {
    var toJson = {
      "id":id
    };
    var apiRequest = ApiRequest("get_user", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(AUTH_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }

  
  Future<ApiResponse> updateProfile(Map values) async {
    var apiRequest = ApiRequest("update_profile", Constants.POST, body: json.encode(values));

    ApiResponse apiResponse = await makeRequest(AUTH_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }


  Future<ApiResponse> requestPwd(String email) async {
    var toJson = {
      "email":email
    };
    var apiRequest = ApiRequest("request_pwd", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(AUTH_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }
  Future<ApiResponse> resetPwd(String code, pwd, email) async {
    var toJson = {
      "code":code,
      "password":pwd,
      "email":email
    };
    var apiRequest = ApiRequest("reset_pwd", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(AUTH_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }








  // firebase implementation

//   final FirebaseAuth _auth = FirebaseAuth.instance;
//   final FirebaseDatabase database = FirebaseDatabase.instance;
//   // FirebaseUser user;


// Future<User> currentUser() async {
//   return _auth.currentUser;
// }

// Future<UserCredential> handleSignIn(String email, String password) async {
//   final UserCredential user = await _auth.signInWithEmailAndPassword(email: email, password: password);
//   return user;
// }

// Future<UserCredential> _handleSignUp(String fullName, String email, String phone, String password, String country) async {
//   final UserCredential user = await _auth.createUserWithEmailAndPassword(email: "flutter@test.com", password: "flutter");
//   return user;
// }

// Future<Query> fetchUser(String email) async {
//   final Query res = database.reference().child("users").orderByChild("email").equalTo(email);
//   return res;
// }

// Future<bool> updateName(String name) async {
//   final User userData =  _auth.currentUser;
//   Map<String, dynamic> value = {"name": name};
//   await database.reference().child("users").child(userData.uid).update(value);
//   return true;
// }

// Future<bool> updateEmail(String email) async {
//   final User userData = _auth.currentUser;

//   try {
//      await userData.updateEmail(email);
//   } catch (e) {
//     print(e);
//   }


//   Map<String, dynamic> value = {"email": email};
//   await database.reference().child("users").child(userData.uid).update(value);
//   return true;
// }


// Future<bool> updatePhone(String phone) async {
//   final User userData = _auth.currentUser;
//   Map<String, dynamic> value = {"phone": phone};
//   await database.reference().child("users").child(userData.uid).update(value);
//   return true;
// }

// Future<User> handleUser() async {
//   return _auth.currentUser;
// }

// Future<void> logout() async {
//   await _auth.signOut();
// }

// Future<bool> setProfilePic(String imageUrl) async {
//   final User userData = _auth.currentUser;
//   Map<String, dynamic> value = {"profile": imageUrl};
//   await database.reference().child("users").child(userData.uid).update(value);
//   return true;
// }




}