

import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:rentme_ghana/src/model/api_models.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/network/rent_me_api.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class RentRepository with RentMeApi {

  static const RENT_SERVICE_BASE_URL = "${Constants.RM_API_BASE_URL}/rent/";


  Future<ApiResponse> getRentsByCountry(String country,type) async {
    Map<String, String> toJson;
    toJson = {
      "country": country,
      "type":type
    };
    var apiRequest = ApiRequest("fetch_by_country", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(RENT_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }

  Future<ApiResponse> fetchAllRents() async {
    var apiRequest = ApiRequest("fetch_rents", Constants.GET,);

    ApiResponse apiResponse = await makeRequest(RENT_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }
  Future<ApiResponse> fetchSimilarRents(String location) async {
    var toJson = {
      "location": location
    };
    var apiRequest = ApiRequest("fetch_similar_rents", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(RENT_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }

  Future<ApiResponse> fetchUserRents(String email) async {
    Map<String, String> toJson;
    toJson = {
      "email": email
    };
    var apiRequest = ApiRequest("fetch_user_posts", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(RENT_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }

  Future<ApiResponse> uploadRent(Rents rent, List<File> files) async {
    List<File> fileList = List();
    // fileList.add(file);
    fileList = files;

    var hed = {
      'Content-type': 'multipart/form-data'
    };
    var apiRequest = ApiRequest("upload", Constants.POST, body: json.encode(rent.toJson()), file: fileList, bodyMap: rent.toJson(), headers: hed);

    ApiResponse apiResponse = await makeRequest(RENT_SERVICE_BASE_URL, apiRequest,);
    return apiResponse;
  }

  Future<ApiResponse> notifyOwner(String posterId) async {
    Map<String, String> toJson;
    toJson = {
      "poster_id": posterId
    };
    var apiRequest = ApiRequest("notify_owner", Constants.POST, body: json.encode(toJson));

    ApiResponse apiResponse = await makeRequest(RENT_SERVICE_BASE_URL, apiRequest);
    return apiResponse;
  }













  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseDatabase database = FirebaseDatabase.instance;
  // FirebaseUser user;


Future<User> currentUser() async {
  return  _auth.currentUser;
}

Future<Query> fetchRents(String country) async {
  final Query res = database.reference().child("Rents").orderByChild("country").equalTo(country);
  return res;
}


// Future<dynamic> uploadRent(Rents rents) async {
//   final User userData =  _auth.currentUser;
//    String autoid = Utils.generateMd5("${DateTime.now().toString()}${userData.uid}");
//   // await database.reference().child("Rents").child(autoid).set(rents.toJson());
//   return true;
// }

Future<Query> searchRents(String location) async {
  final Query res = database.reference().child("Rents").orderByChild("location").equalTo(location);
  return res;
}

Future<Query> fetchRentsOnly(String country) async {
  Query res;
  try {
    res = database.reference().child("Rents").orderByChild("rentType").equalTo("rent");
    return res;
  } catch (e) {
    print(e);
  }
  return res;
}

Future<Query> fetchSalesOnly(String country) async {
  final Query res = database.reference().child("Rents").orderByChild("rentType").equalTo("sale");
  return res;
}

// Future<Query> fetchSimilarRents(String location) async {
//   final Query res = database.reference().child("Rents").orderByChild("location").equalTo(location);
//   return res;
// }

Future<bool> updateRentImage(String imageUrl) async {
  final User userData = _auth.currentUser;
  Map<String, dynamic> value = {"profile": imageUrl};
  await database.reference().child("Rents").child(userData.uid).update(value);
  return true;
}



}