import 'dart:ui';

// class RentMeColors{
//   static const Color silverGrey = const Color(0xFFF2F2F7);
//   static final Color colorPrimary = const Color(0xFFffffff);
//   static final Color colorAccent= const Color(0xFFFF6B87);
//   static final Color lightGreen= const Color(0xFF81C874);
//   static final Color whitePrimary= const Color(0xFFFFFFFF);
//   static final Color bluePrimary= const Color(0xFF64B5F6);
//   static final Color greenPrimary= const Color(0xFF81C784);
//   static final Color grey= const Color(0xFFE0E0E0);
//   static final Color lightBlack = const Color(0xFF000000);
// }



class RentMeColors {
  static const Color silverGrey = const Color(0xFFF2F2F7);
  static const Color colorPrimary = const Color(0xFF72C177);
  static const Color colorSecondary = const Color(0xFF005B82);
  static const Color colorAccent = const Color(0xFFF1F7FA);
  static const Color colorPrimaryLight = const Color(0x3372C177);
  static const Color colorSecondaryLight = const Color(0xFF119CD4);

  static const Color colorBorder = const Color(0x806B91A0);
  static const Color colorBorderGrey = const Color(0x47707070);
  static const Color colorBackground = const Color(0xFFF1F7FA);
  static const Color colorShadow = const Color(0x26000000);

  static const Color textPrimary = const Color(0xFF000000);
  static const Color textPrimaryLight = const Color(0x80000000);
  static const Color textSecondary = const Color(0xFF005B82);
  static const Color textAccent = const Color(0xFF6B91A0);
  static const Color textDanger = const Color(0xFFBA0041);
  
  static const Color transparent = const Color(0x00000000);
  static const Color white = const Color(0xFFFFFFFF);
  static const Color black = const Color(0xFF000000);
  static const Color grey = const Color(0xFFEEEEEE);
  static const Color red = const Color(0xFFE35454);
  static const Color blue = const Color(0xFF179BD4);
  static const Color windowBackground = const Color(0xFFF8F8F8);

  static const Color faintIconColor = const Color(0xFFB1BACA);


  static const Color bpPurpleVeryLight = const Color(0xFF74682b4);

}