
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:websafe_svg/websafe_svg.dart';

class AppIcon {
  final String value;

  const AppIcon(this.value);

  Widget draw({Color color = RentMeColors.black, double size}) => WebsafeSvg.asset(
    value,
    semanticsLabel: 'icon',
    width: size ?? 24,
    height: size ?? 24,
    color: color,
  );

  Widget drawSvg({Color color = RentMeColors.black, double size}) => SvgPicture.asset(
    value,
    semanticsLabel: 'icon',
    width: size ?? 24,
    height: size ?? 24,
    // color: color,
  );


  static const AppIcon getSartedIcon = AppIcon('assets/svg/intro.svg');
  static const AppIcon upload = AppIcon('assets/svg/upload.svg');
  static const AppIcon searchPlace = AppIcon('assets/svg/search_place.svg');
  static const AppIcon search = AppIcon('assets/svg/search.svg');
  static const AppIcon emptyMsg = AppIcon('assets/svg/message_empty.svg');
  static const AppIcon emptyPost = AppIcon('assets/svg/empty_post.svg');
  static const AppIcon apartmentRent = AppIcon('assets/svg/apartment_rent.svg');
  static const AppIcon emptyLi = AppIcon('assets/svg/empty_li.svg');
  static const AppIcon logo = AppIcon('assets/svg/muvin.svg');
  static const AppIcon user = AppIcon('assets/svg/user.svg');
  static const AppIcon alarm = AppIcon('assets/svg/bullhorn.svg');
  static const AppIcon magnifier = AppIcon('assets/svg/magnifier.svg');
  static const AppIcon bubble = AppIcon('assets/svg/bubble.svg');
  static const AppIcon license = AppIcon('assets/svg/license.svg');
  static const AppIcon exit = AppIcon('assets/svg/exit.svg');
  static const AppIcon about = AppIcon('assets/svg/about.svg');

}
