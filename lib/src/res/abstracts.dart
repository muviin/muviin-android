import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/network/rent_me_api.dart';

abstract class Repository {
  RentMeApi api = RentMeApi();

  Repository();
}

typedef ModelObjectCast<T> = T Function(Map<String, dynamic>);

abstract class Validator {
  final BuildContext context;
  final bool isAsync;

  Validator(this.context, {this.isAsync = false});

  String validate(String value, [String optional]);
  Future<String> validateAsync(String value, [String optional]);

}
