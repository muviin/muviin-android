import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';


class AppStyles {

  static const TextStyle textTitleDefualt = const TextStyle(
    color: RentMeColors.textPrimary,
    fontSize: Dimens.textBig,
    fontWeight: FontWeight.bold,
    fontFamily: 'Nunito_Sans',
  );
    static const TextStyle textTitleLight = const TextStyle(
      color: RentMeColors.colorPrimary,
      fontSize: Dimens.textSmall,
      fontWeight: FontWeight.w500,
      fontFamily: 'Nunito_Sans',
    );

  static const TextStyle textBodyDefault = const TextStyle(
    color: RentMeColors.textPrimary,
    fontSize: Dimens.textRegular,
    fontWeight: FontWeight.w400,
    fontFamily: 'Nunito_Sans',
  );

    // 15 R
  static const TextStyle textBody2 = const TextStyle(
    color: RentMeColors.textPrimary,
    fontSize: Dimens.textSmall,
    fontWeight: FontWeight.w400,
    fontFamily: 'Nunito_Sans',
  );

  // 14 R
  static const TextStyle textBody3 = const TextStyle(
    color: RentMeColors.textPrimary,
    fontSize: Dimens.textSmall,
    fontWeight: FontWeight.w400,
    fontFamily: 'Nunito_Sans',
  );

    static const TextStyle textTitleRegular = const TextStyle(
    color: RentMeColors.textPrimary,
    fontSize: Dimens.textRegular,
    fontWeight: FontWeight.bold,
    fontFamily: 'Nunito_Sans',
  );

      static const TextStyle textTitle1 = const TextStyle(
    color: RentMeColors.textPrimary,
    fontSize: Dimens.textRegular,
    fontWeight: FontWeight.w700,
    fontFamily: 'Helvetica Neue',
    // fontFamily: 'OpenSans',
  );

}