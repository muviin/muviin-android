
enum SizeE {
  SMALL, MEDIUM, BIG, LARGE, FULL
}

enum SizeD { SMALL, BIG }

enum PermissionState{
  REQUEST, ENABLE, SUCCESS
}
enum QueryGate {
  AND, OR
}

enum ButtonSize {
  COMPACT, NORMAL, FULL, SMALL, TINY, ICON, MEDIUM
}

enum SettingsType{
  NAME_CHANGE, USERNAME_CHANGE, COUNTRY_CHANGE, MOBILE_CHANGE, EMAIL_CHANGE, PASSWORD_CHANGE
}

enum RentType{
  ALL, RENT, SALE, OFFICE, HOTEL,WAREHOUSE,EVENT
}

enum CameraOptions {
  CAMERA,
  GALLERY,
  REMOVE
}
enum AuthTokenType {
  USER, DOMAIN, CLIENT
}

enum SortDirection {
  ASC, DESC
}
enum PhoneFormat {
  INTERNATIONAL, NATIONAL, E164
}