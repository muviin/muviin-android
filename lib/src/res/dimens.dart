class Dimens {

  static const double marginSuperLarge = 100.0;
  static const double marginHuge = 80.0;
  static const double marginXXLarge = 60.0;
  static const double marginXLarge = 46.0;
  static const double marginLarge = 40.0;
  static const double marginBig = 32.0;
  static const double marginRegular = 20.0;
  static const double marginMedium = 16.0;
  static const double marginSmall = 10.0;
  static const double marginTiny = 4.0;
  static const double marginTopFromSafe= 37.0;

  static const double textExtraLarge = 46.0;
  static const double textLarge = 40.0;
  static const double textHuge = 27.0;
  static const double textBig = 21.0;
  static const double textRegular = 16.0;
  static const double textMedium = 15.0;
  static const double textSmall = 14.0;
  static const double textTiny = 12.0;


  static const double dividerLarge = 10.0;
  static const double dividerBig = 8.0;
  static const double dividerRegular = 4.0;
  static const double dividerSmall = 2.0;
  static const double dividerLine = 1.0;
  static const double dividerTiny = 0.5;
  
  static const double borderRadius = 3.0;

  static const double dialogWidth = 400;

  
}