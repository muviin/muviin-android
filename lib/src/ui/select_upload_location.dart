import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_place/google_place.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/ui/widgets/text_field.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/validators.dart';
import 'package:geolocator/geolocator.dart' as Geo;


class SelectLocationSearch extends StatefulWidget{


  @override
  State<StatefulWidget> createState() {
    return SelectLocationSearchState();
  }

}


class SelectLocationSearchState extends State<SelectLocationSearch>{
  static const kGoogleApiKey = "AIzaSyDWg6uFUfbxGf0zEKtUorrkJRAo6I00Kvg";


  TextController _searchctrl = TextController();

  GooglePlace googlePlace;
  List<AutocompletePrediction> predictions = [];
  List<DetailsResult> showPredictions = [];
  List<String> li = [];
  FocusNode focusNode = FocusNode();
  bool isloading = false;




  @override
  void initState() {
    super.initState();

    googlePlace = GooglePlace(kGoogleApiKey);

    _searchctrl.addListener(() {
      if (_searchctrl.text.isNotEmpty) {
        setState(() {
          isloading = true;
        });
        autoCompleteSearch(_searchctrl.text);
      } else {
        if (predictions.length > 0 && mounted) {
          setState(() {
            predictions = [];
          });
        }
      }


    });

  }


  void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    if (result != null && result.predictions != null && mounted) {
      setState(() {
        predictions = result.predictions;
        // isSelected = false;
      });
      getDetils();
    }
  }

    void getDetils([String placeId]) async {
      showPredictions.clear();
      for(AutocompletePrediction predict in predictions){
        var result = await this.googlePlace.details.get(predict.placeId);
        if (result != null && result.result != null && mounted) {
          setState(() {
            showPredictions.add(result.result);
            isloading = false;
          });
        }
      }
  }




  @override
  Widget build(BuildContext context) {
    focusNode.requestFocus();
    return Scaffold(
      appBar: RentMeAppBar.create(
        onBackPressed: (){
          Navigator.pop(context);
        }
      ),
      body: Container(
        color: RentMeColors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: AppTextField(
                        controller: _searchctrl,
                        borderColor: RentMeColors.transparent,
                        showCountryEntry: false,
                        placeholder: "search by location...",
                        focusNode: focusNode,
                        validations: [
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: !Utils.isEmptyOrNull(_searchctrl.text) ? 
                showPredictions.length > 0? ListView.builder(
                  itemCount: showPredictions.length, //searchedRents.length,
                  itemBuilder: (BuildContext ctx, index){
                    return InkWell(
                      onTap: (){
                        Navigator.pop(context, showPredictions[index].name);
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.all(10),
                            child: ListTile(
                              contentPadding: EdgeInsets.all(0),
                              title: Text(showPredictions[index].name),
                              subtitle: Padding(
                                padding: const EdgeInsets.only(top:5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(CupertinoIcons.location, color: Colors.redAccent,),
                                    Text(showPredictions[index].formattedAddress),
                                  ],
                                ),
                              )
                            )
                          ),
                          Divider()
                        ],
                      ),
                    );
                  },
                ):Align(
                  alignment: Alignment.center,
                  child: RentmeProgressIndicator(),
                )
                :
                Align(
                  alignment: Alignment.center,
                  child: Container() //RentmeProgressIndicator(),
                ),
              ),
            )
          ],
        ),
        ),
    );
  }

  

  






}