import 'dart:async';
import 'dart:io';

import 'package:bubble/bubble.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/message_bloc.dart';
import 'package:rentme_ghana/src/model/message.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:url_launcher/url_launcher.dart';


class ChatDetail extends StatefulWidget{

  final Rents rentData;
  final String id;
  final Message messageData;

  const ChatDetail({Key key, this.rentData, this.id, this.messageData}) : super(key: key);


  @override
  State<StatefulWidget> createState() {
    return ChatDetailState();
  }

}


class ChatDetailState extends State<ChatDetail>{

  UserSettings _userSettings;
  MessageBloc _messageBloc = MessageBloc();
  TextEditingController ctrl = TextEditingController();
  List<Message> messages = List();
  bool _isloading = true;
  ScrollController _scrlCtrl = ScrollController();
  Timer timer;
  String onlineStatus = "last seen today at 11:15";
  

  fetchMessages(){
    _messageBloc.getMessages(widget.id, "${!Utils.isEmptyOrNull(widget.rentData)?widget.rentData.posterId:widget.messageData.senderId}");
  }
  changeMsgStatus(){
    List<String> included = List();
    for (var msg in messages) {
      if(msg.messageStatus == "0" && msg.receiverId == "${_userSettings.getUser().id}"){
        // use this condition in blueticking the sender
        // the receiver opens the chat and we check if its unread and the receiver id is his
        // then we change the status, hence the sender gets bluetick
        included.add(msg.id);
      }
    }
    // this means there are some messages related to this chat which is still unread
    /// change status based on [messageId] collected
    if(included.length > 0){
      Utils.log(included);
      _messageBloc.changeMsgStatus(included);
    }
  }

  refreshChat(){
  timer = Timer.periodic(Duration(seconds: 1), (timer) {
      fetchMessages();
   });
}


  @override
  void initState() {
    refreshChat();
    listeners();
    fetchMessages();
    super.initState();
  }

  @override
  void dispose() { 
    if(timer != null){
      timer.cancel();
    }
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
     _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    //  fetchMessages();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black.withOpacity(0.5),
        title: Row(
          children: [
            Container(
              width: 38,
              height: 38,
              // picture: Image.asset("assets/images/beauty.jpg",fit: BoxFit.cover,height: 30.0, width: 30.0,)
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: CachedNetworkImage(
                  imageUrl: !Utils.isEmptyOrNull(widget.messageData) ? Constants.SPACE_URL+"users/"+widget.messageData.senderImage??"src/images/default.jpg":Constants.RM_API_BASE_URL+"/"+widget.rentData.posterImage??"src/images/default.jpg",
                  placeholder: (context, url) => RentmeProgressIndicator(),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: Text(!Utils.isEmptyOrNull(widget.rentData) ? widget.rentData.posterName:widget.messageData.senderName, style: AppStyles.textBodyDefault.copyWith(color:Colors.white, fontWeight: FontWeight.bold,),overflow: TextOverflow.clip,)
                ),
                SizedBox(height: 5,),
                Text(onlineStatus, style: AppStyles.textBodyDefault.copyWith(fontSize: 14, color: Colors.white),),
              ],
            )
          ],
        ),
        // actions: <Widget>[
        //   IconButton(
        //     icon: Icon(Icons.phone),
        //     color: Colors.white,
        //     onPressed: (){
        //       print("call");
        //     },
        //   )
        // ],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 9,
              child: Container(
                child: _isloading ? Center(child: RentmeProgressIndicator(),):
                
                !fileChosen ? _buldChat(): 
                Container(
                  color: Colors.red,
                  child:Image(
                      image: FileImage(img),
                      fit: BoxFit.cover,
                    ),
                )
              ),
            ),
            Divider(),
            Container(
              height: 70,
              // color: Colors.grey,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 8,
                      child: Container(
                        margin: EdgeInsets.only(left: 10, right:  10),
                        child: TextField(
                          controller: ctrl,
                          decoration: InputDecoration(
                            hintText: "type a message",
                            border: InputBorder.none,
                          ),
                          maxLines: 5,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Row(
                        children: [
                          IconButton(
                            icon: Icon(Icons.camera_alt_outlined, color: Colors.blue,),
                            onPressed: loadAssets,
                          ),
                          IconButton(
                            icon: Icon(Icons.send, color: Colors.blue,),
                            onPressed: _sendmessage,
                          ),
                        ],
                      ),
                      )
                  ],
                ),
              ),
          ],
        ),
        ),
    );
  }

File img;
bool fileChosen = false;
  Future<void> loadAssets() async {
    img = await Utils.captureImageFromCamera(quality: 60,type: CameraOptions.CAMERA);
    img = await Utils.compressImageFile(img, 40);
    setState(() {
      fileChosen = true;
    });
  }


  _sendmessage(){
    // Utils.log("value");
    // connection.connect();
    var currentDate = new DateTime.now();
    if(!Utils.isEmptyOrNull(ctrl.text) || !Utils.isEmptyOrNull(img)){
      Message message = Message()
      ..message = ctrl.text
      ..messageStatus = "0"
      ..receiverId = !Utils.isEmptyOrNull(widget.rentData) ? widget.rentData.posterId:widget.messageData.senderId
      ..senderId = "${_userSettings.getUser().id}"
      ..date = DateFormat('d-MMM-yyyy hh:mm a').format(currentDate)
      ..senderName = _userSettings.getUser().name
      ..receiverName = !Utils.isEmptyOrNull(widget.rentData)? widget.rentData.posterName:widget.messageData.senderName
      ..senderImage = _userSettings.getUser().profile
      ..receiverimage = !Utils.isEmptyOrNull(widget.rentData)? widget.rentData.posterImage:widget.messageData.receiverimage
      ..productName = !Utils.isEmptyOrNull(widget.rentData) ? widget.rentData.type: widget.messageData.productName
      ..messageFile = !Utils.isEmptyOrNull(img)? img.path:""
      ..imageFile = !Utils.isEmptyOrNull(img)? img:null;
       _messageBloc.sendMessage(message);

      setState(() {
        messages.add(message);
        if(fileChosen){
          fileChosen = false;
          img = null;
        }
      });
       ctrl.text = "";
    }
  }


  listeners(){
    _messageBloc.messages.listen((messagelist) {
      setState(() {
        messages = messagelist;
        _isloading = false;
        changeMsgStatus();
      });
    });
    // when a message is sent instantly
    // _messageBloc.messageSent.listen((msg) {
    //   setState(() {
    //     messages.add(msg);
    //   });
    // });
  }


  Widget _buldChat(){
    //TODO: STILL WORK IN PROGRESS
    /// SHOWING THE [TODAY] Bubble is still work in progress
    return ListView(
      reverse: true,
      controller: _scrlCtrl,
      padding: EdgeInsets.all(20),
      children: [
        SizedBox(height:Dimens.marginBig),
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          padding: EdgeInsets.all(0),
          shrinkWrap: true,
          itemCount: messages.length,
          itemBuilder: (BuildContext ctx, index){
            return Container(
              child: messages[index].senderId == "${_userSettings.getUser().id}" ? 
              Bubble(
                margin: BubbleEdges.only(top: 10),
                nip: BubbleNip.rightBottom,
                child: Utils.isEmptyOrNull(messages[index].messageFile)?  
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Flexible(child: Text(messages[index].message, textAlign: TextAlign.right,)),
                      SizedBox(width: 5,),
                      Padding(
                        padding: const EdgeInsets.only(top:4.0),
                        child: Text(datef(messages[index].date), textAlign: TextAlign.right,style: AppStyles.textBody2.copyWith(fontSize: 10, color: RentMeColors.black.withOpacity(0.6)),),
                      ),
                      // Icon(Icons.check, size: 15, color: Colors.blue,)
                      SizedBox(width: 2,),
                      Padding(
                        padding: const EdgeInsets.only(top:4.0),
                        child: FaIcon(FontAwesomeIcons.checkDouble,size: 10,color: messages[index].messageStatus == "1"? Colors.blue:Colors.black45,),
                      ), 
                    ],
                  )
                  :
                  Column(
                    children: [
                      Container(
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: CachedNetworkImage(
                              imageUrl: Constants.SPACE_URL+"messages/"+messages[index].messageFile,
                              placeholder: (context, url) => RentmeProgressIndicator(),
                              errorWidget: (context, url, error) => Icon(Icons.error),
                              fit: BoxFit.contain,
                              height: MediaQuery.of(context).size.width * 0.8,
                            ),
                          ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                           Padding(
                            padding: const EdgeInsets.only(top:4.0),
                            child: Text(datef(messages[index].date), textAlign: TextAlign.right,style: AppStyles.textBody2.copyWith(fontSize: 10, color: RentMeColors.black.withOpacity(0.6)),),
                          ),
                          // Icon(Icons.check, size: 15, color: Colors.blue,)
                          SizedBox(width: 2,),
                          Padding(
                            padding: const EdgeInsets.only(top:4.0),
                            child: FaIcon(FontAwesomeIcons.checkDouble,size: 10,color: Colors.blue,),
                          ),
                        ],
                      )
                    ],
                  ),
                  alignment: Alignment.topRight,
                  color: Color.fromRGBO(225, 255, 199, 1.0),
                )

                //other party
              :Bubble(
                margin: BubbleEdges.only(top: 10),
                nip: BubbleNip.leftTop,
                child: Utils.isEmptyOrNull(messages[index].messageFile)?  
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(child: Text(messages[index].message, textAlign: TextAlign.left)),
                    SizedBox(width: 5,),
                    Padding(
                      padding: const EdgeInsets.only(top:4.0),
                      child: Text(datef(messages[index].date), textAlign: TextAlign.right,style: AppStyles.textBody2.copyWith(fontSize: 10, color: RentMeColors.black.withOpacity(0.6)),),
                    ),
                  ],
                )
                :
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: CachedNetworkImage(
                                imageUrl: Constants.SPACE_URL+"messages/"+messages[index].messageFile,
                                placeholder: (context, url) => RentmeProgressIndicator(),
                                errorWidget: (context, url, error) => Icon(Icons.error),
                                fit: BoxFit.contain,
                                height: MediaQuery.of(context).size.width * 0.8,
                              ),
                            ),
                        ),
                         Padding(
                          padding: const EdgeInsets.only(top:4.0),
                          child: Text(datef(messages[index].date), textAlign: TextAlign.right,style: AppStyles.textBody2.copyWith(fontSize: 10, color: RentMeColors.black.withOpacity(0.6)),),
                        ),
                      ],
                    ),
                alignment: Alignment.topLeft,
              )
            );
          },
        ),
        Bubble(
          alignment: Alignment.center,
          color: Color.fromRGBO(212, 234, 244, 1.0),
          child: Text('TODAY', textAlign: TextAlign.center, style: TextStyle(fontSize: 11.0)),
        ),
      ],
    );
  }

    String datef(String date){
    if(!Utils.isEmptyOrNull(date)){



      try {
        DateTime tempDate = new DateFormat("d-MMM-yyyy hh:mm a").parse(date);
        var stringDate = DateFormat('hh:mm a').format(tempDate);
        return stringDate;
        
      } catch (e) {
        //12-Jan-2021
        DateTime tempDate = new DateFormat("dd-MMM-yyyy").parse(date);
        var stringDate = DateFormat('hh:mm a').format(tempDate);
        return stringDate;
      }



    }
    return "";
  }



}