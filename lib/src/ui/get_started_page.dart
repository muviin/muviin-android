
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/model/intro_service.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/icons.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/ui/login.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';
import 'package:rentme_ghana/src/ui/widgets/page_indicator.dart';

class GetStartedPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _GetStartedPageState();
  }
}

class _GetStartedPageState extends State<GetStartedPage> {

  int totalPages = 3;
  int page = 0;
  IntroService introService = IntroService();
  

  @override
  void initState() {
    super.initState();

    
  }



  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    // _userSettings = Provider.of<UserSettings>(context);

    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              // Container(
              //   padding: EdgeInsets.only(top: 19),
              //   child: AppIcon.appLogo.drawSvg(
              //     ),
              // ),

              Expanded(
                flex: 9,
                child: __buildPager(),
              ),
              


              // Container(
              //   padding: EdgeInsets.only(top: 30),
              //   child: Text(
              //     "RENTME",
              //     style: AppStyles.textTitleDefualt.copyWith(color: RentMeColors.colorAccent),
              //   )
              // ),
              // Container(
              //   padding: EdgeInsets.only(top: 48, left: 80, right: 80),
              //   child: Text(
              //     "Upload Your House for sale or for rent purposes",
              //     style: AppStyles.textBodyDefault.copyWith(color: RentMeColors.bpPurpleVeryLight),
              //     textAlign: TextAlign.center,
              //   )
              // ),
              // Expanded(
              //   flex: 6,
              //   child: Container(
              //     padding: EdgeInsets.symmetric(horizontal: 20),
              //     width: MediaQuery.of(context).size.width,
              //     child: AppIcon.getSartedIcon.drawSvg(
              //     ),
              //   ),
              // ),
              Expanded(
                flex: 2,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      Container(
                        child: PagerStatusIndicatorSmall(
                          activePage: this.page,
                          totalPages: this.totalPages,
                          action: () {
                            setState(() {

                            });
                          },
  
                          hideButton: false, 
                          instruction: "Next",
                        ),
                      ),
                      SizedBox(height: 16),
                      RentmeButton(
                        text:"Get Started",
                        onPressed: (){
                          Navigator.pushReplacement(
                            context, 
                            MaterialPageRoute(
                              builder: (BuildContext ctx){
                                return Login();
                              }
                            )
                          );
                          // BPNavs.navigatePush(context: context, page: TermsPage());
                        },
                      ),
                    ],
                  )
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


  Widget views(int i){
    IntroService servce = introService.getServices()[i];
    return Column(children: [
      Container(
        padding: EdgeInsets.only(top: 30),
        child: Text(
          "Muviin",
          style: AppStyles.textTitleDefualt.copyWith(color: RentMeColors.bpPurpleVeryLight),
        )
      ),
      Container(
        padding: EdgeInsets.only(top: 48, left: 80, right: 80),
        child: Text(
          servce.message,
          style: AppStyles.textBodyDefault.copyWith(color: RentMeColors.bpPurpleVeryLight),
          textAlign: TextAlign.center,
        )
      ),
      Expanded(
        flex: 6,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          width: MediaQuery.of(context).size.width,
          child: servce.icon.drawSvg() //AppIcon.getSartedIcon.drawSvg(
        ),
      ),
    ],);
  }

  __buildPager(){
    return Container(
      child: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
        },
        child: PageView.builder(
          itemCount: introService.getServices().length,
          itemBuilder: (BuildContext ctx, index){
            return views(index);
          },
          onPageChanged: (i){
            setState(() {
              page = i;
            });
          },
        ),
      ),
    );
  }

 
}
