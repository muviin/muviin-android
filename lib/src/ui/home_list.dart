import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/rent_bloc.dart';
import 'package:rentme_ghana/src/model/notifications.dart';
import 'package:rentme_ghana/src/model/rent_types.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/icons.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/services/ui.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/details.dart';
import 'package:rentme_ghana/src/ui/search.dart';
import 'package:rentme_ghana/src/ui/serach_rent.dart';

import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/ui/widgets/search_locations.dart';
import 'package:rentme_ghana/src/ui/widgets/text_field.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/utils_date.dart';
import 'package:rentme_ghana/src/utils/validators.dart';


class HomeList extends StatefulWidget{
  final User user;
  final String location;
  final Notifications notifications;

  const HomeList({Key key, this.user, this.location, this.notifications}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return HomeListState();
  }

}


class HomeListState extends State<HomeList>{
  final GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();

  bool _isLoading = true;
  RentBloc _rentBloc;
  List<Rents> _rentList;
  List<Rents> _nearRentList = [];
  UserSettings _userSettings;
  PageController pCtrl = PageController(viewportFraction: 0.8);
  ScrollController _scrollCtrl = ScrollController();
  bool _showDownButton = false; 



fetchRents(String type){
  _rentBloc.fetchRent(widget.user.country, type);
}

listeners(){
  if(_rentBloc == null){
    _rentBloc = RentBloc();

    // init listing of rents
    _rentBloc.rents.listen((rents){
      setState(() {
        _rentList = rents.reversed.toList();
        List<Rents> _nearList = [];
        for (var list in _rentList) {
          if(list.location.toLowerCase() == widget.location.toLowerCase()){
            _nearList.add(list);
          }
        }
        _nearRentList = _nearList;
        _isLoading = false;
      });
    });
  }

    _scrollCtrl.addListener(() {
        final scrollLimit = (_scrollCtrl.offset > 100);
        if(_showDownButton != scrollLimit) {
          setState(() {
            _showDownButton = scrollLimit;
          });
        }
    });
}



  Country _selectedCountry;

  Future<void> displaySearch(context) async {

    Rents rent = await Navigator.push(context, MaterialPageRoute(
      builder: (context) => SearchRent(rents: _rentList,user: widget.user,)
    ));

    if( rent != null){
      // Navigator.push(context, MaterialPageRoute(
      //   builder: (context) => Details(rent: rent, user: widget.user,)
      // ));
    }

  }

  void displayCountries(context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext contex){
        return Center(
          child: CountryPicker(
            showFlag: true,
            showName: true,
            onChanged: (country){
              print(country.name);
              setState(() {
                this._selectedCountry = country;
                Navigator.pop(context);
                _scaffold.currentState.showSnackBar(UI.displayNotification("displaying Houses from ${country.name}", false));
              });
            },
            selectedCountry: this._selectedCountry,
          )
        );
      }
    );
  }

ServicesModel servicesModel = ServicesModel();
void displayTypes(context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext contex){
        return Container(
          // height: (MediaQuery.of(context).size.height * 0.9), //180,
          child: customGrid(),
        );
      }
    );
  }

    Widget customGrid(){
    return Container(
      padding: EdgeInsets.only(top: 0, bottom:50),
      child: GridView.count(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
          crossAxisCount: 4,
          childAspectRatio: 1.0,
          mainAxisSpacing: 5.0,
          children: servicesModel.getServices().map((ServicesModel model) {
            return InkWell(
              onTap: (){
                _performAction(service: model);
              },
              child: GridTile(
                  footer: Container(
                    margin:EdgeInsets.only(bottom:0.0),
                    child: Center(child: Text(model.name, style: AppStyles.textBody2.copyWith(fontSize: 12),)),
                  ),
                    child: Container(
                      margin:EdgeInsets.only(bottom:0.0),
                      padding: EdgeInsets.only(top: 20,),
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 0),
                            height: 60,
                            width: 60,
                            decoration: BoxDecoration(
                              color: model.color.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(25)
                            ),
                            child: Image(
                              image: AssetImage(model.ico),
                            )
                          ),
                        ],
                      ),
                    )
                  ),
            );
          }).toList()),
    );
  }

   _performAction({ServicesModel service}){
     setState(() {
       _isLoading = true;
     });
    fetchRents(service.type.toString().split(".")[1]);
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    listeners();
    fetchRents(RentType.ALL.toString().split(".")[1]);
  }

  @override
  void dispose() {
    _rentBloc.dispose();
    super.dispose();
  }




  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    // _fetchLocalRents();
    return Scaffold(
      key: _scaffold,
      body: Container(
        child: Center(
          child: _isLoading ? RentmeProgressIndicator() :
         _rentList.length > 0? 
         Stack(
           children: [
             _buildList(context),
             _showDownButton ? Container(
                  alignment: Alignment.bottomRight,
                  margin: EdgeInsets.only(
                    bottom: 20,
                    right: Dimens.marginRegular,
                  ),
                  child: FloatingActionButton(
                    onPressed: () {
                      _scrollCtrl.jumpTo(
                        _scrollCtrl.position.minScrollExtent, 
                      );
                    },
                    mini: true,
                    backgroundColor: Colors.blueAccent,
                    child: Icon(Icons.arrow_upward, color: Colors.white,),
                  ),
                ):Container()
           ],
         ):
         Stack(
           children: [
             Column(
               crossAxisAlignment: CrossAxisAlignment.center,
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 AppIcon.emptyLi.drawSvg(size: 200),
                 Text("Sorry, no results found", style: AppStyles.textTitleDefualt.copyWith(color:RentMeColors.bpPurpleVeryLight),)
               ],
             ),
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: Icon(Icons.filter_alt, color: Colors.red,), 
                onPressed: (){
                  displayTypes(context);
                }
              ),
            )
           ],
         )
        ),
        ),
    );
  }
  
TextController _searchctrl = TextController();
  Widget _buildList(BuildContext context){
  return  NotificationListener<OverscrollIndicatorNotification>(
    onNotification: (overscroll) {
      overscroll.disallowGlow();
    },
    child: ListView(
      controller: _scrollCtrl,
      cacheExtent: 9999,
      children: [
        !Utils.isEmptyOrNull(widget.notifications.message)? Container(
          color: Colors.black,
          padding: EdgeInsets.all(15),
          child: Text(widget.notifications.message, style: AppStyles.textBody2.copyWith(color:Colors.white),),
        ):Container(),
        Row(
          children: [
            Expanded(
              child: GestureDetector(
                onTap: (){
                  displaySearch(context);
                },
                child: AbsorbPointer(
                  absorbing: true,
                  child: AppTextField(
                    controller: _searchctrl,
                    borderColor: RentMeColors.transparent,
                    showCountryEntry: false,
                    placeholder: "where would you like to stay",
                    validations: [
                      RequiredValueValidation(context),
                    ],
                  ),
                ),
              ),
            ),
            IconButton(
              icon: Icon(Icons.filter_alt, color: Colors.red,), 
              onPressed: (){
                displayTypes(context);
              }
            )
          ],
        ),

        Container(
          height: MediaQuery.of(context).size.height * 0.6,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                // child: CachedNetworkImage(
                //   imageUrl: Constants.POS_API_BASE_URL+"/src/images/notice/nice_img.jpg",
                //   placeholder: (context, url) => RentmeProgressIndicator(),
                //   errorWidget: (context, url, error) => Icon(Icons.error),
                //   fit: BoxFit.cover,
                // ),
                // child: FadeInImage.assetNetwork(
                //   placeholder: "assets/images/nice_img.jpg", 
                //   image: Constants.POS_API_BASE_URL+"/src/images/notice/nice_img.jpg",
                //   fit: BoxFit.cover,
                //   fadeInDuration: new Duration(milliseconds: 100),
                // ),

                child: Image(
                  image: AssetImage("assets/images/nice_img.jpg"),
                  fit: BoxFit.cover,
                ),
              ),


              Align(
                alignment: Alignment.bottomCenter,
                child: _getGradientShadeBelow(height: 225, topPosition: false) //_getGradientShade(height: 225, topPosition: false),
              ),


              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom:80.0, left: Dimens.marginRegular),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: Text(
                          "Search Closer",
                          style: AppStyles.textTitleDefualt.copyWith(color:Colors.white, fontSize: 30),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      RentmeButton(
                        text: "Nearby Rents",
                        size: ButtonSize.SMALL,
                        primaryColor: Colors.white,
                        textColor: Colors.black,
                        onPressed: (){
                          // SearchLocation(),
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (BuildContext ctx){
                                return MapSearch(user: _userSettings.getUser(),showAppBar: true,);
                              }
                            )
                          );
                        },
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height:20),
        !Utils.isEmptyOrNull(widget.location) && _nearRentList.length>0 ? Padding(
          padding: EdgeInsets.only(left: 16, bottom: 0, right: 16),
          child: Text("Rents Near you (${widget.location})", style: AppStyles.textTitleDefualt,),
        ):Container(),
        !Utils.isEmptyOrNull(widget.location) && _nearRentList.length>0 ?SizedBox(height:20):Container(),
        !Utils.isEmptyOrNull(widget.location) ?Container(
          height: _nearRentList.length>0? 300:0,
          child: _placesNear()
        ):Container(),
         SizedBox(height:20),
        Padding(
          padding: EdgeInsets.only(left: 16, bottom: 0, right: 16),
          child: Text("Explore", style: AppStyles.textTitleDefualt,),
        ),
         SizedBox(height:20),
        ListView.builder(
          cacheExtent: 9999,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: _rentList.length,
          itemBuilder: (context, i){
            return GestureDetector(
              child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 16, bottom: 8, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          _rentList[i].type,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18
                          ),
                        ),
                         _rentList[i].verify == "1"? Image(
                          image: AssetImage("assets/images/verify.jpg"),
                          height: 20,
                        ):Container(),

                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 16, right:16),
                    child: Container(
                      height: 240,
                      width: MediaQuery.of(context).size.width,
                      // decoration: BoxDecoration(
                      //   image: DecorationImage(
                      //     image:  NetworkImage(Constants.RM_API_BASE_URL+"/"+_rentList[i].thumbnail),
                      //     fit: BoxFit.cover
                      //   )
                      // ),
                      child: CachedNetworkImage(
                          imageUrl: Constants.SPACE_URL+"rents/"+_rentList[i].thumbnail,
                          placeholder: (context, url) => RentmeProgressIndicator(),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                          fit: BoxFit.cover,
                      ),
                    )
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 30,
                                width: 30,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  // image: DecorationImage(
                                  //   image: NetworkImage(Constants.RM_API_BASE_URL+"/"+_rentList[i].posterImage),
                                  //   fit: BoxFit.cover
                                  // )
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: CachedNetworkImage(
                                    imageUrl: Constants.SPACE_URL+"users/"+_rentList[i].posterImage,
                                    placeholder: (context, url) => RentmeProgressIndicator(),
                                    errorWidget: (context, url, error) => Icon(Icons.error),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left:10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      _rentList[i].posterName,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    Text(
                                      DateUtils.timeAgoSinceDate(_rentList[i].createdAt,format: "d-MMM-yyyy"),//_rentList[i].createdAt,
                                      style: TextStyle(
                                        color: Colors.black45
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Icon(CupertinoIcons.location),
                                        Text(
                                          _rentList[i].location,
                                          style: TextStyle(
                                            color: Colors.black45
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Text(
                          _rentList[i].rentType,
                          style: TextStyle(
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider()
                ],
              )
            ),
            onTap: (){
              print("selected");
              Utils.log(_rentList[i].thumbnail);
              Utils.log(_rentList[i].thumbnailArray);
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => Details(rent: _rentList[i], user: widget.user,)
              ));
            },
            );
          },
        ),
      ],
    ),
  );
}

Widget _placesNear(){
  return ListView.builder(
    // controller: pCtrl,
    scrollDirection: Axis.horizontal,
    itemCount: _nearRentList.length,
    itemBuilder: (context, i){
      return GestureDetector(
        child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 16, right:16),
              child: Container(
                height: 240,
                width: 300, //MediaQuery.of(context).size.width * 0.5,
                child: CachedNetworkImage(
                    imageUrl: Constants.SPACE_URL+"rents/"+_nearRentList[i].thumbnail, //Constants.RM_API_BASE_URL+"/"+_nearRentList[i].thumbnail,
                    placeholder: (context, url) => RentmeProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    fit: BoxFit.cover,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              child: Text(
                _nearRentList[i].type,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18
                ),
              ),
            ),
          ],
        )
      ),
      onTap: (){
        print("selected");
        Utils.log(_nearRentList[i].thumbnail);
        Utils.log(_nearRentList[i].thumbnailArray);
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => Details(rent: _nearRentList[i], user: widget.user,)
        ));
      },
      );
    },
  );
}


  _getGradientShadeBelow({double height = 100, bool topPosition = true}) {
    return Container(
      width: double.infinity,
      height: 300,//double.infinity, //height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,

          colors: [
            Colors.black,
            Colors.black,
            Colors.black,
            Colors.black.withOpacity(topPosition ? 0.0 : 0.0)
          ],
          stops: [
            0.0,
            0.05,
            0.1,
            6.1
          ]

        ),
      ),
    );
  }

  double addition = 50;
  Widget _loadWidget(){
    return Stack(
      children: [
        AnimatedContainer(
            duration: Duration(milliseconds: 500),
            height: 5,
            width: 200,
            decoration: BoxDecoration(
                borderRadius : BorderRadius.circular(5),
              color: Colors.redAccent.withOpacity(0.2)
            ),
          ),
        AnimatedContainer(
            duration: Duration(milliseconds: 500),
            height: 5,
            width: addition,
            decoration: BoxDecoration(
                borderRadius : BorderRadius.circular(5),
              color: Colors.redAccent
            ),
          ),
      ],
    );
  }



}
