import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/strings.dart';
import 'package:rentme_ghana/src/ui/widgets/poster_location.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/constants.dart';


class Poster extends StatefulWidget{
  final Rents rent;
  final User user;

  const Poster({Key key, this.rent, this.user}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PosterState();
  }

}

class PosterState extends State<Poster>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 0),
        child: Column(
          children: <Widget>[

            Expanded(
              flex: 2,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 6,
                    child: Container(
                      margin: EdgeInsets.only(left: 20, top: 50),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(widget.rent.posterName, style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),),
                          Container(height: 10,),
                          Text("Ghana"),
                          Image(
                            image: AssetImage("assets/images/verify.jpg"),
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Hero(
                      tag: "test",
                      child: Container(
                      margin: EdgeInsets.only(right: 30, top: 10),
                      width: 150,
                      height: 150,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(75),
                        child: CachedNetworkImage(
                          imageUrl: Constants.SPACE_URL+"users/"+widget.rent.posterImage,
                          placeholder: (context, url) => RentmeProgressIndicator(),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                          fit: BoxFit.cover,
                        ),
                      )
                      // decoration: BoxDecoration(
                      //   shape: BoxShape.circle,
                      //   image: DecorationImage(
                      //       image: NetworkImage(Constants.POS_API_BASE_URL+"/"+widget.rent.posterImage), //AssetImage("assets/images/beauty.jpg"),
                      //       fit: BoxFit.cover
                      //   ),
                      // ),
                    ),
                    )
                  )
                ],
              ),
            ),

            Expanded(
              flex: 2, //4
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                      Text("Bio", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),), 
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        padding: EdgeInsets.all(10),
                        height: 100,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12),
                          borderRadius: BorderRadius.all(
                            const Radius.circular(10.0)
                          )
                        ),
                        child: Center(child: Text("${widget.rent.posterName} is one of our most reliable customers who have agreed with the terms of rentme to upload either houses for rent or sale or a land for sale. ${widget.rent.posterName} ls located in ${widget.user.country}."),),
                      ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                child: Center(
                  child: PosterLocation(user: widget.user,rent: widget.rent,fromDetail: false,),
                ),
              ),
            )



          ],
        )
      ),
    );
  }
}