import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/res/colors.dart';

class PagerStatusIndicatorSmall extends StatelessWidget {

  const PagerStatusIndicatorSmall({Key key, @required this.action, @required this.instruction, @required this.totalPages, @required this.activePage, this.hideButton = false, this.onComplete})
    : assert(totalPages != null),
      assert(activePage != null),
      assert(action != null),
      assert(hideButton != null),
      super(key: key);

  final Function action;
  final String instruction;
  final int totalPages;
  final int activePage;
  final bool hideButton;
  final VoidCallback onComplete;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[

          Container(
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: getIndicators(context, totalPages),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> getIndicators(BuildContext context, int totalPages) {
  
    List<Widget> displayWidgets = [];

    for (var i = 0; i < totalPages; i++) {
      Widget w = AnimatedContainer(
        width: i == activePage ? 29 : 6,
        height: 7,
        margin: EdgeInsets.only(left: 2, right: 9, top: 10),
        decoration: BoxDecoration(
          color: i == activePage ? RentMeColors.colorPrimary : RentMeColors.colorPrimary.withOpacity(0.3),
          borderRadius: i == activePage ? BorderRadius.circular(4.0) : BorderRadius.circular(50),
        ),
        duration: Duration(milliseconds: 500),
        curve: Curves.bounceOut,
      );
      displayWidgets.add(w);
    }
    return displayWidgets;
  }
}