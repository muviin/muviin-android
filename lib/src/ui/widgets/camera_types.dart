
import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/res/enums.dart';


class CameraType extends StatelessWidget{

  final ValueChanged<String> onSelected;
  final String userImage;

  const CameraType({Key key, this.onSelected, this.userImage}) : super(key: key);


  Widget roundedOptions(BuildContext context,{String type, Widget icon, Color backgroundColor, CameraOptions options , Function action}){
    return GestureDetector(
      onTap: (){
        onSelected(options.toString().split(".")[1]);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: backgroundColor, 
            ),
            height: 60, 
            width: 60,
            child: icon,
          ),
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: Text(type),
          )
        ],
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 100, right:100), //20
      height: 120,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            roundedOptions(
              context,
              type: "CAMERA",//InvmLocalizations.of(context).camera,
              icon: Image(
                image: AssetImage("assets/images/cam.png"),
              ), //Icon(Icons.camera, size: 30,color: Colors.white,),
              backgroundColor: Colors.white,
              options: CameraOptions.CAMERA
            ),
            userImage != '' 
              ? roundedOptions(
                  context,
                  type: "GALLERY",//InvmLocalizations.of(context).gallery,
                  icon: Image(
                    image: AssetImage("assets/images/gallery.png"),
                  ),
                  // icon: Icon(Icons.image, size: 30,color: Colors.white,),
                  backgroundColor: Colors.white,
                  options: CameraOptions.GALLERY
                )
              : Container(),
              //TODO:
            // userImage != '' 
            //   ? roundedOptions(
            //       context,
            //       type: "REMOVE PHOTO",//InvmLocalizations.of(context).removePhotoN,
            //       icon: Icon(Icons.delete, size: 30,color: Colors.white,),
            //       backgroundColor: Colors.redAccent,
            //       options: CameraOptions.REMOVE
            //     )
            //   : roundedOptions(
            //       context,
            //       type: "GALLERY", //InvmLocalizations.of(context).gallery,
            //       icon: Icon(Icons.image, size: 30,color: Colors.white,),
            //       backgroundColor: Colors.purpleAccent,
            //       options: CameraOptions.GALLERY
            //     )
          ],
        ),
      ),
    );
  }

}