
import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';

class RadialChart extends StatelessWidget {
  // final double percentage;
  final List<double> percentages;
  final List<Color> colors;
  final double size;
  final double padding;
  final double radius;
  final TextStyle textStyle;
  final bool showCompletedLabel;
  final bool showPercentageLabel;

  const RadialChart({Key key, @required this.percentages, this.colors, this.size = 115, this.radius = 32, this.textStyle, this.showCompletedLabel = true, this.showPercentageLabel = true, this.padding = 0, }) : super(key: key);

  /// 
  /// Draw a radial chart with last 25% transparent, and middle 'grey' empty portion
  /// 
  List<CircularSegmentEntry> _calcPercentage(double value, Color color) {
    double diff = (value / 100) * 75;
    return [
      CircularSegmentEntry(
        diff,
        color,
      ),
      CircularSegmentEntry(
        75 - diff,
        RentMeColors.grey,
      ),
      CircularSegmentEntry(
        25,
        Colors.transparent,
      ),
    ];
  }

  List<CircularStackEntry> _getChartData() {
    List<CircularStackEntry> list = List();
    for(int i = 0; i < percentages.length; i++) {
      // spacing
      if(i > 0) {
        list.add(CircularStackEntry([CircularSegmentEntry(100, Colors.transparent)]));
      }
      list.add(CircularStackEntry(_calcPercentage(percentages.elementAt(i), colors.elementAt(i))));
    }
    return list;
  }
  
  @override
  Widget build(BuildContext context) {
    assert(percentages != null);
    assert(colors != null);

    if(percentages.length != colors.length) {
      throw(Exception('Length of percentages[] should match that of colors[]'));
    }

    final width = size; //- (Dimens.marginRegular * 1);
    // final style = textStyle.copyWith(
    //   fontSize: Dimens
    // );

    return Container(
      width: width,
      height: width,
      child: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: AnimatedCircularChart(
              size: Size(width, width),
              initialChartData: _getChartData(),
              chartType: CircularChartType.Radial,
              percentageValues: false,
              holeRadius: radius - (percentages.length * 6),
              edgeStyle: SegmentEdgeStyle.round,
              duration: Duration(milliseconds: 500),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                showPercentageLabel
                  ? Text(
                    _calcAverage().toStringAsFixed(0) + '%',
                      style: textStyle,
                    )
                  : Container(),
                showCompletedLabel
                  ? Text(
                      "Completed".toLowerCase(),
                      style: TextStyle(
                        color: textStyle?.color ?? RentMeColors.textPrimary,
                        fontSize: (textStyle?.fontSize ?? Dimens.textRegular) - (10 + (radius / 10)),
                      ),
                    )
                  : Container(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  double _calcAverage() {
    double total = 0;
    percentages.forEach((item) {
      total += item;
    });
    return total / percentages.length;
  }
  
}