import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// class RentmeProgressIndicator extends StatelessWidget {

//   final double width;
//   final Color color;

//   const RentmeProgressIndicator({Key key, this.width, this.color}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Platform.isIOS
//         ? CupertinoActivityIndicator()
//         : CircularProgressIndicator(
//             backgroundColor: color ?? Colors.blue,
//             valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
//             strokeWidth: width ?? 2,
//         );
//         //         ? CupertinoActivityIndicator()
//         // : Loading(colors: colors,);
//   }
// }

class RentmeProgressIndicator extends StatelessWidget {

  final double width;
  final Color color;

  const RentmeProgressIndicator({Key key, this.width, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoActivityIndicator();
  }
}
