import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart' as Geo;
import 'package:google_place/google_place.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/rent_bloc.dart';
import 'package:rentme_ghana/src/blocs/verification_block.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/details.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart' as piker;

class SearchLocation extends StatefulWidget{

  final User user;
  final UserSettings userSettings;
  

  const SearchLocation({Key key, this.user, this.userSettings,}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SearchLocationState();
  }

}


class SearchLocationState extends State<SearchLocation>{

static const kGoogleApiKey = "AIzaSyDWg6uFUfbxGf0zEKtUorrkJRAo6I00Kvg";
Set<Marker> markers;
 Completer<GoogleMapController> _controller = Completer();
 PageController _pageController = PageController(viewportFraction: 0.85);
 bool isSelected = false;
 DetailsResult selectedAddress;
 bool mapLoading = true;

  piker.Country _selectedCountry;

  RentBloc _rentBloc;
  List<Rents> _rentList;
  UserSettings _userSettings;



  GooglePlace googlePlace;
  List<AutocompletePrediction> predictions = [];
  List<DetailsResult> showPredictions = [];
  List<String> li = [];

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(7.9465, 1.0232),
    zoom: 15.4746,
  );


  displayMarker(Rents rents) async {
    ImageConfiguration imgconfig = ImageConfiguration(size: Size(0.2, 0.2));
    Marker mm  =  Marker(
      markerId: MarkerId(rents.type),
      position: LatLng(double.parse(rents.lat), double.parse(rents.long)),
      infoWindow: InfoWindow(title: rents.type),
      icon: BitmapDescriptor.defaultMarkerWithHue(10), //await BitmapDescriptor.fromAssetImage(imgconfig, "assets/images/verify.jpg"),
      onTap: (){
        print(rents.type);
        _animateRoute(double.parse(rents.lat), double.parse(rents.long));
        _showSelectRentDialog(rents);
      },
    );

    setState(() {
      markers.add(mm);
    });
  }


  fetchRents(){
    // _rentBloc.allRents.listen((rents){
    
      // setState(() {
      //   _rentList = rents;
      // });
      // _getCurrentLocation();
    // });
    // _rentBloc.fetchAllRents();
    _rentBloc.fetchRent(widget.user.country, RentType.ALL.toString().split(".")[1]);
  }

  _fetchLocalRents(){
  var localRents = widget.userSettings.getRents();
  List<Rents> list = List();
  for (var i = 0; i < localRents.length; i++) {
    list.add(Rents.fromJson(localRents[i]));
  }

  setState(() {
    _rentList = list.reversed.toList();
    _getCurrentLocation();
  });
}


  listeners(){
  if(_rentBloc == null){
    _rentBloc = RentBloc();
    _rentBloc.rents.listen((rents){
      setState(() {
        _rentList = rents;
      });
      _getCurrentLocation();
    });
  }
}




  @override
  void initState() {
    googlePlace = GooglePlace(kGoogleApiKey);

    markers = Set.from([]);

    // listeners();

    // fetchRents();

    _selectedCountry = piker.Country(
      name: widget.user.country,
      currency: "Ghanaian cedi",
      asset: "assets/flags/gh_flag.png",
      currencyISO: "GHS",
      dialingCode: "233",
      isoCode: "GH"
  );


    _fetchLocalRents();


    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);

    return Scaffold(
      body: Container(
        child: !mapLoading ? 
        Stack(
          children: [
            GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: _kGooglePlex,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              markers: markers,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.symmetric(vertical: Dimens.marginRegular),
                height: 130,
                width: MediaQuery.of(context).size.width,
                child: _buildHorizontal(),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 40,
                margin: EdgeInsets.symmetric(vertical: Dimens.marginRegular),
                width: MediaQuery.of(context).size.width,
                child: _buildCountryChoose(),
              ),
            )
          ],
        ):
        Center(
          child: RentmeProgressIndicator(),
        )
      ),
    );
  }

  final Geo.Geolocator geolocator = Geo.Geolocator()..forceAndroidLocationManager;
  _getCurrentLocation() async {

    // check fr accepted permissions
    VerificationBloc _vb = VerificationBloc();
    _vb.pushPermissionCheck(
      onSuccess: (status) async {

        Geo.Position pos = await geolocator.getCurrentPosition(desiredAccuracy: Geo.LocationAccuracy.best);
        _getAddressFromLatLng(pos);

      },
      onFailure: (status){
        if(status == PermissionState.ENABLE){
          Utils.log(status);
          autoCompleteSearchNew(widget.userSettings.getUser().country);
          setState(() {
            mapLoading = false;
          });
        }
        else if(status == PermissionState.REQUEST){
          Utils.log(status);
          showSingleOptionDialog(context, "Location Permission, accept and try again", "Ok", (){
            Navigator.pop(context);
          });
        }
      }
    );
  }

  _getAddressFromLatLng(Geo.Position mm) async {
    try {
      List<Geo.Placemark> p = await geolocator.placemarkFromCoordinates(mm.latitude, mm.longitude);
      Geo.Placemark place = p[0];
      double lat = place.position.latitude;
      double long = place.position.longitude;

      setState(() {
        mapLoading = false;
      });

      // animate to selected location
      _animateRoute(lat, long, place);
    } catch (e) {
      print(e);
    }
  }

  _animateRoute(double lat, long, [Geo.Placemark place]) async{
    CameraPosition cp = CameraPosition(
      target: LatLng(lat, long),
      tilt: 59.440717697143555,
      zoom: 12.151926040649414,
        bearing: 192.8334901395799,
      );
      final GoogleMapController controller = await _controller.future;
      // displayMarker(lat, long);
      controller.animateCamera(CameraUpdate.newCameraPosition(cp));


      for(Rents rent in _rentList){
        displayMarker(rent);
      }
  }

  // end



  // TODO: getting location by search



  Widget _getInputField(){
    return Container(
      height: !isSelected ? MediaQuery.of(context).size.height * 0.5:140,
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(
              labelText: "Search",
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2.0,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.black54,
                  width: 2.0,
                ),
              ),
            ),
            onChanged: (value) {
              if (value.isNotEmpty) {
                autoCompleteSearch(value);
              } else {
                if (predictions.length > 0 && mounted) {
                  setState(() {
                    predictions = [];
                  });
                }
              }
            },
          ),
          Container(
            height: !isSelected ? MediaQuery.of(context).size.height * 0.4:0,
            child: ListView.builder(
              itemCount: showPredictions.length,
              itemBuilder: (BuildContext ctx, index){
                return !isSelected ? Container(
                  color: Colors.white,
                  padding: EdgeInsets.only(left:20, right: 20, top: 20),
                  child: InkWell(
                    onTap: (){
                      _getAddress(showPredictions[index]);
                    },
                    child: Container(
                      padding: EdgeInsets.only(bottom: 20),
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(color: Colors.grey)
                        )
                      ),
                      
                      child: Row(
                        children: [
                          Icon(CupertinoIcons.location),
                          SizedBox(width: 10),
                          Text(showPredictions[index].formattedAddress),
                        ],
                      ),
                    ),
                  ),
                ): Container();
              },
            ),
          )
        ],
      ),
    );
  }


   void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    if (result != null && result.predictions != null && mounted) {
      setState(() {
        predictions = result.predictions;
        isSelected = false;
      });
      getDetils();
    }
  }

    void getDetils([String placeId]) async {
      showPredictions.clear();
      for(AutocompletePrediction predict in predictions){
        var result = await this.googlePlace.details.get(predict.placeId);
        if (result != null && result.result != null && mounted) {
          setState(() {
            showPredictions.add(result.result);
          });
        }
      }
  }

    _getAddress(DetailsResult addressResult) async {
      this.selectedAddress = addressResult;
      setState(() {
        isSelected = true;
      });
    try {
      double lat = addressResult.geometry.location.lat;
      double long = addressResult.geometry.location.lng;

      // animate to selected location
      _animateRouteFromSearch(lat, long,);
    } catch (e) {
      print(e);
    }
  }

  _animateRouteFromSearch(double lat, long, {double zoom = 19.151926040649414, double bearing = 192.8334901395799}) async{
    CameraPosition cp = CameraPosition(
      target: LatLng(lat, long),
      tilt: 59.440717697143555,
      zoom: zoom,
        bearing: bearing
      );
      final GoogleMapController controller = await _controller.future;

      // asign icon to location
      for(Rents rent in _rentList){
        displayMarker(rent);
      }

      controller.animateCamera(CameraUpdate.newCameraPosition(cp));


  }



  //end



  Future<Null> _showSelectRentDialog(Rents rent) async {
    return await showRoundedModalBottomSheet<Null>(
      dismissOnTap: true,
      radius: 10.0,
      autoResize: true,
      color: Colors.black.withOpacity(0.7),
      context: context,
      builder: (BuildContext contextt) {
        return InkWell(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => Details(rent: rent, user: _userSettings.getUser(),)
            ));
          },
          child: Container(
            height: MediaQuery.of(context).size.height * 0.4,
            child: Stack(
              children: [
                CachedNetworkImage(
                  imageUrl: Constants.SPACE_URL+"rents/"+rent.thumbnail,
                  placeholder: (context, url) => RentmeProgressIndicator(),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  fit: BoxFit.cover,
                  width: MediaQuery.of(contextt).size.width,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 20, left: 20, top: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          rent.type,
                          style: TextStyle(
                            fontSize: 20,
                            color: RentMeColors.white
                          ),
                        ),
                        Text(
                          rent.location,
                          style: TextStyle(
                            fontSize: 20,
                            color: RentMeColors.white
                          ),
                        ),
                        Text(
                          rent.country,
                          style: TextStyle(
                            fontSize: 20,
                            color: RentMeColors.white
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      }
    );
  }


  Widget _buildHorizontal(){
    return PageView.builder(
      controller: _pageController,
      itemCount: _rentList.length,
      onPageChanged: (index){
        var lat = _rentList[index].lat;
        var long = _rentList[index].long;
        _animateRouteFromSearch(double.parse(lat), double.parse(long));
      },
      itemBuilder: (BuildContext ctx, index){
        return InkWell(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => Details(rent: _rentList[index], user: widget.user,)
            ));
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  // spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(2, 3), // changes position of shadow
                ),
              ],
            ),
            margin: EdgeInsets.only(right:0, left: 10),
            child: Row(
              children: [
                Expanded(
                  flex: 4,
                  child: Container(
                    height: 130,
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(16),
                        bottomLeft: Radius.circular(16),
                      ),
                      child: CachedNetworkImage(
                        imageUrl: Constants.SPACE_URL+"rents/"+_rentList[index].thumbnail,
                        placeholder: (context, url) => RentmeProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                ),
                Expanded(
                  flex: 8,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 150,
                              child: Text(_rentList[index].type, style: AppStyles.textBodyDefault,overflow: TextOverflow.ellipsis,)
                            ),
                            SizedBox(height:24),
                            _buildRow(_rentList[index].location+"-"+_rentList[index].country, Icons.location_on,),
                            SizedBox(height:5),
                            _buildRow(_rentList[index].getCalculatedPrice(_userSettings), Icons.attach_money_rounded,),
                            SizedBox(height:5),
                            
                          ],
                        ),
                      ),
                      Padding(
                         padding: EdgeInsets.all(10),
                        child: Text(_rentList[index].rentType, style: AppStyles.textBody2.copyWith(color: RentMeColors.bpPurpleVeryLight),),
                      ),
                    ],
                  )
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildCountryChoose(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 100),
      padding: EdgeInsets.all(3),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            // spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(2, 3), // changes position of shadow
          ),
        ],
      ),
      child: CountryPicker(
        showFlag: false,
        showName: true,
        selectedCountry: _selectedCountry,
        onChanged: (country){
          setState(() {
            this._selectedCountry = country;
            autoCompleteSearchNew(country.name);
          });
        }
      ),
    );
  }

  void autoCompleteSearchNew(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    if (result != null && result.predictions != null && mounted) {
      getDetilsNew( result.predictions.first.placeId);
    }
  }

    void getDetilsNew([String placeId]) async {
      showPredictions.clear();
        var result = await this.googlePlace.details.get(placeId);
        if (result != null && result.result != null && mounted) {
          Utils.log(result.result);
          // animate to view
          var cordinates = result.result.geometry.location;
           _animateRouteFromSearch(cordinates.lat, cordinates.lng, zoom: 5, bearing: 180);
        }
  }

  Widget _buildRow(String value,IconData icon){
    return Row(
      children: [
        Icon(icon,size: 16,color: Colors.red,),
        Text(value, style: AppStyles.textBody2.copyWith(color: Colors.black45, fontSize: 13),),
      ],
    );
  }














}





