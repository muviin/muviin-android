import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class RentmeButton extends StatelessWidget {
  RentmeButton({
    Key key,
    this.primary = true,
    this.text,
    // this.icon,
    this.iconSize,
    this.textColor = Colors.white,
    this.onPressed,
    this.size = ButtonSize.NORMAL,
    this.primaryColor = RentMeColors.colorPrimary,
    this.isLoading = false,
    this.isEnabled = true, 
    this.fullButtonTextAlignment,
    // this.append,
  });

  final primary;
  final String text;
  // final AppIcon icon;
  final Color textColor;
  final Function onPressed;
  final ButtonSize size;
  final double iconSize;
  final Color primaryColor;
  // final Widget append;
  final Alignment fullButtonTextAlignment;
  final bool isLoading;
  final bool isEnabled;

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = _determineTextStyle().copyWith(
      color: primary
        ? textColor
        : RentMeColors.colorPrimary,
    );

        return Container(
          decoration: BoxDecoration(

            boxShadow: primary 
              ? [
                  BoxShadow(
                    color: RentMeColors.colorShadow,
                    blurRadius: 6,
                    offset: Offset(0, 3)
                  ),
                ]
              : [],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: ShaderMask(
              shaderCallback: (rect) {
                return LinearGradient(colors: [ Colors.white, Colors.white ]).createShader(rect);
              },
              child: Material(
                color: primary
                  ? ((!isLoading && isEnabled)
                      ? primaryColor
                      : primaryColor.withBlue(180).withGreen(180).withRed(180))
                  : Colors.transparent,
                child: InkWell(
                  onTap: !isLoading && isEnabled 
                    ? () => onPressed != null ? onPressed() : null
                    : null,
                  child: Container(
                    constraints: BoxConstraints(
                      minWidth: _determineMinWidth(),
                    ),
                    height: _determineHeight(),
                    child: Container(
                      padding: _determinePadding(),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                          color: primary ? Colors.transparent : RentMeColors.colorPrimary,
                          width: Dimens.dividerSmall,
                          style: BorderStyle.solid,
                        ),
                      ),
                    child: !isLoading
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: size == ButtonSize.FULL ? MainAxisSize.max : MainAxisSize.min,
                          children: <Widget>[

                            _determineTextWidget(textStyle),

                            // icon 
                            // icon != null 
                            //   ? Padding(
                            //     padding: EdgeInsets.only(left: (text != null) ? Dimens.marginSmall : 0),
                            //       child: icon.draw(
                            //         color: textStyle.color,
                            //         // size: iconSize ?? (textStyle.fontSize + 8),
                            //         size: iconSize ?? 18,
                            //       ),
                            //     )
                            //   : Container()
                      ],
                    )
                    : SizedBox(
                      height: 20.0,
                      width: 20.0,
                      child: RentmeProgressIndicator(),
                    ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  double _determineHeight() {
    switch(size) {
      case ButtonSize.TINY:
        return 33;

      case ButtonSize.SMALL:
      case ButtonSize.MEDIUM:
        return 40;
        
      default: 
        return 48;
    }
  }

  double _determineMinWidth() {
    switch(size) {
      case ButtonSize.NORMAL:
      case ButtonSize.COMPACT:
        return 157;
      case ButtonSize.MEDIUM:
        return 100;
      default: 
        return 38;
    }
  }

  EdgeInsets _determinePadding() {
    switch(size) {
      case ButtonSize.TINY:
        return EdgeInsets.symmetric(horizontal: Dimens.marginTiny);
      
      case ButtonSize.SMALL:
      case ButtonSize.ICON:
        return EdgeInsets.symmetric(horizontal: Dimens.marginSmall);
      default: 
        // if(text != null && icon != null) {
        //   return EdgeInsets.only(left: 24, right: 14);
        // }
        
      return EdgeInsets.symmetric(horizontal: Dimens.marginRegular);
    }
  }

  TextStyle _determineTextStyle() {
    switch(size) {
      case ButtonSize.TINY:
        return AppStyles.textBody3;
      case ButtonSize.SMALL:
      case ButtonSize.ICON:
        return AppStyles.textBody2;
      default:
        return AppStyles.textBodyDefault;
    }
  }

  Widget _determineTextWidget(TextStyle textStyle) {
    switch(size) {

      case ButtonSize.FULL:
        return Expanded(
          child: !Utils.isEmptyOrNull(text) 
            ? Container(
              alignment: fullButtonTextAlignment ?? Alignment.center,
              child: Text(
                  text,
                  overflow: TextOverflow.ellipsis,
                  style: textStyle,
                ),
            )
            : Container(),
        );
      
      default:
        return !Utils.isEmptyOrNull(text) 
          ? Container(
            alignment: fullButtonTextAlignment ?? Alignment.center,
            child: Text(
                text,
                overflow: TextOverflow.ellipsis,
                style: textStyle,
              ),
          )
          : Container();
    }
  }

}
