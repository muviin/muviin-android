
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/res/abstracts.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/utils_responsive.dart';

class AppTextField extends StatefulWidget {
  final TextController controller;
  final String title;
  final String placeholder;
  final String hint;
  final TextInputType keyboardType;
  final TextCapitalization textCapitalization;
  final FocusNode focusNode;
  final bool isPassword;
  final bool isEnabled;
  final Function(String) onSubmitted;
  final Function(Country) onCountrySelected;
  final List<TextInputFormatter> inputFormatters;
  final TextAlign textAlign;
  final Color borderColor;
  final Widget prepend;
  final Widget append;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final int minLines;
  final int maxLines;
  final int maxLength;
  final bool showCountryEntry;
  final bool showTextField;
  final List<Validator> validations;
  final Country selectedCountry;


  const AppTextField({
    Key key,
    this.controller,
    this.title,
    this.placeholder = '',
    this.keyboardType,
    this.isPassword = false,
    this.focusNode,
    this.onSubmitted,
    this.inputFormatters = const [],
    this.textAlign,
    this.borderColor = RentMeColors.colorBorder,
    this.prepend,
    this.append,
    this.hint,
    this.minLines,
    this.maxLines = 1,
    this.maxLength,
    this.isEnabled = true, 
    this.margin,
    this.textCapitalization,
    this.padding, 
    this.validations = const [], 
    this.showCountryEntry = true, this.onCountrySelected, this.selectedCountry, this.showTextField = true,
  }) : super(key: key);

  @override
  _AppTextFieldState createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  String get requiredStatus => (widget.controller?.isRequired == true ? '*' : '');
  UserSettings _userSettings;

  @override
  void initState() { 
    super.initState();
    
    // listen
    widget.controller.addListener(_ctrlListener);
  }

  void _ctrlListener() {
    setState(() {});
  }

  @override
  void dispose() { 
    widget.controller.removeListener(_ctrlListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _userSettings = Provider.of(context);
    
    if(widget.controller != null) {
      // set validations on controller
      widget.controller.validations = widget.validations;
      widget.controller.formatters = [
        ...widget.inputFormatters,
        ...(widget.maxLength != null ? [LengthLimitingTextInputFormatter(widget.maxLength)] : []),
      ];

      // widget.controller.countryList = _userSettings.getCountries().values.toList(); TODO:
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        widget.title != null
          ? Container(
            child: Text(
                '${widget.title} $requiredStatus',
                style: AppStyles.textBody2,
              ),
          )
          : Container(),

          _buildTextField(context)
      ],
    );
  }

  Widget _buildTextField(BuildContext context) {
    ResponsiveUtils().init(context);

    // get settings config from userSettings
    // Settings settings = Provider.of<UserSettings>(context).getSettingsConfig();
    
    return MediaQuery(
      // change the text scal of the textfield
      data: MediaQuery.of(context).copyWith(textScaleFactor: ResponsiveUtils.textScale(1)), //MediaQuery.of(context).copyWith(textScaleFactor: ResponsiveUtils.textScale(settings.fontSize.value)),
      child:  Container(
        // padding: widget.margin ?? const EdgeInsets.only(top: Dimens.marginSmall, bottom: Dimens.marginRegular),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Container(
                // height: 60,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(13),
                  border: Border.all(
                    color: widget.controller != null && !widget.controller.lastCheckedStatus ? RentMeColors.red:RentMeColors.transparent
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      // spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(2, 3), // changes position of shadow
                    ),
                  ],

                ),
                width: MediaQuery.of(context).size.width, //* 0.7,
                padding: EdgeInsets.only(left: 12, right:12, top: !widget.showTextField ? 10: 0, bottom: !widget.showTextField ? 10: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    widget.showCountryEntry ? Row(
                      children: [
                        CountryPicker(
                                  showFlag: true,
                                  showName: true,
                                  selectedCountry: widget.selectedCountry,
                                  onChanged: widget.onCountrySelected,

                        ),
                        // AppIcon.dropdown.drawSvg(size: 8),
                      ],
                    ):Container(),
                    Expanded(
                      flex: 2,
                      child: widget.showTextField ? TextFormField(
                        controller: widget.controller,
                        style: AppStyles.textBodyDefault.copyWith(
                          color: widget.controller != null && !widget.controller.lastCheckedStatus
                            ? RentMeColors.red
                            : (widget.isEnabled ? RentMeColors.textPrimary : RentMeColors.textPrimary.withOpacity(0.9)),
                        ),
                        textAlign: widget.textAlign ?? TextAlign.start,
                        onFieldSubmitted: (text) {
                          if(widget.onSubmitted != null) {
                            widget.onSubmitted(text);
                          }
                          if(widget.controller != null) {
                            return widget.controller.validate();
                          }
                          return '';
                        },
                        keyboardType: widget.keyboardType,
                        obscureText: widget.isPassword,
                        textCapitalization: widget.textCapitalization ?? TextCapitalization.none,
                        obscuringCharacter: '*',
                        enabled: widget.isEnabled,
                        focusNode: widget.focusNode,
                        minLines: widget.minLines,
                        maxLines: widget.maxLines,
                        enableSuggestions: false,
    
                        decoration: InputDecoration(
                          // contentPadding: widget.padding ?? const EdgeInsets.all(14),
                          hintText: widget.placeholder,
                          hintStyle: AppStyles.textBodyDefault.copyWith(
                            color: RentMeColors.textAccent,
                          ),
                          helperText: widget.hint,
                          prefixIcon: widget.prepend,
                          suffixIcon: (widget.controller != null && !widget.controller.lastCheckedStatus && !Utils.isEmptyOrNull(widget.controller.errorText))
                            ? Icon(
                                Icons.info,
                                size: 20,
                                color: RentMeColors.red,
                              )
                            : widget.append,
    
                          filled: true,
                          fillColor: widget.isEnabled == true ? RentMeColors.white : RentMeColors.windowBackground,
                          focusColor: RentMeColors.white,
                          border: _defaultBorder(),
                          enabledBorder: _defaultBorder(),
                          focusedBorder: _defaultBorder(),
                        ),
                        cursorColor: RentMeColors.colorPrimary,
                      ):Container(),
                    ),
                  ],
                ),
              ),
            ),
            widget.controller != null && !widget.controller.lastCheckedStatus && !Utils.isEmptyOrNull(widget.controller?.errorText) ?
            Container(
              margin: EdgeInsets.only(top: 5),
              child: Text(
                widget.controller != null && !widget.controller.lastCheckedStatus && !Utils.isEmptyOrNull(widget.controller?.errorText)
                          ? widget.controller?.errorText
                          : null,
                          style: AppStyles.textBodyDefault.copyWith(color: RentMeColors.red),
              ),
            ) : Container()
          ],
        ),
      ),
    );
  }

  OutlineInputBorder _defaultBorder() {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(3),
      borderSide: BorderSide(
        color: widget.borderColor,
        width: Dimens.dividerLine,
      ),
    );
  }
}

// class AppPhoneField extends StatelessWidget with Dialogs {
//   final String title;
//   final PhoneTextController controller;
//   final List<Validator> validations;
//   final FocusNode focusNode;
//   // final List<Country> countries;
//   final Function(String) onSubmitted;

//   const AppPhoneField({Key key, @required this.controller, this.validations, this.onSubmitted, this.title, this.focusNode}) : super(key: key);
  
//   @override
//   Widget build(BuildContext context) {
//     UserSettings _userSettings = Provider.of(context);

//     if(controller != null) {
//       // set validations on controller
//       controller.countryList = _userSettings.getCountries().values.toList();
//     }

//     return AppTextField(
//       title: title,
//       controller: controller,
//       validations: validations,
//       keyboardType: TextInputType.numberWithOptions(decimal: false),
//       onSubmitted: onSubmitted,
//       focusNode: focusNode,
//       padding: const EdgeInsets.only(
//         top: 14,
//         bottom: 14,
//         left: 5,
//         right: 14,
//       ),
//       prepend: InkWell(
//         onTap: () {
//           showWidgetDialog(
//             context,
//             AppMenuDropdown<Country>(
//               title: AppLocalizations.of(context).selectCountry,
//               items: Map()..addEntries(controller.countryList.map((e) => MapEntry(e.name, e))),
//               text: (key, value) => '$key (+${value.zip})',
//               prepend: (value) {
//                 return Container(
//                   margin: const EdgeInsets.only(right: Dimens.marginSmall),
//                   width: 25,
//                   height: 25,
//                   child: WebsafeSvg.asset(
//                     AppImages.country(value.code),
//                     width: 25,
//                     height: 25,
//                   ),
//                 );
//               },
//               onSelected: (value, text) {
//                 controller.country = value;
//               },
//             ),
//           );
//         },
//         child: ValueListenableBuilder(
//           valueListenable: controller,
//           builder: (context, __, ___) {
//             return Row(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 Container(
//                   margin: const EdgeInsets.only(left: 14, right: Dimens.marginTiny),
//                   width: 21,
//                   height: 21,
//                   color: AppColors.windowBackground,
//                   child: controller.getSelectedCountry(_userSettings) != null
//                     ? WebsafeSvg.asset(
//                         AppImages.country(controller.getSelectedCountry(_userSettings).code.toLowerCase()),
//                         width: 25,
//                         height: 25,
//                       )
//                     : Container(),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.only(right: 4),
//                   child: AppText(
//                     '+' + (controller.getSelectedCountry(_userSettings)?.zip ?? '0'),
//                     style: AppStyles.textBody1.copyWith(
//                       color: AppColors.textAccent
//                     ),
//                   ),
//                 ),
//               ],
//             );
//           }
//         ),
//       ),
//     );
//   }
// }