import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/icons.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class RentMeAppBar {
  static AppBar create({
    Color backgroundColor,
    String title,
    Color titleColor,
    bool elevation,
    Color backIconColor,
    Function onBackPressed,
    Widget leading,
    Widget trailing,
    bool isDashes = false,
    Color firstDashColor,
    Color secondDashColor,
    bool isClose = false,
    Brightness statusBarColor = Brightness.light,
  }) {
    
    // FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);

    return AppBar(
        titleSpacing: 5,
        brightness: statusBarColor,
        centerTitle: true,
        backgroundColor: backgroundColor != null ? backgroundColor : Colors.white,
        elevation: elevation == true ? 4 : 0,
        title: !isDashes ? Text(
          !Utils.isEmptyOrNull(title) ? title : '',
          // style: TextStyle(
          //   fontSize: 10,
          //   fontWeight: FontWeight.bold,
          //   color: titleColor ?? Colors.black45,
          // )
          style: AppStyles.textBodyDefault.copyWith(),
        ):
        Container(
          height: 2,
          width: 200,
          child: Row(
            children: <Widget>[
              Container(
                height: 2,
                width: 100,
                color: firstDashColor != null ? firstDashColor: Colors.grey,
              ),
              Container(
                height: 2,
                width: 100,
                color: secondDashColor != null ? secondDashColor: Colors.grey,
              )
            ],
          ),
        ),

        leading: (leading == null)
            ? (onBackPressed != null)
                ? Container(
                  margin: EdgeInsets.only(left:10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle
                  ),
                  child: IconButton(
                      iconSize: 24,
                      // icon:  SvgPicture.asset(
                      //   SvgImage.arrl,
                      // ),
                      icon: Icon(Icons.arrow_back,),
                      color: backIconColor != null
                          ? backIconColor
                          : Colors.black,
                      onPressed: onBackPressed,
                    ),
                )
                : Container()
            : leading,
        actions: <Widget>[
          trailing != null
              ? Container(
                  padding: EdgeInsets.only(right: 16),
                  child: trailing,
                )
              : Container(),
        ],
      );
  }
}
