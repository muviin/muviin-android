import 'dart:async';
import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:google_place/google_place.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class PosterLocation extends StatefulWidget{
  
  final User user;
  final Rents rent;
  final bool fromDetail;

  const PosterLocation({Key key, this.user, this.rent, this.fromDetail = true}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return PosterLocationState();
  }

}


class PosterLocationState extends State<PosterLocation>{

static const kGoogleApiKey = "AIzaSyDWg6uFUfbxGf0zEKtUorrkJRAo6I00Kvg";
Set<Marker> markers;
 Completer<GoogleMapController> _controller = Completer();
 bool isSelected = false;
 DetailsResult selectedAddress;


  GooglePlace googlePlace;

  CameraPosition _kGooglePlex;

  displayMarker(double lat,  long, String country) async {
    final http.Response response = await http.get(Constants.SPACE_URL+"users/"+widget.rent.posterImage);
    // download the image and cache
    final File markerImageFile = await DefaultCacheManager().getSingleFile(Constants.SPACE_URL+"users/"+widget.rent.posterImage);

    final Uint8List markerImageByte = await markerImageFile.readAsBytes();

    //resizing images
    final int targetWidth = 120;
    final int targetHeight = 120;
    final Codec markerImageCodec = await instantiateImageCodec(markerImageByte, targetWidth: targetWidth, targetHeight: targetHeight);

    final FrameInfo frameInfo = await markerImageCodec.getNextFrame();

    final ByteData byteData = await frameInfo.image.toByteData(format: ImageByteFormat.png);

    final Uint8List resizeMarkerImageBytes = byteData.buffer.asUint8List();

    Marker mm  =  Marker(
      markerId: MarkerId(country),
      position: LatLng(lat, long),
      infoWindow: InfoWindow(title: !widget.fromDetail ? widget.rent.posterName: widget.rent.type),
      icon: !widget.fromDetail ?BitmapDescriptor.fromBytes(resizeMarkerImageBytes) : BitmapDescriptor.defaultMarkerWithHue(10)//BitmapDescriptor.defaultMarkerWithHue(10),
    );

    setState(() {
      markers.add(mm);
    });
  }





  @override
  void initState() {
    googlePlace = GooglePlace(kGoogleApiKey);

    _kGooglePlex = CameraPosition(
      target: LatLng(double.parse(widget.rent.lat), double.parse(widget.rent.long)),
      zoom: 15.4746,
    );

    markers = Set.from([]);

    // TODO: get posters information
    if(widget.fromDetail){
      _showOnMap();
    }else{
      autoCompleteSearch(widget.rent.country);
    }


    super.initState();
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: _kGooglePlex,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              markers: markers,
            ),

            isSelected ? Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Card(
                  child: FlatButton(
                    color: Colors.white,
                    child: RichText(
                      text: TextSpan(
                        text: "Use Location: ",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18
                        ),
                        children: [
                          TextSpan(
                            text: selectedAddress.formattedAddress,
                            style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                              fontSize: 20
                            ),
                          ),
                        ]
                      ),
                    ), //Text("Seelct Location "+selectedAddress.formattedAddress),
                    onPressed: (){} //_goToTheLake,
                  ),
                ),
              ),
            ):Container()
          ],
        ),
      ),
    );
  }


   void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    if (result != null && result.predictions != null && mounted) {
      // setState(() {
      //   predictions = result.predictions;
      // });
      getDetils(result.predictions.first.placeId);
    }    
  }

  // List<AutocompletePrediction> predictions = [];
  // DetailsResult res;

    void getDetils([String placeId]) async {
      var result = await this.googlePlace.details.get(placeId);
      if (result != null && result.result != null && mounted) {
        print(result.result);
        // res = result.result;
      }
      _animateRoute(result.result);
  }

    _animateRoute(DetailsResult result) async{
    CameraPosition cp = CameraPosition(
      target: LatLng(result.geometry.location.lat, result.geometry.location.lng),
      tilt: 59.440717697143555,
      zoom: 12.151926040649414,
        bearing: 192.8334901395799,
      );
      final GoogleMapController controller = await _controller.future;
      // displayMarker(lat, long);
      controller.animateCamera(CameraUpdate.newCameraPosition(cp));

       displayMarker(result.geometry.location.lat, result.geometry.location.lng, result.name);
  }


_showOnMap()async{
    CameraPosition cp = CameraPosition(
      target: LatLng(double.parse(widget.rent.lat), double.parse(widget.rent.long)),
      tilt: 59.440717697143555,
      zoom: 12.151926040649414,
        bearing: 192.8334901395799,
      );
      final GoogleMapController controller = await _controller.future;
      // displayMarker(lat, long);
      controller.animateCamera(CameraUpdate.newCameraPosition(cp));

       displayMarker(double.parse(widget.rent.lat), double.parse(widget.rent.long), widget.rent.country);
}















}





