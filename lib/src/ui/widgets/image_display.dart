import 'package:flutter/material.dart';

class RentImageDisplay extends StatelessWidget{
  final String imageURL;

  const RentImageDisplay({Key key, this.imageURL}) : super(key: key);
  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.black,
          child: Center(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Image(
                image: NetworkImage(imageURL),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      );
  }

}