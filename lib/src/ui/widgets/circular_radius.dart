import 'package:flutter/material.dart';

class CircularRadius extends StatelessWidget {

  final AssetImage view;
  double height;
  double width;

  CircularRadius({AssetImage view, double height = 50, double width = 50})
      : view = view,
        height = height,
        width = width,
        super(key: ObjectKey([view, height, width]));

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(image: view),
        
      ),
    );
  }
}
