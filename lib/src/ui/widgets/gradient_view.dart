import 'package:flutter/material.dart';

class SetGradient extends StatelessWidget{
  final bool topPosition;
  final double height;

  const SetGradient({Key key, this.topPosition, this.height = 100}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 300,//double.infinity, //height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,

          colors: [
            Colors.black,
            Colors.black,
            Colors.black,
            Colors.black.withOpacity(topPosition ? 0.0 : 0.0)
          ],
          stops: [
            0.0,
            0.05,
            0.1,
            6.1
          ]

        ),
      ),
    );
  }

}