

import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';

class UserAvatar extends StatelessWidget {
  final double width;
  final double height;
  final Color color;
  final Widget picture;
  final bool isLoading;
  final bool isUploadable;
  final VoidCallback onUploadTap;

  const UserAvatar(
      {Key key,
      this.onUploadTap,
      this.picture,
      this.color,
      this.width,
      this.height,
      this.isLoading = false,
      this.isUploadable = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: !isUploadable
        ? Container(
            width: width,
            height: height,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
            ),
              child: ClipRRect(
                borderRadius: picture != null? BorderRadius.circular(15.0) : BorderRadius.circular(5.0),
                child: picture != null
                  ? picture
                  : Icon(
                      Icons.supervised_user_circle, 
                      color: color,
                    ),
              ),
            // ),
          )
        : Stack(
            children: <Widget>[
              Container(
                width: width,
                height: height,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.grey,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50.0),
                  child: picture != null
                    ? picture
                    : Icon(
                        Icons.supervised_user_circle, 
                        color: color,
                      ),
                ),
              ),
              Container(
                width: width,
                height: height,
                alignment: Alignment.center,
                child: isLoading
                    ? RentmeProgressIndicator()
                    : Container(),
              ),
              isUploadable
                  ? Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        height: 32,
                        width: 32,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.grey,
                          border: Border.all(width: 2.0, color: Colors.white)
                        ),
                        // child: 
                      ),
                    )
                  : Container(),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      width: 32.0,
                      height: 32.0,
                      child: IconButton(
                          icon: Icon(Icons.camera_alt),
                          color: Colors.pink,
                          iconSize: 16.0,
                          onPressed: onUploadTap,
                        ),
                      ),
            ],
          ),
        );
  }
}
