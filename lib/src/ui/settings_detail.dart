import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/home_controller.dart';
import 'package:rentme_ghana/src/ui/setup_page.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';
import 'package:rentme_ghana/src/ui/widgets/text_field.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/utils_verifications.dart';
import 'package:rentme_ghana/src/utils/validators.dart';

class SettingsDetail extends StatefulWidget {
  final SettingsType type;

  const SettingsDetail({Key key, this.type}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return SettingsDetailState();
  }
}

class SettingsDetailState extends State<SettingsDetail> {

  TextController nameCTRL = TextController();
  TextController phoneCTRL = TextController();
  TextController emailCTRL = TextController();
  TextController pwdCtrl = TextController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  UserSettings _userSettings;


  SetupBloc _setupBloc;
  bool isLoading = false;

  updateField({SettingsType type }) async{

    var nameValid = await nameCTRL.validate();
    var phoneValid = await phoneCTRL.validate();
    var emailValid = await emailCTRL.validate();
    var pwdValid = await pwdCtrl.validate();

    setState(() async {
      if(type == SettingsType.NAME_CHANGE && nameValid){
        isLoading = true;
        Map<String, dynamic> toJson = Map();
        toJson = {
          "id": _userSettings.getUser().id,
          "type": "3",
          "name": nameCTRL.text
        };
        _setupBloc.updateProfile(toJson);
      }
      else if(type == SettingsType.MOBILE_CHANGE  && phoneValid){
        if(await VerificationUtils.parsePhoneNumber(phoneCTRL.text, phoneCTRL.getSelectedCountry(_userSettings).countryCode) == null) {
          showMessage(context, "Invalid Phone");
          isLoading = true;
          return;
        }
        // _setupBloc.updatePhone(phoneCTRL.text);
      }
      else if(type == SettingsType.EMAIL_CHANGE  && emailValid){
        isLoading = true;
        // _setupBloc.updateEmail(emailCTRL.text);
      }
      else if(type == SettingsType.PASSWORD_CHANGE  && pwdValid){
        isLoading = true;
        Map<String, dynamic> toJson = Map();
        toJson = {
          "id": _userSettings.getUser().id,
          "type": "2",
          "password": pwdCtrl.text
        };
        _setupBloc.updateProfile(toJson);
      }
    });
  }

  restartApp(){
    Navigator.push(context, MaterialPageRoute(
      builder: (BuildContext ctx){
        return MultiProvider(
          providers: [
            Provider<SetupBloc>(
              builder: (_) => SetupBloc(),
            ),
          ],
          child: SetupPage()
        );
      }
    ));
  }


  listeners(){
    if(_setupBloc == null ){
      _setupBloc = SetupBloc();

      _setupBloc.profile.listen((response) { 
        setState(() {
          isLoading = false;
        });

        restartApp();

      });

    }
  }


  @override
  void initState() {


    listeners();
    super.initState();


  }
  

  @override
  Widget build(BuildContext context) {
     _userSettings = Provider.of<UserSettings>(context);
    return Scaffold(
      appBar: RentMeAppBar.create(
        onBackPressed: (){
          Navigator.pop(context);
        }
      ),
      body: Form(
        key: _formKey,
        child: Container(
          color: RentMeColors.white, //Colors.black45.withOpacity(0.1),
          child: Stack(
            children: [
              Column(
                children: [

                  widget.type == SettingsType.NAME_CHANGE ? fullname():
                  widget.type == SettingsType.EMAIL_CHANGE ? emailWIdget():
                  widget.type == SettingsType.MOBILE_CHANGE ? phoneWIdget():
                  passwordWIdget(),

                  


                  Container(
                     padding: EdgeInsets.all(20),
                    child: widget.type == SettingsType.NAME_CHANGE ? getNameInput(context):
                    widget.type == SettingsType.MOBILE_CHANGE ? getPhoneInput(context):
                    widget.type == SettingsType.EMAIL_CHANGE ? getEmailInput(context):
                    getPasswordInput(context)
                  )


                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: RentmeButton(
                    text: "Continue",
                    size: ButtonSize.FULL,
                    isLoading: isLoading,
                    onPressed: (){
                      updateField(type: widget.type);
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


  Widget fullname(){
    return Container(
      color: RentMeColors.white,
      padding: EdgeInsets.all(20),
      child: Text(
        "To set a new name, please enter your new to be used in the field below",
        style: AppStyles.textBody2.copyWith(height: 1.5),
        textAlign: TextAlign.center,
      )
    );
  }

  Widget emailWIdget(){
    return Container(
      color: RentMeColors.white,
      padding: EdgeInsets.all(20),
      child: Text(
        "To set a new EMAIL, please enter your new  EMAIL to be used in the field below",
        style: AppStyles.textBody2.copyWith(height: 1.5),
        textAlign: TextAlign.center,
      )
    );
  }

  Widget phoneWIdget(){
    return Container(
      color: RentMeColors.white,
      padding: EdgeInsets.all(20),
      child: Text(
        "To set a new phone number, please enter the new phone to be used in the field below",
        style: AppStyles.textBody2.copyWith(height: 1.5),
        textAlign: TextAlign.center,
      )
    );
  }
  Widget passwordWIdget(){
    return Container(
      color: RentMeColors.white,
      padding: EdgeInsets.all(20),
      child: Text(
        "To set a new password, please enter the new password to be used in the field below",
        style: AppStyles.textBody2.copyWith(height: 1.5),
        textAlign: TextAlign.center,
      )
    );
  }


  Widget getNameInput(BuildContext context) {
    return AppTextField(
      controller: nameCTRL,
      borderColor: RentMeColors.transparent,
      showCountryEntry: false,
      placeholder: "Enter full name",
      validations: [
        RequiredValueValidation(context),
      ],
    );
  }

    Widget getPhoneInput(BuildContext context) {
    return AppTextField(
      controller: phoneCTRL,
      borderColor: RentMeColors.transparent,
      showCountryEntry: true,
      placeholder: "Enter Phone No.",
      keyboardType: TextInputType.phone,
      validations: [
        RequiredValueValidation(context),
        // PhoneNumberValidation(context)
      ],
    );
  }

    Widget getEmailInput(BuildContext context) {
    return AppTextField(
      controller: emailCTRL,
      borderColor: RentMeColors.transparent,
      showCountryEntry: false,
      placeholder: "Enter Email",
      validations: [
        RequiredValueValidation(context),
        EmailValidation(context)
      ],
    );
  }

    Widget getPasswordInput(BuildContext context) {
    return AppTextField(
      controller: pwdCtrl,
      borderColor: RentMeColors.transparent,
      showCountryEntry: false,
      placeholder: "Enter Password",
      isPassword: true,
      validations: [
        RequiredValueValidation(context),
      ],
    );
  }






}
