import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/verification_block.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/icons.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/about_us_page.dart';
import 'package:rentme_ghana/src/ui/notification_settings_page.dart';
import 'package:rentme_ghana/src/ui/profile_detail_page.dart';
import 'package:rentme_ghana/src/ui/settings.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/ui/widgets/user_avatar.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsMainPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _SettingsMainPageState();
  }

}


class _SettingsMainPageState extends State<SettingsMainPage>{

  UserSettings _userSettings;


  changeprofile(){
    Navigator.push(context, MaterialPageRoute(
      builder: (context) => Settings()
    ));
  }
  changeNotificationSettings(){
    Navigator.push(context, MaterialPageRoute(
      builder: (context) => NotificationSettingsPage(user: _userSettings.getUser(),)
    ));
  }
  toAboutUs(){
    Navigator.push(context, MaterialPageRoute(
      builder: (context) => AboutusPage()
    ));
  }
  contactUs(){
    _launchURL('mailto:' + "muviin620@gmail.com");
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void logout() {
    showDoubleOptionDialog(context, "Do you want to logout ?",
    (option) async {
      if(option){
        _userSettings.logout(context);
        // SetupBloc setup = Provider.of<SetupBloc>(context);
        // setup.logout.listen((response) {
        //   print(response);
        //   Navigator.push(context, ScaleRoute(widget: Login()));
        // });
        // setup.logouActiont();
        }
      }
    );
  }



  
  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    VerificationBloc _verificationBloc = VerificationBloc();
    return FutureBuilder(
      future: _verificationBloc.appVersion(),
      builder: (context, AsyncSnapshot<String> version){
        return Container(
          child: Padding(
            padding: const EdgeInsets.only(left:28.0, right: 28.0),
            child:  NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                overscroll.disallowGlow();
              },
              child: ListView(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: CachedNetworkImage(
                                imageUrl: !Utils.isEmptyOrNull(_userSettings.getUser().profile) ? "${Constants.SPACE_URL}users/${_userSettings.getUser().profile}": "http://via.placeholder.com/350x150",
                                placeholder: (context, url) => RentmeProgressIndicator(),
                                errorWidget: (context, url, error) => Icon(Icons.error),
                                fit: BoxFit.cover,
                                height: 100.0, 
                                width: 100.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(24.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(_userSettings.getUser().name, style: AppStyles.textTitleDefualt,),
                                SizedBox(height: 5,),
                                GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(
                                      builder: (context) => ProfileDetailPage()
                                    ));
                                  },
                                  child: Text("Show Profile", style: AppStyles.textBody2.copyWith(color:RentMeColors.bpPurpleVeryLight),)
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height:Dimens.marginRegular),
                      Divider(),
                      SizedBox(height:Dimens.marginRegular),
                      Text("Account Settings", style: AppStyles.textBodyDefault.copyWith(color:Colors.black54),),
                      _buildRow(title: "Personal Information", icon: AppIcon.user, onpressed: changeprofile),
                      _buildRow(title: "Notifications", icon: AppIcon.alarm, onpressed: changeNotificationSettings),
                      SizedBox(height:Dimens.marginRegular),
                      Text("Support", style: AppStyles.textBodyDefault.copyWith(color:Colors.black54),),
                      _buildRow(title: "Find out more about Muviin", icon: AppIcon.magnifier, onpressed: toAboutUs),
                      _buildRow(title: "Contact us", icon: AppIcon.bubble, onpressed: contactUs),
                      SizedBox(height:Dimens.marginRegular),
                      Text("Legal", style: AppStyles.textBodyDefault.copyWith(color:Colors.black54),),
                      _buildRow(title: "Terms Of Service", icon: AppIcon.license),
                      _buildRow(title: "Logout", icon: AppIcon.exit, onpressed: logout),
                      Container(
                        padding: EdgeInsets.all(Dimens.marginBig),
                        child: Center(
                          child: Text(
                            (!Utils.isEmptyOrNull(version.data)) ? version.data : '',
                              style: TextStyle(
                                fontSize: Dimens.textSmall,
                                color: RentMeColors.textPrimaryLight,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                        ),
                      ),
                      




                      
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }


  Widget _buildRow({String title, AppIcon icon, Function onpressed}){
    return InkWell(
      onTap: onpressed,
      child: Container(
        margin: EdgeInsets.only(top:16),
        padding: EdgeInsets.only(bottom:16),
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: RentMeColors.colorBorderGrey, width: 0.4,))
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
             Text(title, style: AppStyles.textBody3.copyWith(fontSize: 20, color: Colors.black87),),
             icon.drawSvg()
          ],
        ),
      ),
    );
  }





}