import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';


class ChooseCountry extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return ChooseCountryState();
  }

}


class ChooseCountryState extends State<ChooseCountry>{

  Country _selectedCountry;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CountryPicker(
            showFlag: true,
            showName: true,
            onChanged: (country){
              this._selectedCountry = country;
            },
            selectedCountry: _selectedCountry,
          ),
      ),
    );
  }


}