import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/strings.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/confirm_otp_page.dart';
import 'package:rentme_ghana/src/ui/widgets/poster_location.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';


class ProfileDetailPage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProfileDetailPageState();
  }

}

class ProfileDetailPageState extends State<ProfileDetailPage>{

  UserSettings _userSettings;
  SetupBloc _setupBloc = SetupBloc();


  verifyEmail(){
    EasyLoading.show(status: 'processing...');
    Map<String, dynamic> toJson = Map();
    toJson = {
      "id": _userSettings.getUser().id,
      "type": "6",
      "email": _userSettings.getUser().email
    };
    _setupBloc.updateProfile(toJson);
  }


  listeners(){
    _setupBloc.profile.listen((response) { 

      Navigator.push(
      context, 
      MaterialPageRoute(
        builder: (BuildContext ctx){
          return ConfirmOtpPage(
            email: _userSettings.getUser().email,
          );
        }
      )
    );
      // EasyLoading.showSuccess('Great Success!');
      EasyLoading.dismiss();


    });
  }

  @override
  void initState() {
    super.initState();
    listeners();
  }


  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 0),
        height: MediaQuery.of(context).size.height,
        child:  NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
          },
          child: ListView(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 6,
                    child: Container(
                      margin: EdgeInsets.only(left: 20, top: 50),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Hi "+_userSettings.getUser().name, style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),),
                          Container(height: 10,),
                          Text("joined at "+_userSettings.getUser().createdAt),
                          Container(height: 10,),
                          Text("bio ", style: AppStyles.textBody2,),
                          Container(height: 10,),
                          Text(_userSettings.getUser().bio??"", style: AppStyles.textBody2,),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Hero(
                      tag: "test",
                      child: Container(
                      margin: EdgeInsets.only(right: 30, top: 50),
                      width: 100,
                      height: 100,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: CachedNetworkImage(
                          imageUrl: Constants.SPACE_URL+"users/"+_userSettings.getUser().profile,
                          placeholder: (context, url) => RentmeProgressIndicator(),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                          fit: BoxFit.cover,
                        ),
                      )
                    ),
                    )
                  )
                ],
              ),
              SizedBox(height: Dimens.marginSuperLarge,),
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                  child: Image(
                    image: AssetImage("assets/images/verify.jpg"),
                    height: 60,
                  ),
                ),
              ),

              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Verification", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),), 
                    SizedBox(height:20),
                    InkWell(
                      onTap: (){
                        showMessage(context, "Coming Soon");
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: RentMeColors.textPrimaryLight
                          )
                        ),
                      child: Text("Get Verification Badge", style: AppStyles.textBodyDefault,),
                      ),
                    ),
                    SizedBox(height:20),
                    Text("Email Verification", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),), 
                    SizedBox(height:20),
                    _userSettings.getUser().emailVerified != "1"? InkWell(
                      onTap: (){
                        // showMessage(context, "Coming Soon");
                        verifyEmail();
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: RentMeColors.textPrimaryLight
                          )
                        ),
                      child: Text("Verify email", style: AppStyles.textBodyDefault,),
                      ),
                    ):Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.green //RentMeColors.textPrimaryLight
                          )
                        ),
                      child: Text("Verified", style: AppStyles.textBodyDefault.copyWith(fontStyle: FontStyle.italic, color: Colors.green),),
                      ),



                  ],
                ),
              ),

            ],
          ),
        )
      ),
    );
  }
}