import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/main.dart';
import 'package:rentme_ghana/src/model/notifications.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/chat_user.dart';
import 'package:rentme_ghana/src/ui/home_list.dart';
import 'package:rentme_ghana/src/ui/profile.dart';
import 'package:rentme_ghana/src/ui/search.dart';
import 'package:rentme_ghana/src/ui/settings_main.dart';
import 'package:rentme_ghana/src/ui/user_profile.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/ui/widgets/user_avatar.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class HomeController extends StatefulWidget{
  final User user;
  final String location;
  final Notifications notifications;

  const HomeController({Key key, this.user, this.location, this.notifications}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return HomeControllerState();
  }
}


class HomeControllerState extends State<HomeController>{

  PageController pageController;
  int numberOfPages = 5;
  int pageNumber = 0;
  UserSettings _userSettings;
  double errorNotificationWidth;
  bool showErr = false;
  bool hideErr = false;




  void onPageChanged(int page) {
    setState(() {
      pageNumber = page;
    });
  }

  Future<Timer> _delay() async {
    return new Timer(Duration(seconds: 2), _proceed);
  }

  _proceed() async {
    setState(() {
      hideErr = true;
    });
  }


  @override
  void initState() {

    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      // Got a new connectivity status!
      setState(() {
        if (result == ConnectivityResult.none) {
        // no internet
          showErr = true;
          hideErr = false;
        }else{
          showErr = false;
          _delay();
        }
      });

    });
  _delay();


    super.initState();
    pageController = PageController();
  }

    Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          child: AlertDialog(
            title: Text('Are you sure?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('No'),
              ),
              FlatButton(
                onPressed: (){
                  exit(0);
                },
                child: Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }



  @override
  Widget build(BuildContext context) {
    errorNotificationWidth = MediaQuery.of(context).size.width;
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
      appBar: RentMeAppBar.create(
        // title: pageNumber == 0? "Explore":"",
        trailing: InkWell(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => UserProfile()
            ));
          },
          child: UserAvatar(
            width: 40,
            height: 40,
            color:RentMeColors.black,
            picture: CachedNetworkImage(
                imageUrl: !Utils.isEmptyOrNull(_userSettings.getUser().profile) ? "${Constants.SPACE_URL}users/${_userSettings.getUser().profile}": "http://via.placeholder.com/350x150",
                placeholder: (context, url) => RentmeProgressIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.error),
                fit: BoxFit.cover,
                height: 30.0, width: 30.0,
            ), //Image.network("${Constants.POS_API_BASE_URL}/${_userSettings.getUser().profile}", fit: BoxFit.cover,height: 30.0, width: 30.0,)
          ),
        )
      ),
      body: Container(
        child: Stack(
          children: [
            Column(
              children: <Widget>[
                Expanded(
                  flex: 9,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: Colors.white,
                    child: PageView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: numberOfPages,
                      controller: pageController,
                      itemBuilder: (context, page){
                        if(page == 0){
                           messageShow = 0;
                          return HomeList(user: _userSettings.getUser(), location: widget.location,notifications: widget.notifications,);
                        }
                        else if(page == 1){
                          messageShow = 0;
                          return MapSearch(user: widget.user);
                        }
                        else if(page == 2){
                          messageShow = 0;
                          return Profile(user: _userSettings.getUser(),);
                        }
                        else if(page == 3){
                          messageShow = 1;
                          return Chat(id: "${_userSettings.getUser().id}");
                        }
                        else if(page == 4){
                          messageShow = 0;
                           return SettingsMainPage(); //Settings();
                        }
                      },  
                      onPageChanged: onPageChanged,                
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    color: Colors.black,
                    child: Row(
                      children: <Widget>[

                        Expanded(
                          flex: 2,
                          child: GestureDetector(
                            child: Container(
                            color: pageNumber == 0 ? Colors.green : Colors.black,
                            height: MediaQuery.of(context).size.height,
                            child: Icon(Icons.home, size: 30,color: Colors.white,),
                            ),
                            onTap: (){
                              pageController.jumpToPage(0);
                            },
                          )
                        ),

                        Expanded(
                          flex: 2,
                          child: GestureDetector(
                            child: Container(
                            color: pageNumber == 1 ? Colors.green : Colors.black,
                            height: MediaQuery.of(context).size.height,
                            child: Icon(Icons.search, size: 30,color: Colors.white,),
                            ),
                            onTap: (){
                              pageController.jumpToPage(1);
                            },
                          )
                        ),

                        Expanded(
                          flex: 2,
                          child: GestureDetector(
                            child: Container(
                            color: pageNumber == 2 ? Colors.green : Colors.black,
                            height: MediaQuery.of(context).size.height,
                            child: Icon(Icons.person, size: 30,color: Colors.white,),
                            ),
                            onTap: (){
                              pageController.jumpToPage(2);
                            },
                          )
                        ),

                        Expanded(
                          flex: 2,
                          child: GestureDetector(
                            child: Container(
                            color: pageNumber == 3 ? Colors.green : Colors.black,
                            height: MediaQuery.of(context).size.height,
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Icon(Icons.message, size: 30,color: Colors.white,),
                                messageCount > 0?Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    height: 20,
                                    width: 20,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle
                                    ),
                                    child: Text("$messageCount", style: AppStyles.textBody2.copyWith(color:Colors.red),)
                                  )
                                ):Container()
                              ],
                            ),
                          ),
                          onTap: (){
                            pageController.jumpToPage(3);
                            },
                          )
                        ),

                        Expanded(
                          flex: 2,
                          child: GestureDetector(
                            child: Container(
                            color: pageNumber == 4 ? Colors.green : Colors.black,
                            height: MediaQuery.of(context).size.height,
                            child: Icon(Icons.settings, size: 30,color: Colors.white,),
                          ),
                          onTap: (){
                            pageController.jumpToPage(4);
                          }
                          )
                        ),


                      ],
                    ),
                    ),
                )
              ],
            ),
            ///
            AnimatedContainer(
              duration: Duration(milliseconds: 300),
              width: errorNotificationWidth,
              height: !hideErr?30:0,
              color: showErr?Colors.red:Colors.green,
              padding: EdgeInsets.only(left:15, right: 15, top: 5, bottom: 5),
              child: Text(showErr?"Lost internet connection":"Connected", style: AppStyles.textBody2.copyWith(color:Colors.white),)
            )
          ],
        )
      ),
    ),
    );
  }
}