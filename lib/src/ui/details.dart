import 'package:avatar_glow/avatar_glow.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/message_bloc.dart';
import 'package:rentme_ghana/src/blocs/rent_bloc.dart';
import 'package:rentme_ghana/src/model/message.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/repository/rent_repository.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/chat_detail.dart';
import 'package:rentme_ghana/src/ui/poster.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';
import 'package:rentme_ghana/src/ui/widgets/image_display.dart';
import 'package:rentme_ghana/src/ui/widgets/poster_location.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:url_launcher/url_launcher.dart';


class Details extends StatefulWidget{
  final Rents rent;
  final User user;


  const Details({Key key, this.rent, this.user}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return DetailsState();
  }

}


class DetailsState extends State<Details>{

  bool showRentImage = false;
  List<Rents> similarRents = List();
  RentBloc _rentBloc;
  Rents selectedRents;
  ScrollController ctrl = ScrollController();
  UserSettings _userSettings;
  bool colorAppBar = false;



  scrollListener(){
    setState(() {
      if(ctrl.position.pixels > MediaQuery.of(context).size.height * 0.4){
          colorAppBar = true;
      }
      if(ctrl.position.pixels < MediaQuery.of(context).size.height * 0.4){
        colorAppBar = false;
      } 
    });
  }



  fetchSimilarRents(){
    _rentBloc.fetchSimilarRents(selectedRents.location);
  }

  listeners(){
  if(_rentBloc == null){
    _rentBloc = RentBloc();

    // init listing of rents
    _rentBloc.similarRents.listen((rents){
      _userSettings.setSimilarRents(rents);
      setState(() {
        // _isLoading = false;
        similarRents = rents;
        similarRents = similarRents.reversed.toList();
      });
    });

    



  }
}



  @override
  void initState() {
    super.initState();
    ctrl.addListener(scrollListener);
    selectedRents = widget.rent;
    listeners();
    fetchSimilarRents();
  }


  void displayUserProfile(context){
      Navigator.push(context, MaterialPageRoute(
        builder: (context) => Poster(rent: widget.rent, user: widget.user)
      ));
  }

  void displayRentImg(String imageurl){
      Navigator.push(context, MaterialPageRoute(
        builder: (context) => RentImageDisplay(imageURL: imageurl,)
      ));
  }


  _fetchLocalRents(){
  var localRents = _userSettings.getSimilarRents();
  if(!Utils.isEmptyOrNull(localRents)){
    List<Rents> list = List();
    for (var i = 0; i < localRents.length; i++) {
      list.add(Rents.fromJson(localRents[i]));
    }

    setState(() {
      similarRents = list.reversed.toList();
    });
  }
}






  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    _fetchLocalRents();
    return Scaffold(
      appBar: RentMeAppBar.create(
        onBackPressed: (){
          Navigator.pop(context);
        },
        backgroundColor: !colorAppBar ? RentMeColors.transparent: RentMeColors.white,
        // trailing: IconButton(
        //   icon: Icon(Icons.cloud), 
        //   onPressed: null
        // )
      ),
      extendBodyBehindAppBar: true,
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(0),
          controller: ctrl,
          children: <Widget>[
            Container(
              height: 300,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: widget.rent.thumbnailArray.length,
                      itemBuilder: (BuildContext ctx, i){
                        return GestureDetector(
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 300,
                            // decoration: BoxDecoration(
                            //   image: DecorationImage(
                            //     image: NetworkImage(Constants.RM_API_BASE_URL+"/"+selectedRents.thumbnailArray[i]), 
                            //     fit: BoxFit.cover
                            //   )
                            // ),
                            child: CachedNetworkImage(
                                imageUrl: Constants.SPACE_URL+"rents/"+selectedRents.thumbnailArray[i],
                                placeholder: (context, url) => RentmeProgressIndicator(),
                                errorWidget: (context, url, error) => Icon(Icons.error),
                                fit: BoxFit.cover,
                              ),
                          ),
                          onTap: (){
                            displayRentImg(Constants.SPACE_URL+"rents/"+selectedRents.thumbnailArray[i]);
                          },
                        );
                      },
                    ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding:EdgeInsets.only(right: 20),
                      child: Text(
                        "${selectedRents.thumbnailArray.length} more",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, right: 10, top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Text(
                            widget.rent.type,
                            style: TextStyle(
                              fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(selectedRents.rentType, textAlign: TextAlign.end, style: TextStyle(
                            color: Colors.redAccent
                          ),),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Text(
                            // "${selectedRents.price}" 
                            selectedRents.getCalculatedPrice(_userSettings)
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                            widget.rent.country, 
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black45
                            ),
                          ),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.map, color: Colors.redAccent,),
                        Container(width: 10,),
                        Expanded(
                          flex: 8,
                          child: Text(widget.rent.location),
                        ),
                        Expanded(
                          flex: 2,
                          child: Image(
                            image: AssetImage("assets/images/verify.jpg"),
                            height: 20,
                            alignment: Alignment.centerRight,
                          ),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.streetview, color: Colors.redAccent,),
                        Container(width: 10,),
                        Expanded(
                          flex: 8,
                          child: Text(
                            widget.rent.street?? "n/a"
                          ),
                        ),

                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.map, color: Colors.redAccent,),
                        Container(width: 10,),
                        Expanded(
                          flex: 8,
                          child: Text(
                            selectedRents.createdAt
                          ),
                        ),

                      ],
                    )
                  ),

                  Divider(),

                  Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text(
                      "About Owner",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black54
                      ),

                    ),
                  ),

                  AvatarGlow(
                    glowColor: Colors.blue,
                    endRadius: 60.0,
                    duration: Duration(milliseconds: 2000),
                    repeat: true,
                    showTwoGlows: true,
                    repeatPauseDuration: Duration(milliseconds: 100),
                    child: Material(
                      elevation: 8.0,
                      shape: CircleBorder(),
                      child: CircleAvatar(
                        backgroundColor: Colors.grey[100],
                        child: GestureDetector(
                          child: Hero(
                            tag: "test",
                            child: Container(
                              width: 60,
                              height: 60,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(30),
                                child: CachedNetworkImage(
                                    imageUrl: Constants.SPACE_URL+"users/"+selectedRents.posterImage,
                                    placeholder: (context, url) => RentmeProgressIndicator(),
                                    errorWidget: (context, url, error) => Icon(Icons.error),
                                    fit: BoxFit.cover,
                                ),
                              ),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                            ),
                          ),
                        onTap: () => displayUserProfile(context)
                        ),
                        radius: 40.0,
                      ),
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text(
                      "Notify Owner",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black54
                      ),

                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 0, top: 10),
                    child: Text(
                      "A notification is sent to the owner of the property notifying them that you like the property",
                      style: AppStyles.textBody2.copyWith(color:RentMeColors.bpPurpleVeryLight)

                    ),
                  ),
                  SizedBox(height:10),
                  IconButton(
                    icon: Icon(Icons.thumb_up), 
                    color: RentMeColors.red,
                    onPressed: _notify
                  ),

                  Divider(),

                  // Container(
                  //   margin: EdgeInsets.only(top: 10),
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.start,
                  //     children: <Widget>[
                        // GestureDetector(
                        //   child: Hero(
                        //     tag: "test",
                        //     child: Container(
                        //       width: 60,
                        //       height: 60,
                        //       child: ClipRRect(
                        //         borderRadius: BorderRadius.circular(30),
                        //         child: CachedNetworkImage(
                        //             imageUrl: Constants.SPACE_URL+"users/"+selectedRents.posterImage,
                        //             placeholder: (context, url) => RentmeProgressIndicator(),
                        //             errorWidget: (context, url, error) => Icon(Icons.error),
                        //             fit: BoxFit.cover,
                        //         ),
                        //       ),
                        //       decoration: BoxDecoration(
                        //         shape: BoxShape.circle,
                        //         // image: DecorationImage(
                        //         //     image: NetworkImage(Constants.RM_API_BASE_URL+"/"+selectedRents.posterImage),
                        //         //     fit: BoxFit.cover
                        //         // ),
                        //       ),
                        //     ),
                        //   ),
                        // onTap: () => displayUserProfile(context)
                        // )
                  //     ],
                  //   )
                  // ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.phone, color: Colors.redAccent,),
                        Container(width: 10,),
                        Expanded(
                          flex: 8,
                          child: Text(
                            selectedRents.posterPhone
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: IconButton(
                            icon: Icon(Icons.phone),
                            alignment: Alignment.centerRight,
                            onPressed: (){
                              launch("tel://${widget.rent.posterPhone}");
                            },
                          )
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.mail, color: Colors.redAccent,),
                        Container(width: 10,),
                        Expanded(
                          flex: 8,
                          child: Text(
                            selectedRents.posterEmail
                          ),
                        ),

                      ],
                    )
                  ),

                  Divider(),

                   Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Type Description", 
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black54
                          ),
                        ),

                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    width: MediaQuery.of(context).size.width*0.8,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Text(
                            selectedRents.description??"n/a"
                          ),
                        ),

                      ],
                    )
                  ),

                  Divider(),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Get Directions", 
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black54
                          ),
                        ),

                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    // child: Row(
                    //   crossAxisAlignment: CrossAxisAlignment.start,
                    //   children: <Widget>[
                        
                        // IconButton(
                        //   icon: Icon(Icons.directions), 
                        //   iconSize: 50,
                        //   onPressed: getDirection
                        // )

                    //   ],
                    // )
                    child: showMapView()
                  ),

                  Divider(),
                  

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Similar Rents", style: TextStyle(
                          fontWeight: FontWeight.bold
                        ),),

                      ],
                    )
                  ),
                  Container(
                    height: 200,
                    margin: EdgeInsets.only(top: 10),
                    child: NotificationListener<OverscrollIndicatorNotification>(
                      onNotification: (overscroll) {
                        overscroll.disallowGlow();
                      },
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: similarRents.length,
                        itemBuilder: (BuildContext ctx, index){
                          return InkWell(
                            onTap: (){
                              setState(() {
                                selectedRents = similarRents[index];
                              });
                              ctrl.animateTo(0, duration: Duration(milliseconds: 600), curve: Curves.fastOutSlowIn); 
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // Image(
                                  //   image: NetworkImage(Constants.RM_API_BASE_URL+"/"+similarRents[index].thumbnail),
                                  //   height: 100,
                                  //   width: 150,
                                  //   fit: BoxFit.cover,
                                  // ),
                                  Container(
                                    height: 100,
                                    width: 150,
                                    child: CachedNetworkImage(
                                        imageUrl: Constants.SPACE_URL+"rents/"+similarRents[index].thumbnail,
                                        placeholder: (context, url) => RentmeProgressIndicator(),
                                        errorWidget: (context, url, error) => Icon(Icons.error),
                                        fit: BoxFit.cover,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Padding(
                                    padding: const EdgeInsets.only(left:0.0),
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width * 0.4,
                                      child: Text(widget.rent.type, style: AppStyles.textBody3.copyWith(color:Colors.black54), overflow: TextOverflow.ellipsis,),
                                    )
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(similarRents[index].location, style: AppStyles.textBody3.copyWith(fontWeight: FontWeight.bold, fontSize: 12),),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left:0.0),
                                    child: Text(similarRents[index].country, style: AppStyles.textBody3.copyWith(fontWeight: FontWeight.bold, fontSize: 12),),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left:0.0),
                                    child: Text(similarRents[index].createdAt, style: AppStyles.textBody3.copyWith(fontWeight: FontWeight.bold, fontSize: 10),),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    )
                  ),

                ],
              ),
            )
          ],
        ),
      ),
        floatingActionButton: widget.rent.posterId != "${_userSettings.getUser().id}" ? FloatingActionButton(
          child: Icon(Icons.chat),
          backgroundColor: Colors.black,
          onPressed: (){
            // Navigator.of(context, rootNavigator: true).push(ScaleRoute(widget: Message()));
              Navigator.push(context, MaterialPageRoute(
                builder: (context){
                  return ChatDetail(rentData: widget.rent,id: "${_userSettings.getUser().id}",);
                }
              ));
          },
        ):Container()
    );
  }


  Widget showMapView(){
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          height:  MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: PosterLocation(user: widget.user,rent: widget.rent,),
          ),
        ),
        // SetGradient(height: 100, topPosition: false),
        Align(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal:Dimens.marginRegular),
            child: Row(
              children: [
                Expanded(
                  flex: 10,
                  child: RentmeButton(
                    text: "Take a look at the area",
                    size: ButtonSize.SMALL,
                    primaryColor: Colors.white,
                    textColor: Colors.black,
                    onPressed: _openStreetView
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.directions, color: Colors.red,), 
                  iconSize: 50,
                  onPressed: getDirection
                )


              ],
            ),
          ),
        )
      ],
    );
  }


  getDirection({String address = "accra"}) async {
    // final url = 'comgooglemaps://?daddr=${Uri.encodeFull(address)}&directionsmode=driving';
    //  await launch(url);
    final url = 'https://www.google.com/maps/search/?api=1&query=${widget.rent.lat},${widget.rent.long}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

MessageBloc _messageBloc = MessageBloc();
  _notify(){
    showDoubleOptionDialog(context, "Do you want to Notify owner ?",
    (option) async {
      if(option){
          // _rentBloc.notifyOwner(widget.rent.posterId);
          var currentDate = new DateTime.now();
        Message message = Message()
          ..message = "Hello, I am kindly interested in your property"
          ..messageStatus = "0"
          ..receiverId = widget.rent.posterId
          ..senderId = "${_userSettings.getUser().id}"
          ..date = DateFormat('d-MMM-yyyy hh:mm a').format(currentDate)
          ..senderName = _userSettings.getUser().name
          ..receiverName = widget.rent.posterName
          ..senderImage = _userSettings.getUser().profile
          ..receiverimage = widget.rent.posterImage
          ..productName = widget.rent.type
          ..messageFile = ""
          ..imageFile = null;
          _messageBloc.sendMessage(message);









        }
      }
    );
  }

  _openStreetView() async{
    /// [Native implememtation]
    // VerificationBloc vv = VerificationBloc();
    // vv.startStreetView(
    //   lat: double.parse(widget.rent.lat),
    //   long: double.parse(widget.rent.long)
    // );

    var key = "AIzaSyDWg6uFUfbxGf0zEKtUorrkJRAo6I00Kvg";
    String url = "https://maps.google.com/maps?q=&layer=c&cbll=${widget.rent.lat},${widget.rent.long}&cbp=11,direction,0,0,0";
    if (await canLaunch(url)) {
    await launch(url);
    } else {
    throw 'Could not launch $url';
    }
  }




}