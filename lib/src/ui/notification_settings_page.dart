import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class NotificationSettingsPage extends StatefulWidget {
  final User user;

  const NotificationSettingsPage({Key key, this.user}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return NotificationSettingsPageState();
  }
}

class NotificationSettingsPageState extends State<NotificationSettingsPage> {

  UserSettings _userSettings;
  SetupBloc _setupBloc = SetupBloc();
  bool checkValue = false;
  bool smsNewsLetter = false;
  bool emailNewsLetter = false;
  bool fcmNewsLetter = false;
  Map notificationObj = {};
  


  smscheck(){
    setState(() {
      smsNewsLetter = !smsNewsLetter;

      save();
    });
  }
  emailcheck(){
    setState(() {
      emailNewsLetter = !emailNewsLetter;
      save();
    });
  }
  fcmcheck(){
    setState(() {
      fcmNewsLetter = !fcmNewsLetter;
      save();
    });
  }



  save(){
    Utils.log("notificationObj");
    Map<String, dynamic> toJson = Map();

    notificationObj = {
      "sms_notif":smsNewsLetter?"1":"0",
      "email_notif":emailNewsLetter?"1":"0",
      "fcm_notif":fcmNewsLetter?"1":"0",
    };

    toJson = {
      "id": _userSettings.getUser().id,
      "type": "5",
      "notif": notificationObj
    };
    _setupBloc.updateProfile(toJson);
  }

  listeners(){
    _setupBloc.profile.listen((response) { 
      User user = _userSettings.getUser();
      user.smsNot = notificationObj['sms_notif'];
      user.fcmNot = notificationObj['fcm_notif'];
      user.emailNot = notificationObj['email_notif'];
      _userSettings.setUser(user);
    });
  }


  @override
  void initState() {
    super.initState();

    smsNewsLetter = widget.user.smsNot == "1"?true:false;
    emailNewsLetter = widget.user.emailNot == "1"?true:false;
    fcmNewsLetter = widget.user.fcmNot == "1"?true:false;


    listeners();

  }






  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    return Scaffold(
      appBar: RentMeAppBar.create(
        onBackPressed: (){
          Navigator.pop(context);
        },
      ),
      body: Container(
        color: Colors.white, //Colors.black45.withOpacity(0.1),
        child:  NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
          },
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Text("Notification Settings", style: AppStyles.textTitleDefualt.copyWith(fontSize:30),),
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: header(
                  title: "SMS",
                  description: "Receive SMS from Muviin about new property uploads, newsletters etc"
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                color: RentMeColors.white,
                child: Column(
                  children: [
                    body(left: "NewsLetters", right: _userSettings.getUser().name, active: smsNewsLetter, action: smscheck),
                    Divider(),
                    body(left: "Poperties", right: _userSettings.getUser().country, active: false, enable: false),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
                child: header(
                  title: "Emails",
                  description: "Receive Email from Muviin about new property uploads, newsletters etc"
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                color: RentMeColors.white,
                child: Column(
                  children: [
                    body(left: "NewsLetters", right: _userSettings.getUser().name, active: emailNewsLetter, action: emailcheck),
                    Divider(),
                    body(left: "Proprties", right: _userSettings.getUser().country, active: false, enable: false),
                  
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
                child: header(
                  title: "Push Notifications",
                  description: "Receive FCM from chat messages, new uploads etc"
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                color: RentMeColors.white,
                child: Column(
                  children: [
                    body(left: "Chats", right: _userSettings.getUser().name, enable: false),
                    Divider(),
                    body(left: "NewsLetters", right: _userSettings.getUser().country, active: fcmNewsLetter, action: fcmcheck),
                  
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }




Widget header({String title, description = ""}){
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Container(
        child: Text(title, style: AppStyles.textTitleDefualt.copyWith(color: RentMeColors.textPrimary,)),
      ),
      Container(
        child: Text(description, style: AppStyles.textBody3.copyWith(color: RentMeColors.textPrimaryLight,)),
      ),
    ],
  );
}

Widget body({String left, right, bool active = true, bool enable = true, Function action}){
  return Container(
    padding: EdgeInsets.symmetric(vertical: Dimens.marginRegular),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(left, style: AppStyles.textBody3.copyWith(fontWeight: FontWeight.bold, color: RentMeColors.textPrimaryLight),),
        // Checkbox(value: false, onChanged: null)
        // Switch(value: false, onChanged: null)
        CupertinoSwitch(
          value: !enable? true: active, 
          onChanged: (value){
            if(enable){
              action();

            }
          },
          trackColor: RentMeColors.red.withOpacity(0.8),
          activeColor: !enable?  RentMeColors.bpPurpleVeryLight.withOpacity(0.5): RentMeColors.bpPurpleVeryLight,
        )
      ],
    )
  );
}




}
