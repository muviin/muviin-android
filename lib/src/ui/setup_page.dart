import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/message_bloc.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/blocs/verification_block.dart';
import 'package:rentme_ghana/src/model/notifications.dart';
import 'package:rentme_ghana/src/repository/auth_repository.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/icons.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/get_started_page.dart';
import 'package:rentme_ghana/src/ui/home_controller.dart';
import 'package:rentme_ghana/src/ui/network_error_page.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart' as Geo;


class SetupPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return SetupPageState();
  }

}


class SetupPageState extends State<SetupPage>{

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final AuthRepository authRepository = AuthRepository();
  final SetupBloc setupBloc = SetupBloc();
  UserSettings _userSettings;
  String local = "";
  Notifications notifications;



  final Geo.Geolocator geolocator = Geo.Geolocator()..forceAndroidLocationManager;
  Future<String> _getCurrentLocation() async {

    // check fr accepted permissions
    VerificationBloc _vb = VerificationBloc();
    PermissionState state = await _vb.pushPermissionChecknEW();
    if(state == PermissionState.SUCCESS){
        Geo.Position pos = await geolocator.getCurrentPosition(desiredAccuracy: Geo.LocationAccuracy.best);
        String response = await _getAddressFromLatLng(pos);
        return response;
    }
    if(state == PermissionState.REQUEST){
      return "REQUEST";
    }
    if(state == PermissionState.ENABLE){

    }
    return "ENABLE";
  }


  Future<String> _getAddressFromLatLng(Geo.Position mm) async {
    try {
      List<Geo.Placemark> p = await geolocator.placemarkFromCoordinates(mm.latitude, mm.longitude);
      Geo.Placemark place = p[0];
      this.local = place.locality;
      return this.local;
    } catch (e) {
      print(e);
    }
    return "";
  }


  listeners(){

    setupBloc.countries.listen((countries)async {
        Utils.log(countries);
        _userSettings.setCountries(countries);
        setState(() {
          addition = addition + 50;
        });

        // fetch notifs
        MessageBloc _mblock = MessageBloc();
        _mblock.notifs.listen((notifs) {
          this.notifications = notifs;
          Utils.log(notifs.message);
          setState(() {
            addition = addition + 50;
          });
          // fetch user
          setupBloc.user.listen((user) { 
            _userSettings.setUser(user);
            // check if device is stored
            if (!_userSettings.isDeviceRegistered()) {
              Utils.log(">>> DEVICE NOT REGISTERED");
              VerificationBloc verificationBloc = VerificationBloc();
              verificationBloc.buildDevice(_userSettings).then((device) {

              setupBloc.device.listen((device){
                  _userSettings.setIsDeviceRegistered(true);
                  _userSettings.setDevice(device);
                  setState(() {
                    addition = addition +50;
                  });

                  _delay();
                }, onError: (error) {
                }
              );
      
                setupBloc.saveDevice(device);
              });
            } else {
              Utils.log(">>> DEVICE REGISTERED");
              setState(() {
                addition = addition + 50;
              });
              // delay 2 seconds before proceeding
              _delay();

            }

          });
          setupBloc.getUser("${_userSettings.getUser().id}");
         });
        _mblock.getNotification();

    });
    
  }

    Future<Timer> _delay() async {
    return new Timer(Duration(seconds: 1), _onDoneLoading);
  }

  _onDoneLoading() async {
    Navigator.push(
      context, 
      MaterialPageRoute(
        builder: (BuildContext ctx){
          return MultiProvider(
            providers: [
              Provider<SetupBloc>(
                builder: (_) => SetupBloc(),
              ),
            ],
            child: HomeController(location: this.local,notifications: this.notifications,),
          );
        }
      )
    );
  }

  noNetwrk(){
    Navigator.pushReplacement(
      context, 
            MaterialPageRoute(
        builder: (BuildContext ctx){
          return NetWorkErrorPage();
        }
      )
    );
  }

  @override
  void initState() {
    super.initState();


      SharedPreferences.getInstance().then((onValue) async {
      _userSettings = UserSettings(onValue);

      // check for updates
      VerificationBloc _verificationBloc = new VerificationBloc();
      bool networkStatus = await _verificationBloc.checkConnectivity();
      if(!networkStatus){
        // move to no netwrk screen
        noNetwrk();
        return;
      }

      if (_userSettings.getUser().isUserLoggedIn()) {
        Utils.log(">>> AUTH TOKEN GOOD");

        // get current location
        String location = await _getCurrentLocation();

        listeners();

        _verificationBloc.checkForUpdate(
          context,
          onSuccess: (status){
            // update required
            if(status){
              _verificationBloc.showVersionDialog(context, "https://play.google.com/store/apps/details?id=com.rentme.rentme_ghana");
            }
            else{
              // fetch countries
              setupBloc.fetchCountries();
            }
             
          },
          onFailure: (status){
            setupBloc.fetchCountries();
          }
        );
        
        
      } else {
        Utils.log(">>> AUTH TOKEN EXPIRED OR PIN NOT ENTERED");
          Navigator.push(
            context, 
            MaterialPageRoute(
              builder: (BuildContext ctx){
                return MultiProvider(
                  providers: [
                    Provider<SetupBloc>(
                      builder: (_) => SetupBloc(),
                    ),
                  ],
                  child: GetStartedPage(),
                );
              }
            )
          );

      }
    });

  }


double addition = 50;

  @override
  Widget build(BuildContext context) {
     _userSettings = Provider.of<UserSettings>(context);
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Container(
            //   width: 300,
            //   height: 100,
            //   child: RentmeProgressIndicator() //Loading(initialRadius: 30,),
            // ),

            AppIcon.logo.drawSvg(
              size: 120
            ),

            // Text("Muviin", style: TextStyle(fontSize: Dimens.textBig, color: RentMeColors.bpPurpleVeryLight),),
            // SizedBox(height:50),
            Stack(
              children: [
                AnimatedContainer(
                    duration: Duration(milliseconds: 500),
                    height: 5,
                    width: 200,
                    decoration: BoxDecoration(
                        borderRadius : BorderRadius.circular(5),
                      color: Colors.redAccent.withOpacity(0.2)
                    ),
                  ),
                AnimatedContainer(
                    duration: Duration(milliseconds: 500),
                    height: 5,
                    width: addition,
                    decoration: BoxDecoration(
                        borderRadius : BorderRadius.circular(5),
                      color: Colors.redAccent
                    ),
                  ),
              ],
            ),
            SizedBox(height:30),
            // Text("Loading Muviin Info", style: TextStyle(fontSize: Dimens.textSmall, color: RentMeColors.bpPurpleVeryLight, fontWeight: FontWeight.bold),),
            
          ],
        )
      ),
      )
    );
  }



}