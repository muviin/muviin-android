import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/main.dart';
import 'package:rentme_ghana/src/blocs/message_bloc.dart';
import 'package:rentme_ghana/src/model/message.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/icons.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/chat_detail.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/utils_date.dart';


class Chat extends StatefulWidget{
  final String id;

  const Chat({Key key, this.id}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return ChatState();
  }

}


class ChatState extends State<Chat>{


var mTextMessageController = new TextEditingController();
SocketIO socketIO;
List<Message> messages = List();
List<Message> oldmessages = List();
MessageBloc _messageBloc = MessageBloc();
bool _isloading = true;
bool _unreadPresent = false;
int unreadCount = 0;
UserSettings _userSettings;
Timer timer;

//  _connectSocket01() {
//     socketIO = SocketIOManager().createSocketIO("http://192.168.8.158:3000", "", query: "", socketStatusCallback: _socketStatus);

//     //call init socket before doing anything
//     socketIO.init();

//     socketIO.subscribe("receive_message", (data){
//       Utils.log(data);
//     });
//     //connect socket
//     socketIO.connect();

//   }

//   _onSocketInfo(dynamic data) {
//     print("Socket info id: "+data);
//   }
//   _socketStatus(dynamic data) {
//     print("Socket status>>>>>>>: " + data);
//   }


//    void _sendChatMessage(String msg) async {
//     if (socketIO != null) {
//       String jsonData = json.encode({'message': "HEYA"});
//       SocketIOManager mm = SocketIOManager();
//       socketIO.sendMessage("send_message", jsonData, _onReceiveChatMessage);
//     }
//   }

//   void _onReceiveChatMessage(dynamic message) {
//     print("Message from UFO: " + message);
//   }
  

  fetchMessages(){
    _messageBloc.getMessagesHistory(widget.id,);
  }

_fetchLocalMessages(){
  var localRents = _userSettings.getMessages();
  List<Message> list = List();
  if(!Utils.isEmptyOrNull(localRents)){
    for (var i = 0; i < localRents.length; i++) {
      list.add(Message.fromJson(localRents[i]));
    }
    setState(() {
      messages= list.reversed.toList();
      _isloading = false;
    });
  }
}

refreshChat(){
  timer = Timer.periodic(Duration(seconds: 1), (timer) {
      fetchMessages();
   });
}



  @override
  void initState() {
    // _connectSocket01();
    refreshChat();
    listeners();
    fetchMessages();

    super.initState();
  }


  @override
  void dispose() { 
    if(timer != null){
      timer.cancel();
    }
    super.dispose();
  }






  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    _fetchLocalMessages();
    return Scaffold(
      body: Center(
        child: _isloading ? Center(child: RentmeProgressIndicator(),): !Utils.isEmptyOrNull(messages)? _messagesList(context):
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppIcon.emptyMsg.drawSvg(
                size: 200
              ),
              SizedBox(height:20),
              Text("No Messages",style: AppStyles.textBodyDefault.copyWith(color:RentMeColors.bpPurpleVeryLight),)
            ],
          )
        )
        ),
    );
  }



Widget _messagesList(BuildContext context){
  return  NotificationListener<OverscrollIndicatorNotification>(
    onNotification: (overscroll) {
      overscroll.disallowGlow();
    },
    child: ListView.builder(
      itemCount: messages.length,
      itemBuilder: (context, i){
        return Container(
          child: Column(
            children: <Widget>[
              GestureDetector(
                child: ListTile(
                // leading: CircularRadius(
                //   view: AssetImage("assets/images/beauty.jpg"),
                // ),
                leading: Container(
                  width: 40,
                  height: 40,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: CachedNetworkImage(
                      imageUrl: !Utils.isEmptyOrNull(messages[i].senderImage)? Constants.SPACE_URL+"users/"+messages[i].senderImage:Constants.RM_API_BASE_URL+"/src/images/default.jpg",
                      placeholder: (context, url) => RentmeProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                title: Text(messages[i].senderName??""),
                subtitle: Text(showLastMsg(messages[i].senderId)),
                trailing: Padding(
                  padding: const EdgeInsets.only(top:10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(datef(messages[i].date), style: AppStyles.textBody2.copyWith(fontSize:12, color: Colors.black54),),
                      SizedBox(height: 5,),
                      chec(messages[i].senderId) ? _buildUnread() : Text("") //Icon(Icons.arrow_drop_down)
                   ],
               ),
                ),
              ),
              onTap: (){
                print("message");
                // _sendChatMessage("HI THERE");
                Navigator.push(context, MaterialPageRoute(
                  builder: (context){
                    return ChatDetail(id: widget.id,messageData: messages[i],);
                  }
                ));
              },
              ),
              Divider()
            ],
          )
        );
      },
    ),
  );
}


  listeners(){
    _messageBloc.history.listen((messagelist) {
      setState(() {
        oldmessages = messagelist;

        List<Message> nonRepetitive = [];
        // THIS BLOCK OF CODES PREVENTS REPITIVE VALUES IN THE LISTS
        for (var i = 0; i < messagelist.length; i++) {
          bool repeated = false;
          for (var j = 0; j < nonRepetitive.length; j++) {
            if (messagelist[i].senderId == nonRepetitive[j].senderId) {
              repeated = true;
            }
          }
          if (!repeated) {
            nonRepetitive.add(messagelist[i]);
          }
        }
        messages = nonRepetitive;
        _isloading = false;


        /// putting new [messages] ontop 
        moveOnTop(messagelist);


      });
    });
  }

  Widget _buildUnread(){
    return Container(
      height: 20,
      width: 20,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.green
      ),
      alignment: Alignment.center,
      child: Text("$unreadCount", style: TextStyle(color: RentMeColors.white),),
    );
  }


  bool chec(String id){
    List<String> included = List();
    for (var msg in oldmessages) {
      if(msg.messageStatus == "0" && msg.receiverId == "${widget.id}" && msg.senderId == id){
          included.add(msg.id);
      }
    }
    if(included.length > 0){
      Utils.log(included);
      unreadCount = included.length;

      /// to show [notification] on bottom nav that u have a message

      // if(id == "52"){
      //    messageCount = unreadCount;
      // }
      return true;
    }
    return false;
  }

  moveOnTop(List<Message> messagelist){
    List<Message> listToMove = [];
    for (var msg in messagelist) {
      // moveOnTop(msg.senderId);
      /// the [receiverId] check is to prevent the seing of senders own messages
      if((msg.receiverId == "${widget.id}" && msg.messageStatus == "0")){
        listToMove.add(msg);
      }
    }
    // remove name from list already
    for (var lst in listToMove) {
      messages.removeWhere((element) => lst.senderId == element.senderId);
      messages.join(', ');
      Utils.log(messages);
      // add ontop
      messages.add(lst);
    }
    _userSettings.setMessages(messages);
  }

  String showLastMsg(String id){
    if(!Utils.isEmptyOrNull(id)){
      List<String> included = List();
      for (var msg in oldmessages) {
        if((msg.receiverId == "${widget.id}" && msg.senderId == id) || (msg.receiverId == id && msg.senderId == "${widget.id}")){
            included.add(msg.message);
        }
      }
      if(!Utils.isEmptyOrNull(included)){
        return included.last??"File";
      }
      return "loading...";
    }
    return "hi";
  }
  String datef(String date){
    if(!Utils.isEmptyOrNull(date)){
      // DateTime tempDate = new DateFormat("d-MMM-yyyy hh:mm a").parse(date);
      // var stringDate = DateFormat('hh:mm a').format(tempDate);

      /// converting into [TIMEAGO]
      try {
        String timeAgo = DateUtils.timeAgoSinceDate(date);
        return timeAgo; //stringDate;
        
      } catch (e) {
        //12-Jan-2021
        String timeAgo = DateUtils.timeAgoSinceDate(date, format: "dd-MMM-yyyy");
        return timeAgo;
      }

    }
    return "";
  }





}