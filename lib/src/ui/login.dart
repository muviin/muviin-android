import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/login_bloc.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/model/signup_model.dart';
import 'package:rentme_ghana/src/repository/auth_repository.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/services/extensions.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/home_controller.dart';
import 'package:rentme_ghana/src/ui/reset_first_page.dart';
import 'package:rentme_ghana/src/ui/setup_page.dart';
import 'package:rentme_ghana/src/ui/signup.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';
import 'package:rentme_ghana/src/ui/widgets/text_field.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/validators.dart';

class Login extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<Login>{

final GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();
bool displayLoad = false;

AuthRepository auth = new AuthRepository();
final SetupBloc setupBloc = SetupBloc();
LoginBloc _loginBloc = LoginBloc();
UserSettings _userSettings;
TextController emailCtrl = TextController();
TextController pwdCtrl = TextController();
final _formKey = GlobalKey<FormState>();


void login()async{

  if(await emailCtrl.validate() && await pwdCtrl.validate()){
    setState(() {
      displayLoad = true;
    });
      LoginModel login = LoginModel()
      ..email = emailCtrl.text.trim()
      ..password = pwdCtrl.text.trim();
      _loginBloc.loginUser(login);

  }
}



    Widget textFieldView(String hints, TextEditingController controller, bool obscure) {
    return Container(
      height: 50,
      margin: EdgeInsets.only(top: 30),
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Container(
        padding: EdgeInsets.only(top: 0, left: 10, right: 10),
        child: TextField(
          obscureText: obscure,
          controller: controller,
          decoration:
              InputDecoration(hintText: hints, border: InputBorder.none),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: RentMeColors.white,
        ),
      ),
    );
  }


  @override
  void initState() { 
    super.initState();
    listeners();
  }


  @override
  Widget build(BuildContext context) {

    _userSettings = Provider.of<UserSettings>(context);

    return Scaffold(
      body: Container(
        // onWillPop: _onWillPop,
        child: Container(
          margin: EdgeInsets.only(top: Dimens.marginXXLarge),
          child: Form(
            key: _formKey,
            child: Container(
              // padding: EdgeInsets.symmetric(vertical: Dimens.marginRegular),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child:  NotificationListener<OverscrollIndicatorNotification>(
                      onNotification: (overscroll) {
                        overscroll.disallowGlow();
                      },
                      child: ListView(
                        children: <Widget>[
                          SizedBox(height: 20),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 80
                            ),
                            child: Center(
                              child: Text(
                                "Login",
                                textAlign: TextAlign.center,
                                style: AppStyles.textTitleDefualt,
                              ),
                            ),
                          ),
                          SizedBox(height: 21),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 80
                              ),
                            child: Center(
                              child: Text(
                                "Use either email or phone and password to login",
                                textAlign: TextAlign.center,
                                style: AppStyles.textBodyDefault.copyWith(color: RentMeColors.bpPurpleVeryLight, fontSize: 15)
                                
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                         Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                              left: 38,
                              right: 38 //Dimens.marginRegular
                            ),
                          child: AppTextField(
                            controller: emailCtrl,
                            borderColor: RentMeColors.transparent,
                            placeholder: "Email / Phone Number", //"Middle name(Optional)",
                            showCountryEntry: false,
                             validations: [
                              RequiredValueValidation(context),
                              // EmailValidation(context)
                            ],

                          ),
                        ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 20,
                                left: 38, //Dimens.marginRegular,
                                right: 38//Dimens.marginRegular
                              ),
                            // child: getCountrySelector(context),
                            child: AppTextField(
                              controller: pwdCtrl,
                              borderColor: RentMeColors.transparent,
                              showCountryEntry: false,
                              placeholder: "Enter Password",
                              isPassword: true,
                              validations: [
                                RequiredValueValidation(context),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: Dimens.marginRegular,
                          ),
                          InkWell(
                            onTap: resetPwd,
                            child: Container(
                              padding: EdgeInsets.all(10),
                              child: Center(
                                child: Text("Forgot Password?", style: AppStyles.textBody2.copyWith(color:RentMeColors.bpPurpleVeryLight),),
                              ),
                            ),
                          )

                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: Dimens.marginRegular),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: Dimens.marginRegular),
                          alignment: FractionalOffset.bottomCenter,
                          child: RentmeButton(
                            onPressed: login,
                            size: ButtonSize.COMPACT,
                            text:  "Login",
                            isLoading: displayLoad,
                          ),
                        ),

                         InkWell(
                           onTap: _toSignup,
                           child: RichText(
                            text: TextSpan(
                              text: "New User?",
                              style: AppStyles.textTitleLight.copyWith(
                                color: RentMeColors.bpPurpleVeryLight,
                                fontWeight: FontWeight.w300
                              ),
                              children: [
                                TextSpan(
                                  text: " Sign up",
                                  style: AppStyles.textTitleLight
                                  // recognizer: TapGestureRecognizer()..onTap = () => Navigation.mainroute(context).pop(),
                                ),
                              ]
                            ),
                        ),
                         )



                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }



    listeners(){

    _loginBloc.loginuser.listen((user) {
      Utils.log(user);
      _userSettings.setUser(user);
        setState(() {
          displayLoad = false;
        });

      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext ctx){
            return MultiProvider(
              providers: [
                Provider<SetupBloc>(
                  builder: (_) => SetupBloc(),
                ),
              ],
              child: SetupPage(),
            );
          }
        )
      );


    }).onError((err){
      Utils.log(err);
      setState(() {
        displayLoad = false;
      });
      showMessage(context, err.toString());
    });

  }

  _toSignup(){
    Navigator.push(
      context, 
      MaterialPageRoute(
        builder: (BuildContext ctx){
          return SignupPage();
        }
      )
    );
  }

  resetPwd(){
    Utils.log(">>>resetting");
    Navigator.push(
      context, 
      MaterialPageRoute(
        builder: (BuildContext ctx){
          return ResetFirstPage();
        }
      )
    );
  }







}
