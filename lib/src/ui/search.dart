import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';
import 'package:rentme_ghana/src/ui/widgets/search_locations.dart';


class MapSearch extends StatefulWidget{
  final User user;
  final bool showAppBar;

  const MapSearch({Key key, this.user, this.showAppBar = false,}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return MapSearchState();
  }

}


class MapSearchState extends State<MapSearch>{

  UserSettings _userSettings;




  @override
  void initState() {
    super.initState();



  }


  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);

    return Scaffold(
      appBar: widget.showAppBar ?RentMeAppBar.create(
        onBackPressed: (){
          Navigator.pop(context);
        }
      ):null,
      body: Center(
        child: SearchLocation(userSettings: _userSettings,user: _userSettings.getUser(),),
      ),
    );
  }
}