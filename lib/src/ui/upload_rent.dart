import 'dart:async';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_place/google_place.dart';
import 'package:intl/intl.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/address_bloc.dart';
import 'package:rentme_ghana/src/blocs/rent_bloc.dart';
import 'package:rentme_ghana/src/blocs/verification_block.dart';
import 'package:rentme_ghana/src/model/address.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/services/ui.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/select_upload_location.dart';
import 'package:rentme_ghana/src/ui/widgets/camera_types.dart';
import 'package:rentme_ghana/src/ui/widgets/masked_text_controller.dart';
import 'package:rentme_ghana/src/ui/widgets/text_field.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';
import 'package:rentme_ghana/src/ui/widgets/user_avatar.dart';
import 'package:rentme_ghana/src/utils/validators.dart';
import 'package:rounded_modal/rounded_modal.dart';

class UploadRentpage extends StatefulWidget{
  final User user;

  const UploadRentpage({Key key, this.user}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return UploadRentpageState();
  }

}

class UploadRentpageState extends State<UploadRentpage>{

  final GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();

  List<File> imagelist = [];


  TextController typeCtrl = TextController();
  TextController locationCtrl = TextController();
  TextController streetCtrl = TextController();
  TextController priceCtrl = TextController();
  TextController descCtrl = TextController();
  TextController countryCtrl = TextController();

  UppercaseTextEditingController _gpsCtrl = UppercaseTextEditingController(mask: 'A@-0000-0000');
  String address = "";


  bool type = false;
  bool finaltype = false;
  String rentType = '';
  String uploadType = "House";
  String chargeType = "/mo";
  

  Country _selectedCountry;
  List<Asset> resultList = List<Asset>();
  List<Asset> images = List<Asset>();
  String _error = 'No Error Dectected';
  bool _isloading = false;
  File img;
  UserSettings _userSettings;

  RentBloc _bloc = RentBloc();
  GooglePlace googlePlace;
  static const kGoogleApiKey = "AIzaSyDWg6uFUfbxGf0zEKtUorrkJRAo6I00Kvg";

  @override
  void initState() {
    super.initState();

    googlePlace = GooglePlace(kGoogleApiKey);

    listeners();

  }



  removeImage(int index){
    setState(() {
       imagelist.removeAt(index);
    });
  }


  @override
  Widget build(BuildContext context) {
     _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    return Scaffold(
      key: _scaffold,
      appBar: RentMeAppBar.create(
        // leading: IconButton(
        //   icon: Icon(Icons.format_align_left),
        //   color: Colors.black,
        //   onPressed: (){
        //   },
        // ),
                leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        trailing: UserAvatar(
          width: 40,
          height: 40,
          // picture: Image.network(widget.user.profile, fit: BoxFit.cover,height: 30.0, width: 30.0,)
        )
      ),
      body: _buildlist(),
    );
  }


  Widget _buildlist(){
    return Container(
      margin: EdgeInsets.only(top:10),
      color: RentMeColors.silverGrey,
      child: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 10, top:10),
            child: Text(
              "Choose Upload Type",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.black45
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.only(left: 10, top:0, right: 10),
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            height: 70,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.all(0),
              scrollDirection: Axis.horizontal,
              children: [
                Row(
                  children: <Widget>[
                    new Radio(
                      value: "House",
                      groupValue: uploadType,
                      onChanged: (chkd){
                        setState(() {
                          uploadType = chkd;
                        });
                        print(uploadType);
                      },
                    ),
                    Text("House")
                  ],
                ),
                Row(
                  children: <Widget>[
                    new Radio(
                      value: "Office",
                      groupValue: uploadType,
                      onChanged: (chkd){
                        setState(() {
                          uploadType = chkd;
                        });
                        print(uploadType);
                      },
                    ),
                    Text("Office")
                  ],
                ),
                Row(
                  children: <Widget>[
                    new Radio(
                      value: "Event Center",
                      groupValue: uploadType,
                      onChanged: (chkd){
                        setState(() {
                          uploadType = chkd;
                        });
                        print(uploadType);
                      },
                    ),
                    Text("Event Center")
                  ],
                ),
              ],
            ),
          ),

          // CONTENT TO DISPLAY
          uploadType == "House" ? uploadhouseWidget()
          : uploadType == "Office" ? soon()
          : soon(),








          Padding(
            padding: EdgeInsets.only(left: 10, top:10),
            child: Text(
              "Add atleast one image",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.black45
              ),
            ),
          ),

          Container(
            padding: EdgeInsets.only(left: 10, top:10),
            height: imagelist.length == 0 ? 10:100,
            child:ListView.builder(
              itemCount: imagelist.length,
              padding: EdgeInsets.all(0),
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext ctx, index){
                return GestureDetector(
                  onTap: (){
                    Utils.log("value");
                    removeImage(index);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right:Dimens.marginRegular),
                    color: RentMeColors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            alignment: Alignment.centerRight,
                            height: 4,
                            width: 20,
                            color: RentMeColors.red,
                          ),
                        ),
                        SizedBox(height:10),
                        Container(
                          height: 60,
                          width: 60,
                          margin: EdgeInsets.only(right: 10),
                          child: Image(
                            image: FileImage(imagelist[index]), //AssetImage("assets/images/smile.jpg"),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            )
          ),

          Padding(
            padding: EdgeInsets.only(left: 10, top:10, right: 10),
            child: RentmeButton(
              text: "Choose Photo",
              primaryColor: Colors.black,
              onPressed: _selectCamType//loadAssets,
            )
          ),
          Padding(
            padding: EdgeInsets.only(left: 10, top:20, right: 10, bottom: 20),
            child: RentmeButton(
              text: "Review Details",
              isLoading: _isloading,
              primaryColor: RentMeColors.colorPrimary,
              onPressed: submitToReview,
            )
          ),


        ],
      ),
    );
  }

  Widget soon(){
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Center(
        child: Text("Coming soon...", style: AppStyles.textTitleDefualt.copyWith(color:RentMeColors.bpPurpleVeryLight),)
      ),
    );
  }


  uploadhouseWidget(){
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppTextField(
            controller: typeCtrl,
            borderColor: RentMeColors.transparent,
            placeholder: "House type eg, 2 BEDROOM WITH PORCH",
            showCountryEntry: false,
              validations: [
              RequiredValueValidation(context),
            ],
          ),
          SizedBox(height: 10),
          GestureDetector(
            onTap: () async{
                String location = await Navigator.push(context, MaterialPageRoute(
                builder: (context) => SelectLocationSearch()
              ));
              Utils.log(location);
              setState(() {
                locationCtrl.text = location;
              });
            },
            //TODO:
            child: AbsorbPointer(
              absorbing: true,
              child: AppTextField(
                controller: locationCtrl,
                borderColor: RentMeColors.transparent,
                placeholder: "Location",
                showCountryEntry: false,
                  validations: [
                  RequiredValueValidation(context),
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
          _userSettings.getUser().country == "Ghana"? Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(13),
                border: Border.all(
                  color: RentMeColors.transparent
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    // spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(2, 3), // changes position of shadow
                  ),
                ],
              ),
              child: TextFormField(
                controller: _gpsCtrl,

                keyboardType: TextInputType.visiblePassword, 
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left:20),
                  hintText: "gps address (eg C3-2234-443)",
                  hintStyle: AppStyles.textBodyDefault.copyWith(
                    color: RentMeColors.textAccent,
                  ),
                ),
                style: AppStyles.textBodyDefault.copyWith(color:RentMeColors.black),

                validator: (value) {
                  if (Utils.isEmptyOrNull(value)) {
                    return "required";
                  }
                  address = value;
                },
              ),
            ):Container(),


           SizedBox(height: 10),
          AppTextField(
            controller: streetCtrl,
            borderColor: RentMeColors.transparent,
            placeholder: "Street name (Optional)",
            showCountryEntry: false,
          ),
          SizedBox(height: 10),
          AppTextField(
            controller: priceCtrl,
            borderColor: RentMeColors.transparent,
            placeholder: "Enter price (Optional)",
            showCountryEntry: false,
            keyboardType: TextInputType.number,
          ),
          SizedBox(height: 10),
          AppTextField(
            controller: descCtrl,
            borderColor: RentMeColors.transparent,
            placeholder: "A simple description of property (Optional)",
            showCountryEntry: false,
            keyboardType: TextInputType.multiline,
            maxLines: null
          ),
          SizedBox(height: 10),
          AppTextField(
            controller: countryCtrl,
            borderColor: RentMeColors.transparent,
            showCountryEntry: true,
            showTextField: false,
            selectedCountry: this._selectedCountry,
            onCountrySelected: (country){
              setState(() {
                this._selectedCountry = country;
              });
            },
          ),
          SizedBox(height: 10),
          Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(left: 10, top:10),
            child: Text(
              "Choose Rent Type",
              style: AppStyles.textBodyDefault
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.only(left: 10, top:10),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                  new Radio(
                    value: "Rent",
                    groupValue: rentType,
                    onChanged: (chkd){
                      setState(() {
                        rentType = chkd;
                      });
                      print(rentType);
                    },
                  ),
                    Text("Rent")
                  ],
                ),
                Row(
                  children: <Widget>[
                    new Radio(
                      value: "Sale",
                      groupValue: rentType,
                      onChanged: (chkd){
                        setState(() {
                          rentType = chkd;
                        });
                        print(rentType);
                      },
                    ),
                    Text("Sale")
                  ],
                )
              ],
            )
          ),
          SizedBox(height: 10),
          rentType == "Rent" ? Container(
            color: Colors.white,
            padding: EdgeInsets.only(left: 10, top:10),
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Charge per", style: AppStyles.textBodyDefault,),
                Row(
                  children: <Widget>[
                    new Radio(
                      value: "/mo",
                      groupValue: chargeType,
                      onChanged: (chkd){
                        setState(() {
                          chargeType = chkd;
                        });
                        print(rentType);
                      },
                    ),
                    Text("/mo")
                  ],
                ),
                Row(
                  children: <Widget>[
                    new Radio(
                      value: "/yr",
                      groupValue: chargeType,
                      onChanged: (chkd){
                        setState(() {
                          chargeType = chkd;
                        });
                        print(rentType);
                      },
                    ),
                    Text("/yr")
                  ],
                ),
              ],
            ),
          ):Container()

        ],
      ),
    );
  }

        _selectCamType() {
        showRoundedModalBottomSheet<void>(
          dismissOnTap: false,
          radius: 10.0,
          color: Colors.white,
          context: context, 
          builder: (BuildContext context) {
            return Container(
              child: CameraType(
                userImage: _userSettings.getUser().profile,
                onSelected: (cameraOptions) async {
                  Navigator.pop(context);

                  switch(cameraOptions){
                    case "CAMERA":
                    loadAssets(options: CameraOptions.CAMERA);
                      return;

                    case "GALLERY":
                    loadAssets(options: CameraOptions.GALLERY);
                      return;

                    case "REMOVE":
                      return;
                  }

                },
              ),
            );
          }
        );
      }

  Future<void> loadAssets({CameraOptions options = CameraOptions.CAMERA}) async {

    /// show [popup] to select type of image select
    img = await Utils.captureImageFromCamera(quality: 60,type: options);
    img = await Utils.compressImageFile(img, 30);

    setState(() {
      imagelist.add(img);
    });
  }


  submitToReview() async {    


    // check to see if all fields are valid
    var typeValid = await typeCtrl.validate();
    var locationValid = await locationCtrl.validate();
    var streetValid = await streetCtrl.validate();
    var priceValid = await priceCtrl.validate();
    var descValid = await descCtrl.validate();

    if(typeValid && locationValid && streetValid && priceValid && descValid 
      && !Utils.isEmptyOrNull(chargeType) 
      && !Utils.isEmptyOrNull(_selectedCountry)
      && !Utils.isEmptyOrNull(rentType)
      ){
     

      try {


        String placeId;
        Location placeCordinates;
        GhPost address;

        if(_userSettings.getUser().country != "Ghana"){
          EasyLoading.show(status: 'Uploading post...');
          VerificationBloc verificationBloc = VerificationBloc();
          placeId = await verificationBloc.autoCompleteSearch(googlePlace: googlePlace, value: locationCtrl.text);
          placeCordinates = await verificationBloc.getDetils(googlePlace: googlePlace, placeId: placeId);
        }
        else{
          // get post address befor proceeding
          if(_userSettings.getUser().country == "Ghana"){
            address = await getAddressInfo();
          }
        }
        EasyLoading.show(status: 'Uploading post...');

        Rents _rents = Rents();
        _rents.posterId = "${_userSettings.getUser().id}";
        _rents.country = _selectedCountry.name; //_userSettings.getUser().country;
        _rents.countryCode = _selectedCountry.isoCode;
        _rents.description = descCtrl.text;
        _rents.posterEmail = _userSettings.getUser().email;
        _rents.lat = _userSettings.getUser().country == "Ghana"? "${address.cordinate[0]}": "${placeCordinates.lat}";
        _rents.location = locationCtrl.text.toLowerCase();
        _rents.long = _userSettings.getUser().country == "Ghana"? "${address.cordinate[1]}":"${placeCordinates.lng}";
        _rents.posterPhone = _userSettings.getUser().phone;
        _rents.posterName = _userSettings.getUser().name;
        _rents.rentType = rentType;
        _rents.street = streetCtrl.text;
        _rents.type = typeCtrl.text;
        _rents.price = priceCtrl.text; // convert price to default dollar value
        _rents.posterImage = _userSettings.getUser().profile ?? "";
        _rents.verify = _userSettings.getUser().verify;
        _rents.upLoadType = uploadType;
        _rents.chargeType = chargeType;
        _bloc.uploadRent(_rents, imagelist);

        
      } catch (e) {
        setState(() {
          _isloading = false;
        });
        EasyLoading.dismiss();
        showMessage(context, "Network Unavailable");
      }
    }
  }


  Future<GhPost> getAddressInfo() async{
    AddressBloc addressBloc = AddressBloc();
    addressBloc.getPostAddressInfo(_gpsCtrl.text);
    var resp = await showStreamProgressDialogMessage<GhPost>(context, "Fetching address", addressBloc.postAddress);
    return resp.responseObject;
  }


  listeners(){
    _bloc.uploadRentResponse.listen((response){
      setState(() {
        _isloading = false;
      });

      // show notification
      // _scaffold.currentState.showSnackBar(UI.displayNotification("done", false));
      
      // app should submit everything in review page
      // but for certain reasons, we are doing it now here.
      EasyLoading.showSuccess('Great Success!');
      Navigator.pop(context);
    }).onError((err){

      setState(() {
        _isloading = false;
      });
      EasyLoading.showError('Failed, ${err.toString()}');
      // showMessage(context, err.toString());

    });
  }



}