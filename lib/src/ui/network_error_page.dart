import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:rentme_ghana/main.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/utils.dart';

class NetWorkErrorPage extends StatefulWidget{
  @override
  _NetWorkErrorPageState createState() => _NetWorkErrorPageState();
}

class _NetWorkErrorPageState extends State<NetWorkErrorPage> {

  goToSetup(){
    Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(
        builder: (context) {
          return MyApp();
        },
      ),
      (Route<dynamic> route) => false
    );
  }


  @override
  void initState() { 
    super.initState();

    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      // Got a new connectivity status!
      Utils.log(result);
      if (result != ConnectivityResult.none) {
        goToSetup();
      }

    });
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(
              image: AssetImage("assets/images/ntwrk.png"),
            ),
            SizedBox(height: Dimens.marginRegular,),
            Text(
              "You are not connected to the internet",
              style: AppStyles.textBodyDefault.copyWith(color:RentMeColors.bpPurpleVeryLight),
            ),
            // SizedBox(height: 5,),
            // RentmeButton(
            //   text: "Refresh page",
            //   primary: true,
            //   primaryColor: Colors.white,
            //   textColor: RentMeColors.bpPurpleVeryLight,
            // ),
            SizedBox(height: Dimens.marginXLarge,),
            _listenNetwrk()
          ],
        ),
      ),
    );
  }

  Widget _listenNetwrk(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "listening for internet connections",
          style: AppStyles.textBodyDefault.copyWith(color:RentMeColors.bpPurpleVeryLight),
        ),
        SizedBox(width:10),
        RentmeProgressIndicator()
      ],
    );
  }
}