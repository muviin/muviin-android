import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/login_bloc.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/model/signup_model.dart';
import 'package:rentme_ghana/src/repository/auth_repository.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/services/extensions.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/home_controller.dart';
import 'package:rentme_ghana/src/ui/login.dart';
import 'package:rentme_ghana/src/ui/reset_second_page.dart';
import 'package:rentme_ghana/src/ui/setup_page.dart';
import 'package:rentme_ghana/src/ui/signup.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';
import 'package:rentme_ghana/src/ui/widgets/text_field.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/validators.dart';

class ResetFirstPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return ResetFirstPageState();
  }

}

class ResetFirstPageState extends State<ResetFirstPage>{

final GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();
bool displayLoad = false;

AuthRepository auth = new AuthRepository();
final SetupBloc setupBloc = SetupBloc();
LoginBloc _loginBloc = LoginBloc();
UserSettings _userSettings;
TextController emailCtrl = TextController();
TextController pwdCtrl = TextController();
final _formKey = GlobalKey<FormState>();


void proceed()async{

  if(await emailCtrl.validate()){
    setState(() {
      displayLoad = true;
    });
      _loginBloc.requestPwd(emailCtrl.text.trim());

  }
}



    Widget textFieldView(String hints, TextEditingController controller, bool obscure) {
    return Container(
      height: 50,
      margin: EdgeInsets.only(top: 30),
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Container(
        padding: EdgeInsets.only(top: 0, left: 10, right: 10),
        child: TextField(
          obscureText: obscure,
          controller: controller,
          decoration:
              InputDecoration(hintText: hints, border: InputBorder.none),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: RentMeColors.white,
        ),
      ),
    );
  }


  @override
  void initState() { 
    super.initState();
    listeners();
  }


  @override
  Widget build(BuildContext context) {

    _userSettings = Provider.of<UserSettings>(context);

    return Scaffold(
      body: Container(
        // onWillPop: _onWillPop,
        child: Container(
          margin: EdgeInsets.only(top: Dimens.marginXXLarge),
          child: Form(
            key: _formKey,
            child: Container(
              // padding: EdgeInsets.symmetric(vertical: Dimens.marginRegular),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child:  NotificationListener<OverscrollIndicatorNotification>(
                      onNotification: (overscroll) {
                        overscroll.disallowGlow();
                      },
                      child: ListView(
                        children: <Widget>[
                          SizedBox(height: 20),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 80
                            ),
                            child: Text(
                              "Enter Email",
                              style: AppStyles.textTitleDefualt,
                            ),
                          ),
                          SizedBox(height: 21),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 80
                              ),
                            child: Center(
                              child: Text(
                                "We will send u a code to use for confirmation ",
                                textAlign: TextAlign.center,
                                style: AppStyles.textBodyDefault.copyWith(color: RentMeColors.bpPurpleVeryLight, fontSize: 15)
                                
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                         Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                              left: 38,
                              right: 38 //Dimens.marginRegular
                            ),
                          child: AppTextField(
                            controller: emailCtrl,
                            borderColor: RentMeColors.transparent,
                            placeholder: "Email", //"Middle name(Optional)",
                            showCountryEntry: false,
                             validations: [
                              RequiredValueValidation(context),
                              EmailValidation(context)
                            ],

                          ),
                        ),

                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: Dimens.marginRegular),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: Dimens.marginRegular),
                          alignment: FractionalOffset.bottomCenter,
                          child: RentmeButton(
                            onPressed: proceed,
                            size: ButtonSize.COMPACT,
                            text:  "Proceed",
                            isLoading: displayLoad,
                          ),
                        ),

                         InkWell(
                           onTap: _toLogin,
                           child: RichText(
                            text: TextSpan(
                              text: "Back to",
                              style: AppStyles.textTitleLight.copyWith(
                                color: RentMeColors.bpPurpleVeryLight,
                                fontWeight: FontWeight.w300
                              ),
                              children: [
                                TextSpan(
                                  text: " login",
                                  style: AppStyles.textTitleLight
                                  // recognizer: TapGestureRecognizer()..onTap = () => Navigation.mainroute(context).pop(),
                                ),
                              ]
                            ),
                        ),
                         )



                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }



    listeners(){

    _loginBloc.passwordRequest.listen((response) {
        setState(() {
          displayLoad = false;
        });

        if(response){
          Navigator.push(
            context, 
            MaterialPageRoute(
              builder: (BuildContext ctx){
                return MultiProvider(
                  providers: [
                    Provider<SetupBloc>(
                      builder: (_) => SetupBloc(),
                    ),
                  ],
                  child: ResetSecondPage(
                    email: emailCtrl.text,
                  ),
                );
              }
            )
          );
        }

    }).onError((err){
      Utils.log(err);
      setState(() {
        displayLoad = false;
      });
      showMessage(context, err.toString());
    });

  }

  _toLogin(){
    Navigator.push(
      context, 
      MaterialPageRoute(
        builder: (BuildContext ctx){
          return Login();
        }
      )
    );
  }

  resetPwd(){
    Utils.log(">>>resetting");
  }







}
