import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/services/extensions.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/login.dart';
import 'package:rentme_ghana/src/ui/settings_detail.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';

class Settings extends StatefulWidget {
  final User user;

  const Settings({Key key, this.user}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return SettingsState();
  }
}

class SettingsState extends State<Settings> {

  UserSettings _userSettings;
  
  void logout() {
    showDoubleOptionDialog(context, "Do you want to logout ?",
    (option) async {
      if(option){
        _userSettings.logout(context);
        // SetupBloc setup = Provider.of<SetupBloc>(context);
        // setup.logout.listen((response) {
        //   print(response);
        //   Navigator.push(context, ScaleRoute(widget: Login()));
        // });
        // setup.logouActiont();
        }
      }
    );
  }


  routeToDetail({SettingsType type}){
    Navigator.push(context, MaterialPageRoute(
      builder: (context) => SettingsDetail(type: type,)
    ));
  }

  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    return Scaffold(
      appBar: RentMeAppBar.create(
        onBackPressed: (){
          Navigator.pop(context);
        },
      ),
      body: Container(
        color: Colors.white, //Colors.black45.withOpacity(0.1),
        child:  NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
          },
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Text("Change Profile", style: AppStyles.textTitleDefualt.copyWith(fontSize:30),),
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: header(title: "MY ACCOUNT",),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                color: RentMeColors.white,
                child: Column(
                  children: [
                    body(left: "Name", right: _userSettings.getUser().name, action: ()=>routeToDetail(type: SettingsType.NAME_CHANGE)),
                    Divider(),
                    body(left: "Country", right: _userSettings.getUser().country, action: ()=>routeToDetail(type: SettingsType.COUNTRY_CHANGE), active: false),
                    Divider(),
                    body(left: "Mobile", right: _userSettings.getUser().phone, action: ()=>routeToDetail(type: SettingsType.MOBILE_CHANGE), active: false),
                    Divider(),
                    body(left: "Email", right: _userSettings.getUser().email, action: ()=>routeToDetail(type: SettingsType.EMAIL_CHANGE), active: false),
                    Divider(),
                    body(left: "Password", right: "", action: ()=>routeToDetail(type: SettingsType.PASSWORD_CHANGE)),
                  ],
                ),
              ),
              // Padding(
              //   padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              //   child: header(title: "ACCOUNT ACTION"),
              // ),
              // Container(
              //   padding: EdgeInsets.only(left: 20, right: 20),
              //   color: RentMeColors.white,
              //   child: Column(
              //     children: [
              //       body(left: "Logout", right: "", action: logout),
              //     ],
              //   ),
              // ),

            ],
          ),
        ),
      ),
    );
  }




Widget header({String title}){
  return Container(
    child: Text(title, style: AppStyles.textBody3.copyWith(color: RentMeColors.textPrimaryLight, fontSize: Dimens.textRegular)),
  );
}

Widget body({String left, right, Function action, bool active = true}){
  return InkWell(
    onTap: active ? action : (){},
    child: Container(
      padding: EdgeInsets.symmetric(vertical: Dimens.marginRegular),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(left, style: AppStyles.textBody3.copyWith(fontWeight: FontWeight.bold),),
          Text(right, style: AppStyles.textBody3.copyWith(color: RentMeColors.black.withOpacity(0.5)),)
        ],
      )
    ),
  );
}




}
