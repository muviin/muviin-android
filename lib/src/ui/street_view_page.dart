import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:rentme_ghana/src/blocs/verification_block.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';

class StreetViewPage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return _StreetViewPageState();
  }

}


class _StreetViewPageState extends State<StreetViewPage>{

  Map<String, dynamic> testData = {
    "name": "tellem"
  };


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimens.marginSmall),
      height: MediaQuery.of(context).size.width,
      width: MediaQuery.of(context).size.width,
      // child: AndroidView(
      //     viewType: "flutterpiechart",
      //     creationParams: testData, //inputs,
      //     creationParamsCodec: StandardMessageCodec()
      //   ),
      child: RentmeButton(
        text: "show streetview",
        onPressed: (){
          VerificationBloc vv = VerificationBloc();
          vv.startStreetView();
        },
      ),
    );
  }

}