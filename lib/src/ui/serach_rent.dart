
import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rentme_ghana/src/blocs/rent_bloc.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/ui/details.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;


class SearchRent extends StatefulWidget{
  final List<Rents> rents;
  final User user;

  const SearchRent({Key key, this.rents, this.user}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return SearchRentState();
  }

}


class SearchRentState extends State<SearchRent>{


  TextController _searchctrl = TextController();
  List<Rents> searchedRents = List();
  bool isloading = false;

  List<Rents> rentList = [];
  FocusNode focusNode = FocusNode();
  RentBloc _rentBloc = RentBloc();



  fetchRents(String type){
  _rentBloc.fetchRent(widget.user.country, type, filter: _searchctrl.text);
}

  listeners(){

      _rentBloc.rents.listen((rents){
      setState(() {
        rentList = rents.reversed.toList();
        isloading = false;
      });
    });


    _searchctrl.addListener(() {
      setState(() {
        fetchRents("ALL");
      });
    });




  }


  @override
  void initState() {
    super.initState();
    listeners();

    _speech = stt.SpeechToText();

  }




  @override
  Widget build(BuildContext context) {
    List<Rents> listItems = rentList;
    focusNode.requestFocus();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: RentMeColors.white,
        leading: IconButton(
          icon: Icon(Icons.chevron_left, color: RentMeColors.black,),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        title: _buildSearchField(),
        actions: _buildActions(),
      ),
      body: Container(
        color: RentMeColors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: !Utils.isEmptyOrNull(_searchctrl.text) ? ListView.builder(
                  itemCount: listItems.length, //searchedRents.length,
                  itemBuilder: (BuildContext ctx, index){
                    return InkWell(
                      onTap: (){
                        // Navigator.pop(context, searchedRents[index]);
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) => Details(rent: listItems[index], user: widget.user,)
                        ));
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.all(10),
                            child: ListTile(
                              contentPadding: EdgeInsets.all(0),
                              title: Text(listItems[index].type),
                              subtitle: Padding(
                                padding: const EdgeInsets.only(top:5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(CupertinoIcons.location, color: Colors.redAccent,),
                                    Text(listItems[index].location+ ', '+listItems[index].country),
                                  ],
                                ),
                              )
                            )
                          ),
                          Divider()
                        ],
                      ),
                    );
                  },
                ):
                Align(
                  alignment: Alignment.center,
                  child: Container() //RentmeProgressIndicator(),
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: AvatarGlow(
        animate: _isListening,
        duration: const Duration(milliseconds: 2000),
        repeat: true,
        endRadius: 75,
        glowColor: RentMeColors.bpPurpleVeryLight,
        child: FloatingActionButton(
          backgroundColor: RentMeColors.bpPurpleVeryLight,
          onPressed: voiceRecognizer,
          child: Icon(_isListening? Icons.mic:Icons.mic_none),
        ),
      ), 
    );
  }


    Widget _buildSearchField() {
    return Container(
      decoration: BoxDecoration(
      ),
      child: TextField(
        autofocus: true,
        controller: _searchctrl,
        decoration: InputDecoration(
          hintText: "search by anything..",
          border: InputBorder.none,
          hintStyle: AppStyles.textBody2.copyWith(color: RentMeColors.bpPurpleVeryLight)
        ),
        style: TextStyle(color: Colors.black, fontSize: 16.0),
      ),
    );
  }

    List<Widget> _buildActions() {
    return <Widget>[
      Padding(
        padding: const EdgeInsets.only(right:8.0),
        child: Center(
          child: IconButton(
            icon: Icon(Icons.close,color: RentMeColors.black,),
            onPressed: (){
              setState(() {
                _searchctrl.text = "";
              });
            },
          )
        ),
      )
  ];
}


  stt.SpeechToText _speech;
  bool _isListening = false;
  double _confidence = 0.1;
  
  voiceRecognizer()async{
    Utils.log(_speech);
    bool hasPermission = await _speech.hasPermission;
    if(!hasPermission){
      // request permission
      
    }
    if(!_isListening){
      bool available = await _speech.initialize(
        onStatus: (val)=> print("onstaus: $val"),
        onError: (val)=>print("onstaus: $val")
      );
      if(available){
        setState(() {
          _isListening = true;
        });
        _speech.listen(
          onResult: (val){
            _searchctrl.text = val.recognizedWords;
            setState(() {
              if(val.hasConfidenceRating && val.confidence > 0){
                _confidence = val.confidence;
                _isListening = false;
              }
            });
          }
        );
      }
    }else{
      setState(() {
        _isListening = false;
      });
      _speech.stop();
    }
  }
  

  






}