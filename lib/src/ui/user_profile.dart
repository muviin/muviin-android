import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/login_bloc.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';
import 'package:rentme_ghana/src/ui/widgets/camera_types.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/ui/widgets/user_avatar.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rounded_modal/rounded_modal.dart';


class UserProfile extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return UserProfileState();
  }

}


class UserProfileState extends State<UserProfile>{


  bool _imageLoading = false;
  LoginBloc _loginBloc = LoginBloc();
  UserSettings _userSettings;
  SetupBloc _setupBloc = SetupBloc();
  TextEditingController bioCtrl = TextEditingController();


  Future getImage({CameraOptions options = CameraOptions.CAMERA}) async {
    setState(() {
      _imageLoading = true;
    });
    File image = await Utils.captureImageFromCamera(quality: 60, type: options);
    File _userImageTemp = await Utils.compressImageFile(image, 30);
    
    if(_userImageTemp == null){
      setState(() {
        _imageLoading = false;
      });
    }

    _loginBloc.profilePicResponse.listen((user){
      _userSettings.setUser(user);
        setState(() {
          _imageLoading = false;
        });
    }).onError((err){
      setState(() {
        _imageLoading = false;
      });
    });
    _loginBloc.setProfilePic(_userSettings.getUser().email, _userImageTemp);
  }

  save(){
    EasyLoading.show(status: 'setting bio...');
    Map<String, dynamic> toJson = Map();
    toJson = {
      "id": _userSettings.getUser().id,
      "type": "4",
      "bio": bioCtrl.text
    };
    _setupBloc.updateProfile(toJson);
  }

  listeners(){
    _setupBloc.profile.listen((response) { 
      User user = _userSettings.getUser();
      user.bio = bioCtrl.text;
      _userSettings.setUser(user);
      EasyLoading.showSuccess('Great Success!');
      EasyLoading.dismiss();
    });
  }


  @override
  void initState() { 
    super.initState();
    listeners();
    
  }



  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    // bioCtrl.text = _userSettings.getUser().bio;
    return Scaffold(
      appBar: RentMeAppBar.create(
        onBackPressed: (){
          Navigator.pop(context);
        },
        trailing: Padding(
          padding: const EdgeInsets.only(top:Dimens.marginSmall),
          child: InkWell(
            onTap: save,
            child: Text("Save", style: AppStyles.textBody2,)
          ),
        )
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        // color: Colors.black.withOpacity(0.7),
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        child:  NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
          },
          child: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: 30),
                      child: UserAvatar(
                        height: 100,
                        width: 100,
                        isLoading: _imageLoading,
                        picture: _userSettings.getUser().profile != "" && _userSettings.getUser().profile != null
                          ? GestureDetector(
                              onTap: () {
                                // _viewImage();
                              },
                              // child: Image(
                              //   image:  NetworkImage(Constants.POS_API_BASE_URL+"/"+_userSettings.getUser().profile),
                              //   fit: BoxFit.cover,
                              // ),
                              child: CachedNetworkImage(
                                  imageUrl: Constants.SPACE_URL+"users/"+_userSettings.getUser().profile,
                                  placeholder: (context, url) => RentmeProgressIndicator(),
                                  errorWidget: (context, url, error) => Icon(Icons.error),
                                  fit: BoxFit.cover,
                              ),
                            )
                          : Container(
                              child: Icon(Icons.supervised_user_circle, size: 70, color: RentMeColors.white),
                            ),
                        isUploadable: true,
                        onUploadTap: () {
                          _selectCamType();
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left:Dimens.marginRegular, top: Dimens.textLarge),
                    child: Text("Account Details", style: AppStyles.textTitleDefualt.copyWith(fontSize:24),),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left:Dimens.marginRegular, top: Dimens.textLarge),
                    child: Text("Location", style: AppStyles.textBody2.copyWith(fontSize:16),),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top:Dimens.marginRegular, left: Dimens.marginRegular),
                    child: Text(_userSettings.getUser().country, style: TextStyle(fontSize: 18, color: Colors.black.withOpacity(0.7)),),
                  ),
                  Container(height: 0.3,color: Colors.black.withOpacity(0.7), margin: EdgeInsets.only(top: 20, bottom: 0, left: Dimens.marginRegular, right: Dimens.marginRegular),),
                  Padding(
                    padding: EdgeInsets.only(left:Dimens.marginRegular, top: Dimens.textLarge),
                    child: Text("Edit your Bio", style: AppStyles.textBody2.copyWith(fontSize:16, color: RentMeColors.textPrimaryLight),),
                  ),
                  Container(
                    height: 60,
                    padding: const EdgeInsets.only(top:0, left: Dimens.marginRegular),
                    // child: Text(_userSettings.getUser().country, style: TextStyle(fontSize: 18, color: Colors.black.withOpacity(0.7)),),
                    child: TextFormField(
                      controller: bioCtrl,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(0),
                        hintText: "Enter Bio",
                      ),
                      maxLines: 5,
                    ),
                  ),
                  Container(height: 0.3,color: Colors.black.withOpacity(0.7), margin: EdgeInsets.only(top: 0, bottom: 0, left: Dimens.marginRegular, right: Dimens.marginRegular),),



                ],
              ),
            ],
          ),
        )
        ),
    );
  }


      _selectCamType() {
        showRoundedModalBottomSheet<void>(
          dismissOnTap: false,
          radius: 10.0,
          color: Colors.white,
          context: context, 
          builder: (BuildContext context) {
            return Container(
              child: CameraType(
                // userImage: widget.user.profile,
                onSelected: (cameraOptions) async {
                  Navigator.pop(context);

                  switch(cameraOptions){
                    case "CAMERA":
                    getImage(options: CameraOptions.CAMERA);
                      return;

                    case "GALLERY":
                    getImage(options: CameraOptions.GALLERY);
                      return;

                    case "REMOVE":
                      return;
                  }

                },
              ),
            );
          }
        );
      }








}