import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/icons.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/widgets/app_bar.dart';

class AboutusPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _AboutusPageState();
  }
}

class _AboutusPageState extends State<AboutusPage> {

  UserSettings _userSettings;

  @override
  Widget build(BuildContext context) {
    _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
    return Scaffold(
      appBar: RentMeAppBar.create(
        onBackPressed: (){
          Navigator.pop(context);
        },
      ),
      body: Container(
        color: Colors.white, //Colors.black45.withOpacity(0.1),
        child:  NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
          },
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  children: [
                    AppIcon.about.drawSvg(size: Dimens.textExtraLarge),
                    Text("About Muviin", style: AppStyles.textTitleDefualt.copyWith(fontSize:30),),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: Text("Muviin is a mobile application that gives everyone and every company the platform and opportunity to sell their properties being it Houses, lands, Rooms, Offices, WareHouses, Event Centers etc. It also provides the platform for people all over the world to see the properties upload and move into business with whoeevr posted it., ", 
                style: AppStyles.textBody2.copyWith(fontSize:16),),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                child: Row(
                  children: [
                    Image(
                      image: AssetImage("assets/images/mission.png",),
                      height: Dimens.textExtraLarge,
                    ),
                    Text("Mission", style: AppStyles.textTitleDefualt.copyWith(fontSize:30),),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10, left: 20, right: 20,),
                child: Text("Our Mission is to make digital or online business as smooth and trustworthy than ever", 
                  style: AppStyles.textBody2.copyWith(fontSize:16),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                child: Row(
                  children: [
                    Image(
                      image: AssetImage("assets/images/mission.png",),
                      height: Dimens.textExtraLarge,
                    ),
                    Text("Vision", style: AppStyles.textTitleDefualt.copyWith(fontSize:30),),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Text("Our Vision is to be the world's leading online platform for property trading businesses", 
                  style: AppStyles.textBody2.copyWith(fontSize:16),
                ),
              ),
             
            ],
          ),
        ),
      ),
    );
  }




Widget header({String title, description = ""}){
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Container(
        child: Text(title, style: AppStyles.textTitleDefualt.copyWith(color: RentMeColors.textPrimary,)),
      ),
      Container(
        child: Text(description, style: AppStyles.textBody3.copyWith(color: RentMeColors.textPrimaryLight,)),
      ),
    ],
  );
}






}
