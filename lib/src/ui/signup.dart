import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart' as piker;
import 'package:google_place/google_place.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/blocs/signup_bloc.dart';
import 'package:rentme_ghana/src/model/country.dart';
import 'package:rentme_ghana/src/model/signup_model.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/services/extensions.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/home_controller.dart';
import 'package:rentme_ghana/src/ui/login.dart';
import 'package:rentme_ghana/src/ui/setup_page.dart';
import 'package:rentme_ghana/src/ui/widgets/buttons.dart';
import 'package:rentme_ghana/src/ui/widgets/circular_radius.dart';
import 'package:rentme_ghana/src/ui/widgets/text_field.dart';
import 'package:rentme_ghana/src/utils/controllers.dart';
import 'package:rentme_ghana/src/utils/dialogs.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/utils_verifications.dart';
import 'package:rentme_ghana/src/utils/validators.dart';
import 'package:rentme_ghana/src/model/country.dart' as cc;

class SignupPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SignupPageState();
  }

}

class SignupPageState extends State<SignupPage>{
  piker.Country _selectedCountry;


  SignupBloc _signupBloc = SignupBloc();
  bool displayLoad = false;
  UserSettings _userSettings;

TextController nameCtrl = TextController();
TextController emailCtrl = TextController();
TextController pwdCtrl = TextController();
TextController phoneCtrl = TextController();
final _formKey = GlobalKey<FormState>();



  @override
  void initState() {
    super.initState();

    listeners();
  }

  // @override
  // Widget build(BuildContext context) {
  //   _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
  //   return Scaffold(
  //     body: Container(
  //       color: RentMeColors.black.withOpacity(0.7),
  //       child: Center(
  //           child: ListView(
  //         children: <Widget>[
  //           Stack(
  //             alignment: AlignmentDirectional.bottomCenter,
  //             children: <Widget>[
  //               Column(
  //                 children: <Widget>[
  //                   // Container(
  //                   //   margin: EdgeInsets.only(top: 50),
  //                   //   child: CircularRadius(
  //                   //     height: 150,
  //                   //     width: 150,
  //                   //     view: AssetImage("assets/images/abi.jpg"),
  //                   //   ),
  //                   // ),
  //                   textFielView("Full Name", namectrl),
  //                   textFielView("email", emailctrl),
  //                   textFielView("Phone", phonectrl),
  //                   textFielView("password", passwordctrl),
  //                   Container(
  //                     margin: EdgeInsets.only(top: 30, left: 20, right: 20),
  //                     width: MediaQuery.of(context).size.width,
  //                     decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.all(Radius.circular(10)),
  //                       color: RentMeColors.white,
  //                     ),
  //                     child: FlatButton(
                        // child: CountryPicker(
                        //           showFlag: true,
                        //           showName: true,
                        //           selectedCountry: _selectedCountry,
                        //           onChanged: (country){
                        //             print(country.name);
                        //             setState(() {
                        //               this._selectedCountry = country;
                        //             });
                        //           },
                        // ),
  //                       // child: Text(
  //                       //   "Select Country",
  //                       //   style: TextStyle(color: AppColors.lightBlack),
  //                       // ),
  //                     ),
  //                   ),
  //                   Container(
  //                     margin: EdgeInsets.only(top: 30, left: 20, right: 20),
  //                     width: MediaQuery.of(context).size.width,
  //                     decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.all(Radius.circular(10)),
  //                       color: RentMeColors.colorPrimary,
  //                     ),
  //                     child: RentmeButton(
  //                       onPressed: signup,
  //                       text:  "Signup",
  //                       isLoading: displayLoad,
  //                     ),
  //                   ),
  //                   Container(
  //                     margin: EdgeInsets.only(top: 30, left: 20, right: 20),
  //                     width: MediaQuery.of(context).size.width,
  //                     child: FlatButton(
  //                       child: Text(
  //                         "Click to Sign",
  //                         style: TextStyle(
  //                             color: RentMeColors.colorPrimary,
  //                             fontWeight: FontWeight.bold),
  //                       ),
  //                       onPressed: () {
  //                         Navigator.push(context, ScaleRoute(widget: Login()));
  //                       },
  //                     ),
  //                   )
  //                 ],
  //               )
  //             ],
  //           )
  //         ],
  //       )),
  //     ),
  //   );
  // }

    @override
  Widget build(BuildContext context) {

    _userSettings = Provider.of<UserSettings>(context);

    return Scaffold(
      body: Container(
        // onWillPop: _onWillPop,
        child: Container(
          margin: EdgeInsets.only(top: Dimens.marginTopFromSafe),
          child: Form(
            key: _formKey,
            child: Container(
              // padding: EdgeInsets.symmetric(vertical: Dimens.marginRegular),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child:  NotificationListener<OverscrollIndicatorNotification>(
                      onNotification: (overscroll) {
                        overscroll.disallowGlow();
                      },
                      child: ListView(
                        children: <Widget>[
                          SizedBox(height: 20),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 80
                            ),
                            child: Center(
                              child: Text(
                                "Lets get you started!",
                                textAlign: TextAlign.center,
                                style: AppStyles.textTitleDefualt,
                              ),
                            ),
                          ),
                          SizedBox(height: 21),
                          // SizedBox(height: 10),
                         Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                              left: 38,
                              right: 38 //Dimens.marginRegular
                            ),
                          child: AppTextField(
                            controller: nameCtrl,
                            borderColor: RentMeColors.transparent,
                            placeholder: "Full name", //"Middle name(Optional)",
                            showCountryEntry: false,
                             validations: [
                              RequiredValueValidation(context),
                            ],

                          ),
                        ),
                         Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                              left: 38,
                              right: 38 //Dimens.marginRegular
                            ),
                          child: AppTextField(
                            controller: emailCtrl,
                            borderColor: RentMeColors.transparent,
                            placeholder: "Email", //"Middle name(Optional)",
                            showCountryEntry: false,
                             validations: [
                              RequiredValueValidation(context),
                              EmailValidation(context)
                            ],

                          ),
                        ),
                         Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                              left: 38,
                              right: 38 //Dimens.marginRegular
                            ),
                          child: AppTextField(
                            controller: phoneCtrl,
                            borderColor: RentMeColors.transparent,
                            placeholder: "Phone No.", //"Middle name(Optional)",
                            showCountryEntry: true,
                            keyboardType: TextInputType.phone,
                            selectedCountry: _selectedCountry,
                            onCountrySelected: (country){
                              cc.Country ct = cc.Country()
                              ..countryCode = country.isoCode
                              ..name = country.name
                              ..currencyName = country.currency
                              ..currencyCode = country.currencyISO;
                              _userSettings.setCountry(ct);
                              
                              Utils.log(country);
                               setState(() {
                                  this._selectedCountry = country;
                                });
                            },
                             validations: [
                              RequiredValueValidation(context),
                            ],

                          ),
                        ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 10,
                                left: 38, //Dimens.marginRegular,
                                right: 38//Dimens.marginRegular
                              ),
                            // child: getCountrySelector(context),
                            child: AppTextField(
                              controller: pwdCtrl,
                              borderColor: RentMeColors.transparent,
                              showCountryEntry: false,
                              placeholder: "Enter Password",
                              isPassword: true,
                              validations: [
                                RequiredValueValidation(context),
                              ],
                            ),
                          ),
                          SizedBox(height:20),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 80
                              ),
                            child: Center(
                              child: Text(
                                "By signing up, you agree to our terms and conditions",
                                textAlign: TextAlign.center,
                                style: AppStyles.textBodyDefault.copyWith(color: RentMeColors.bpPurpleVeryLight, fontSize: 15)
                                
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: Dimens.marginRegular),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: Dimens.marginRegular),
                          alignment: FractionalOffset.bottomCenter,
                          child: RentmeButton(
                            onPressed: signup,
                            size: ButtonSize.COMPACT,
                            text:  "Signup",
                            isLoading: displayLoad,
                          ),
                        ),

                         InkWell(
                           onTap: _toLogin,
                           child: RichText(
                            text: TextSpan(
                              text: "Already have an account?",
                              style: AppStyles.textTitleLight.copyWith(
                                color: RentMeColors.bpPurpleVeryLight,
                                fontWeight: FontWeight.w300
                              ),
                              children: [
                                TextSpan(
                                  text: " Sign in",
                                  style: AppStyles.textTitleLight
                                  // recognizer: TapGestureRecognizer()..onTap = () => Navigation.mainroute(context).pop(),
                                ),
                              ]
                            ),
                        ),
                         )



                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


  signup() async{

    try {

      if(await nameCtrl.validate() && await emailCtrl.validate() && await phoneCtrl.validate() && await pwdCtrl.validate()){

        setState(() {
          displayLoad = true;
        });

        if(await VerificationUtils.parsePhoneNumber(phoneCtrl.text, phoneCtrl.getSelectedCountry(_userSettings).countryCode) == null) {
          showMessage(context, "Invalid Phone");
          setState(() {
            displayLoad = false;
          });
          return;
        }

        SignUp signUp = SignUp()
        ..name = nameCtrl.text.trim()
        ..email = emailCtrl.text.trim()
        ..phone = phoneCtrl.text.trim()
        ..country = _selectedCountry.name
        ..password = pwdCtrl.text.trim();
        _signupBloc.signupUser(signUp);

      }
      
    } catch (e) {
      showMessage(context, "Something went wrong, check country");
    }
  }



  listeners(){

    _signupBloc.signupuser.listen((user) {
      Utils.log(user);
      _userSettings.setUser(user);

          setState(() {
          displayLoad = true;
        });

      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext ctx){
            return MultiProvider(
              providers: [
                Provider<SetupBloc>(
                  builder: (_) => SetupBloc(),
                ),
              ],
              child: SetupPage(),
            );
          }
        )
      );


    }).onError((err){
        setState(() {
        displayLoad = false;
      });
      showMessage(context, err.toString());
      Utils.log(err);
    });

  }

  _toLogin(){
    Navigator.push(
      context, 
      MaterialPageRoute(
        builder: (BuildContext ctx){
          return Login();
        }
      )
    );
  }








}
