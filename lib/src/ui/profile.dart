import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:provider/provider.dart';
import 'package:rentme_ghana/src/blocs/rent_bloc.dart';
import 'package:rentme_ghana/src/blocs/setup_bloc.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/user.dart';
import 'package:rentme_ghana/src/res/colors.dart';
import 'package:rentme_ghana/src/res/dimens.dart';
import 'package:rentme_ghana/src/res/icons.dart';
import 'package:rentme_ghana/src/res/styles.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/ui/upload_rent.dart';
import 'package:rentme_ghana/src/ui/widgets/graphs/radial_chart.dart';
import 'package:rentme_ghana/src/ui/widgets/rprogress_indicator.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';


class Profile extends StatefulWidget{

  final User user;

  const Profile({Key key, this.user}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ProfileState();
  }

}


class ProfileState extends State<Profile>{

  bool _dialVisible = true;
  bool _isLoading = true;
  String email = "";
  RentBloc _rentBloc = RentBloc();
  List<Rents> userRentList = List();
  SetupBloc setup;
  File imageFile;
  bool loadingUpload = false;
  UserSettings _userSettings;

  List<Rents> houseRent = List();
  List<Rents> offcieRent = List();
  List<Rents> eventRent = List();
  List<Rents> pendingRent = List();
  double housePercentage = 0;
  double evntPercentage = 0;
  double officePercentage = 0;

  bool isRentSelected = false;
  bool isofficeSelected = false;
  bool isEventSelected = false;
  bool isPendingSelected = false;


  // void fetchUser(BuildContext context) {
  //   if(mounted){
  //     // setup = Provider.of<SetupBloc>(context);
  //     setup.userInfo();
  //   }
  // }

  fetchUserPost(){
    if(mounted){
      _rentBloc.fetchUserPost(widget.user.email);
    }
  }

  _fetchLocalUserPosts(){
  var localRents = _userSettings.getUserPosts();
  if(!Utils.isEmptyOrNull(localRents)){
      List<Rents> list = List();
    for (var i = 0; i < localRents.length; i++) {
      list.add(Rents.fromJson(localRents[i]));
    }
    setState(() {
      userRentList = list;

      initAssign(userRentList);

      _isLoading = false;
    });
  }
}


initAssign(List<Rents> userRents){
  houseRent.clear();
  eventRent.clear();
  offcieRent.clear();
  pendingRent.clear();
    // assign the values
    if(!Utils.isEmptyOrNull(userRents)){
      for (var userRent in userRents) {
        if(userRent.upLoadType.toUpperCase() == Constants.DISPLAY_HOUSE.toUpperCase() && userRent.approve == "1"){
          houseRent.add(userRent);
        }
        else if(userRent.upLoadType.toUpperCase() == Constants.DISPLAY_OFFICE.toUpperCase() && userRent.approve == "1"){
          offcieRent.add(userRent);
        }
        else if(userRent.upLoadType.toUpperCase() == Constants.DISPLAY_EVENT.toUpperCase() && userRent.approve == "1"){
          eventRent.add(userRent);
        }
        else if(userRent.approve == "0"){
          pendingRent.add(userRent);
        }
      }
    }

  // calculating for percentage
  var totalValue = userRentList.length;
  /// percentage value for [HOUSE]
  housePercentage = ((houseRent.length) / totalValue) * 100;
  /// percentage value for [EVENT]
  evntPercentage = ((eventRent.length) / totalValue) * 100;
  /// percentage value for [OFFICE]
  officePercentage = ((offcieRent.length) / totalValue) * 100;
}


  // route to upload rent
  toUpload(){
    Navigator.push(context, MaterialPageRoute(
      builder: (context) => UploadRentpage()
    ));
  }

  listener(){
    if(setup == null){

      _rentBloc.userRents.listen((userRents){
        _userSettings.setUserPosts(userRents);
        userRentList = userRents;
          
        setState(() {
          _isLoading = false;
          initAssign(userRents);
        });
      }).onError((err){
        setState(() {
          _isLoading = false;
        });
      });

    }
  }


  @override
  void initState() {
    super.initState();

    listener();

    fetchUserPost();
    // fetchUser(context);

  }

  @override
  void dispose() {
    super.dispose();
    // setup.dispose();
    // _rentBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
  _userSettings = _userSettings ?? Provider.of<UserSettings>(context);
  _fetchLocalUserPosts();
    return Scaffold(
      body: !loadingUpload ? Center(
        child: Column( //Column
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10, top: 10),
              child: Text(
                "Current Properties owned",
                style: TextStyle(
                  fontWeight: FontWeight.bold
                ),
                
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
              child: Text(
                "Tap any of the rent to add new images",
                style: AppStyles.textBody3
                
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.70, //MediaQuery.of(context).size.height * 0.5,
              padding: EdgeInsets.only(left: 10, top: 10),
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: _isLoading ? 
              Center(
                  child: RentmeProgressIndicator(
                ),
              ) :
              userRentList.length > 0 ? _buildList():
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AppIcon.emptyPost.drawSvg(
                        size: 200
                      ),
                      SizedBox(height:20),
                      Text("No Uploaded Rents",style: AppStyles.textBodyDefault.copyWith(color:RentMeColors.bpPurpleVeryLight),)
                    ],
                  )
                ) 
              )
            ),
          ],
        ),
        ):
        Center(
          child: RentmeProgressIndicator(),
        ),
        floatingActionButton: SpeedDial(
          marginRight: 18,
          marginBottom: 20,
          animatedIcon: AnimatedIcons.menu_close,
          animatedIconTheme: IconThemeData(size: 22.0),
          visible: _dialVisible,
          closeManually: false,
          curve: Curves.bounceIn,
          overlayColor: Colors.black,
          overlayOpacity: 0.5,
          onOpen: (){

          },
          onClose: (){
  
          },
          backgroundColor: Colors.black,
          foregroundColor: Colors.white,
          elevation: 8.0,
          shape: CircleBorder(),
          children: [
            SpeedDialChild(
              child: Icon(Icons.credit_card),
              backgroundColor: Colors.black,
              label: 'new package',
            ),
            SpeedDialChild(
              child: Icon(Icons.brush),
              backgroundColor: Colors.black,
              label: 'new Upload',
              onTap: toUpload
            ),
          ],
        ),
    );
  }

  Widget _buildList(){
    return  Container(
      child: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
          },
        child: ListView(
          children: [
            _buildCategoryGraph(),

            // pending view
            SizedBox(height:Dimens.marginBig),
           pendingRent.length > 0? _buildPendingLists(count: pendingRent.length, title: "Pending uploads", values: pendingRent):Container(),



            SizedBox(height:Dimens.marginBig),
            Text("Approved List", style: AppStyles.textTitleDefualt.copyWith(fontSize:Dimens.textRegular),),
             SizedBox(height:Dimens.marginSmall),
            houseRent.length > 0? _buildRentLists(title: "House", count: houseRent.length, values: houseRent):Container(),
            SizedBox(height:Dimens.marginSmall),
            offcieRent.length > 0? _buildOfficeLists(title: "Office", count: offcieRent.length, values: offcieRent):Container(),
            SizedBox(height:Dimens.marginSmall),
            eventRent.length > 0?_buildEventLists(title: "Event Centers", count: eventRent.length, values: eventRent):Container(),
          ],
        ),
      ),
    );
  }



   Widget profileHeaderButtons(IconData icon, String title, Function action){
    return GestureDetector(
      child: Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(right: 15, bottom: 5),
          width: 60,
          height: 60,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white
          ),
          child: Icon(icon),
        ),
        Container(
          margin: EdgeInsets.only(right: 15),
          child: Text(title, style: TextStyle(color: Colors.white),),
        )
        ],
      ),
      onTap: action
    );
  }



  Widget _buildRentLists({String title, int count, List<Rents> values}){
    return InkWell(
      onTap: (){
        setState(() {
          isRentSelected = !isRentSelected;
        });
      },
      child: Column(
        children: [
          Card(
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Text(title, style: AppStyles.textTitleDefualt.copyWith(fontSize:Dimens.textRegular),),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       AppIcon.apartmentRent.drawSvg(size: 40),
                       Text("$count items", style: AppStyles.textBodyDefault.copyWith(color:RentMeColors.bpPurpleVeryLight, fontSize: Dimens.textSmall),)
                     ],
                   ),
                 )
              ],
            )
          ),
          isRentSelected ? list(values):Container()
        ],
      ),
    );
  }
  Widget _buildOfficeLists({String title, int count, List<Rents> values}){
    return InkWell(
      onTap: (){
        setState(() {
          isofficeSelected = !isofficeSelected;
        });
      },
      child: Column(
        children: [
          Card(
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Text(title, style: AppStyles.textTitleDefualt.copyWith(fontSize:Dimens.textRegular),),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       AppIcon.apartmentRent.drawSvg(size: 40),
                       Text("$count items", style: AppStyles.textBodyDefault.copyWith(color:RentMeColors.bpPurpleVeryLight, fontSize: Dimens.textSmall),)
                     ],
                   ),
                 )
              ],
            )
          ),
           isofficeSelected ? list(values):Container()
        ],
      ),
    );
  }
  Widget _buildEventLists({String title, int count, List<Rents> values}){
    return InkWell(
      onTap: (){
        setState(() {
          isEventSelected = !isEventSelected;
        });
      },
      child: Column(
        children: [
          Card(
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Text(title, style: AppStyles.textTitleDefualt.copyWith(fontSize:Dimens.textRegular),),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       AppIcon.apartmentRent.drawSvg(size: 40),
                       Text("$count items", style: AppStyles.textBodyDefault.copyWith(color:RentMeColors.bpPurpleVeryLight, fontSize: Dimens.textSmall),)
                     ],
                   ),
                 )
              ],
            )
          ),
           isEventSelected ? list(values):Container()
        ],
      ),
    );
  }
  Widget _buildPendingLists({String title, int count, List<Rents> values}){
    return InkWell(
      onTap: (){
        setState(() {
          isPendingSelected = !isPendingSelected;
        });
      },
      child: Column(
        children: [
          Card(
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Text(title, style: AppStyles.textTitleDefualt.copyWith(fontSize:Dimens.textRegular),),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Text("(Pending Rents may take up to 10 days)", style: AppStyles.textBody2.copyWith(fontSize:13),),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       AppIcon.apartmentRent.drawSvg(size: 40),
                       Text("pending $count items", style: AppStyles.textBodyDefault.copyWith(color:RentMeColors.bpPurpleVeryLight, fontSize: Dimens.textSmall),)
                     ],
                   ),
                 )
              ],
            )
          ),
           isPendingSelected ? list(values):Container()
        ],
      ),
    );
  }

  Widget list(List<Rents> values){
    return Container(
      height: double.parse("${values.length * 100}"),
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: values.length,
        itemBuilder: (context, i){
          return Container(
            margin: EdgeInsets.only(bottom: 0),
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: Container(
                    height: 100,
                    width: 100,
                    child: CachedNetworkImage(
                        imageUrl: Constants.SPACE_URL+"rents/"+values[i].thumbnail,
                        placeholder: (context, url) => RentmeProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        fit: BoxFit.cover,
                    ),
                  ),
                  title: Text(
                    // "2 BEDROOM FOR SALE", 
                    values[i].type,
                    style: TextStyle(
                      fontSize: 15
                    ),
                  ),
                  subtitle: Text(values[i].createdAt),
                ),
                Divider()
              ],
            )
          );
        },
      ),
    );
  }



    Widget _buildCategoryGraph(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 4,
          child: Column(
            children: [
              RadialChart(
                percentages: [housePercentage],
                colors: [RentMeColors.red],
                size: 140,
                showCompletedLabel: false,
              ),
              Text("Houses")
            ],
          ),
        ),
        Expanded(
          flex: 4,
          child: Column(
            children: [
              RadialChart(
                percentages: [officePercentage],
                colors: [RentMeColors.black],
                size: 140,
                showCompletedLabel: false,
              ),
              Text("Offices")
            ],
          ),
        ),
        Expanded(
          flex: 4,
          child: Column(
            children: [
              RadialChart(
                percentages: [evntPercentage],
                colors: [RentMeColors.bpPurpleVeryLight,],
                size: 140,
                showCompletedLabel: false,
              ),
              Text("Event Centers")
            ],
          ),
        ),
      ],
    );
  }




}