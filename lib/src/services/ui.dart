import 'package:flutter/material.dart';

class UI {

  static void alert(BuildContext context, String title, String message, String subMessage, {Color color = Colors.red}){
      showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: ListBody(
              children: <Widget>[
                Text(message),
                Text(subMessage)
              ],
            )
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("cancel", style: TextStyle(color: Colors.blue),),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),

              FlatButton(
                child: Text("continue", style: TextStyle(color: color),),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
      );
  }


  static SnackBar displayNotification(String title, bool action, [String label]){
    if(action == true){
      return SnackBar(
        content: new Text(title),
        action: SnackBarAction(
          label: label,
          textColor: Colors.red,
          onPressed: (){
            print("ON WAY!!!");
          },
        ),
      );
    }
    else if(action == false){
      return SnackBar(
        content: new Text(title),
      );
    }
  }




}