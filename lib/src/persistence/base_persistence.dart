

import 'package:rentme_ghana/src/model/schema.dart';
import 'package:rentme_ghana/src/model/schemas/all_schema.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/settings/user_settings.dart';
import 'package:rentme_ghana/src/utils/constants.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/utils_db.dart';
import 'package:rentme_ghana/src/utils/utils_sql.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqlcool/sqlcool.dart';

final SqlCool database = SqlCool();

class DataPersistence {
  // static String _lastCurrentMigration;
  // static bool debugMode = !Utils.isProductionMode();
  static bool debugMode = true;
  static bool _debugMode = true;

  static Future<void> initDatabase({UserSettings userSettings}) async {
    try {
      await database.init(path: Constants.RM_DATABASE_PATH, schema: AllSchema.list, verbose: debugMode, debug: debugMode);

      // get last migration detail for DB init
      if(userSettings != null) {
        // MIGRATION
        // final lastMigration = userSettings.getLastDbMigration();
      
        // do migration
        // final latestMigration = doMigration(lastMigration);

        // save migration data
        // if(!Utils.isEmptyOrNull(latestMigration)) {
        //   userSettings.setLastDbMigration(latestMigration);
        // }
      }

      return Future.value();
    } catch (e) {
      Utils.log(e);
    }
    return Future.error('Could not initialize database');
  }

  static String doMigration(String lastMigration) {
    // List<String> migrationQueries = [];
    bool beginMigration = false;
    
    final migrations = AllSchema.migration ?? [];

    migrations.forEach((e) {
      if(Utils.isEmptyOrNull(lastMigration) || beginMigration) {
        // migrationQueries.addAll(e.queries);
        e.queries.forEach((migration) => database.query(migration));
        lastMigration = e.description;
      }

      // check if we are at last migration yet
      if(!beginMigration) {
        beginMigration =  e.description == lastMigration;
      }
    });

    return lastMigration;
  }

  Future<SelectBloc> dbSelectStream(Schema tableSchema, {
    String orderByColumn,
    SortDirection orderDirection = SortDirection.DESC,
    DatabaseQuery conditions,
    Schema joinTableSchema,
    String joinColumn,
    int pageIndex = 0,
    int pageSize = Constants.DEFAULT_PAGE_SIZE,
  }) async {
    if(!database.isReady) {
      Utils.log('DATABASE:: Database not ready. Initializing...');
      await initDatabase();
    }

    // check joins
    if(joinColumn != null || joinTableSchema != null) {
      if(joinColumn != null && joinTableSchema == null) {
        throw('When joinColumn is set, joinTableSchema cannot be empty');
      } else if(joinColumn == null && joinTableSchema != null) {
        throw('When joinTableSchema is set, joinColumn cannot be empty');
      }
    }

    pageSize = pageSize ?? Constants.DEFAULT_PAGE_SIZE;
    
    return SelectBloc(
      database: database,
      table: tableSchema.name,
      orderBy: (orderByColumn ?? tableSchema.sortColumn) + ' ' + Utils.enumToString(orderDirection ?? tableSchema.sortDirection),
      where: conditions?.isNotEmpty == true ? conditions?.query : null,
      // limit: pageSize,
      // offset: pageSize * pageIndex,
      limit: pageSize * ((pageIndex ?? 0) + 1), // should get in incremental order, not really paginated
      offset: 0,
      joinTable: joinTableSchema?.name,
      joinOn: joinTableSchema != null
        ? '${tableSchema.name}.$joinColumn = ${joinTableSchema.name}.${joinTableSchema.primaryColumn}'
        : null,
      reactive: true,
    );
  }

  Future<List<Map<String, dynamic>>> dbSelect(Schema tableSchema, {String columns = '*', String orderByColumn, SortDirection orderDirection, DatabaseQuery conditions, int pageIndex = 0, int pageSize = Constants.DEFAULT_PAGE_SIZE}) async {
    if(!database.isReady) {
      Utils.log('DATABASE:: Database not ready. Initializing...');
      await initDatabase();
    }
    
    return await database
        .select(
          table: tableSchema.name, 
          columns: columns,
          orderBy: (orderByColumn ?? tableSchema.sortColumn) + ' ' + Utils.enumToString(orderDirection ?? tableSchema.sortDirection),
          where: conditions?.isNotEmpty == true ? conditions?.query : null,
          // limit: pageSize,
          // offset: pageSize * pageIndex,
          limit: pageSize * (pageIndex + 1), // should get in incremental order, not really paginated
          offset: 0,
          verbose: _debugMode
        )
        .catchError((dynamic e) {
      throw e;
    });
  }

  Future<Map<String, dynamic>> dbSelectOne(Schema tableSchema, {DatabaseQuery conditions}) async {
    if(!database.isReady) {
      Utils.log('DATABASE:: Database not ready. Initializing...');
      await initDatabase();
    }
    
    final result = await database
        .select(
          table: tableSchema.name, 
          where: conditions?.isNotEmpty == true ? conditions?.query : null,
          limit: 1,
          offset: 0,
          verbose: debugMode
        )
        .catchError((dynamic e) {
          throw e;
        }
    );

    if(result.length > 0) {
      return result.first;
    } else {
      return null;
    }
  }

  Future<void> dbInsertUpdate(Schema tableSchema, Map<String, dynamic> data) async {
    if(!database.isReady) {
      Utils.log('DATABASE:: Database not ready. Initializing...');
      await initDatabase();
    }

    await database
        .upsert(
          table: tableSchema.name, 
          row: convertTODbRow(data), 
          verbose: debugMode,
        )
        .catchError((dynamic e) {
      throw e;
    });
  }

  // Future<DatabaseUpsert> dbInsertUpdate(Schema tableSchema, Map<String, dynamic> data, {String checksColumn, bool checks = false}) async {
  //   if(!database.isReady) {
  //     Utils.log('DATABASE:: Database not ready. Initializing...');
  //     await initDatabase();
  //   }

  //   // check for existing values before insert/update
  //   final find = checks 
  //     ? await dbSelectOne(
  //         tableSchema, 
  //         conditions: DatabaseQuery()
  //           ..where(checksColumn ?? tableSchema.primaryColumn, data[checksColumn ?? tableSchema.primaryColumn]),
  //       ) 
  //     : null;
    
  //   await database
  //       .upsert(
  //         table: tableSchema.name, 
  //         row: convertTODbRow(data), 
  //         verbose: this.verboseMode,
  //       )
  //       .catchError((dynamic e) {
  //     throw e;
  //   });

  //   return find != null ? DatabaseUpsert.UPDATE : DatabaseUpsert.INSERT;
  // }
  
  Future<void> dbUpdate(Schema tableSchema, Map<String, dynamic> data) async {
    if(!database.isReady) {
      Utils.log('DATABASE:: Database not ready. Initializing...');
      await initDatabase();
    }
    
    DatabaseQuery condition = DatabaseQuery()..where(tableSchema.primaryColumn, data[tableSchema.primaryColumn]);
    
    await database
      .update(
        table: tableSchema.name, 
        row: convertTODbRow(data), 
        where: condition.query,
        verbose: debugMode, )
      .catchError((dynamic e) {
      throw e;
    });
  }

  Future<void> dbInsertBulk(Schema tableSchema, List<Map<String, dynamic>> data) async {
    if(!database.isReady) {
      Utils.log('DATABASE:: Database not ready. Initializing...');
      await initDatabase();
    }

    await database
        .batchInsert(
          table: tableSchema.name, 
          rows: data.map((e) => convertTODbRow(e)).toList(), 
          conflictAlgorithm: ConflictAlgorithm.replace,
          verbose: debugMode
        )
        .catchError((dynamic e) {
      throw e;
    });
  }

  Future<void> dbTruncate(Schema tableSchema) async {
    if(!database.isReady) {
      Utils.log('DATABASE:: Database not ready. Initializing...');
      await initDatabase();
    }

    await database
        .query(
          'DELETE FROM ${tableSchema.name}',
          verbose: debugMode
        )
        .catchError((dynamic e) {
      throw e;
    });
  }

  Future<void> dbDelete(Schema tableSchema, String column, String value) async {
    if(!database.isReady) {
      Utils.log('DATABASE:: Database not ready. Initializing...');
      await initDatabase();
    }

    await database
        .query(
          'DELETE FROM ${tableSchema.name} WHERE $column = "$value"',
          verbose: debugMode
        )
        .catchError((dynamic e) {
      throw e;
    });
  }


  Map<String, String> convertTODbRow(Map<String, dynamic> map) {
    Map<String, String> newMap = {};
    map.keys.forEach((theKey) {
      newMap.putIfAbsent(theKey, () => map[theKey].toString());
    });
    return newMap;
  }

  dispose() {
    database.dispose();
  }


}