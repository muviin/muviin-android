import 'dart:async';


import 'package:flutter/foundation.dart';
import 'package:rentme_ghana/src/model/rents.dart';
import 'package:rentme_ghana/src/model/schema.dart';
import 'package:rentme_ghana/src/model/schemas/rents_schema.dart';
import 'package:rentme_ghana/src/persistence/base_persistence.dart';
import 'package:rentme_ghana/src/res/enums.dart';
import 'package:rentme_ghana/src/utils/utils.dart';
import 'package:rentme_ghana/src/utils/utils_db.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sqlcool/sqlcool.dart';

class RentPersistence {
  
  final Schema _table = RentsSchema();
  final _rentsFetcher = PublishSubject<List<Rents>>();
  final _db = DataPersistence();

  Stream<List<Rents>> get rents => _rentsFetcher.stream;
  SelectBloc _selectBloc;
  // bool _streamInit = false;
  int count = 0;
  // bool _streamFilterInit = false;

  void fetchProperties({
    String location, 
    String type, 
    String filter, 
    int pageIndex,
    int pageSize,
  }) async {
    if(_selectBloc != null) {
      // clear old listeners
      _selectBloc.dispose();
    }

    DatabaseQuery query = DatabaseQuery();
      
    if(location != null) {
      query.where('location', location);
    }
    
    if(type != null) {
      query.where('type', type);
    }

    if(!Utils.isEmptyOrNull(filter)) {
      // else do a search for the filter text in multiple fields
      query
        .group(
          DatabaseQuery()
            ..like('type', filter, QueryGate.OR)
            ..like('rent_type', filter, QueryGate.OR)
            ..like('location', filter, QueryGate.OR)
            ..like('poster_name', filter, QueryGate.OR)
            ..like('country', filter, QueryGate.OR)
            ..like('status', filter, QueryGate.OR)
      );
  }

    // initialize sqlcool stream
    _selectBloc = await _db.dbSelectStream(
      _table,
      conditions: query,
      pageIndex: pageIndex,
      pageSize: pageSize,
    );

    // listen for changes in stream
    _selectBloc.items.listen((items) {
      Utils.log('Number of times = ${count++}');
      List<Rents> productList = [];

      items.forEach((element) {
        productList.add(Rents.fromJson(element));
      });

      _rentsFetcher.sink.add(productList);
    });
  }

  Future<Rents> getSingleProduct(String id) async {
    Map<String, dynamic> map = await _db.dbSelectOne(_table, conditions: DatabaseQuery()..where(_table.primaryColumn, id));
    if(map != null) {
      return Rents.fromJson(map);
    }
    return null;
  }

  Future<List<String>> getProducers() async {
    var _col = 'producer';
    List<Map<String, dynamic>> list = await _db.dbSelect(
      _table,
      columns: _col,
    );

    if(list != null) {
      return list.map((e) => e[_col].toString()).toList();
    }
    return null;
  }

  Future addProduct(Rents item) async {
    await _db.dbInsertUpdate(_table, item.toDb());
  }

  Future editProduct(Rents item) async {
    await _db.dbInsertUpdate(_table, item.toDb());
  }

  Future updateProductInventory({@required String id, @required int quantity, @required int threshold}) async {
    await _db.dbUpdate(_table, {
      'product_id': id,
      'quantity': quantity,
      'min_threshold': threshold,
    });
  }

  Future storeProductList(List<Rents> productList) async {
    if(!kIsWeb) {
      await _db.dbInsertBulk(_table, productList.map((e) => e.toDb()).toList());
    } else {
      _rentsFetcher.sink.add(productList);
    }
  }

  emptyProducts() async {
    await _db.dbTruncate(_table);
  }

  dispose() {
    _rentsFetcher.close();

    if(_selectBloc != null) {
      _selectBloc.dispose();
    }
  }


}
